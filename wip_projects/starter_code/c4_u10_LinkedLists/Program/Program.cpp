#include "Program.h"

#include "../Utilities/Menu.h"

#include <iostream>
#include <iomanip>
#include <fstream>

void Program::SetDataPath(std::string path)
{
	m_dataPath = path;
	std::cout << "Program data should be located at \"" << m_dataPath << "\"..." << std::endl;
}

void Program::Setup()
{
	// Load in the data file
	std::ifstream input(m_dataPath);
	if (input.fail())
	{
		std::cout << "ERROR! Could not find data file at \"" << m_dataPath << "\"! Please double check the path!" << std::endl;
		return;
	}


	int size;
	input >> size;

	for (int i = 0; i < size; i++)
	{
		std::string name;
		float price;
		int quantity;

		input.ignore();
		std::getline(input, name);
		input >> price;
		input >> quantity;

		m_products.push_back(Product(name, price, quantity)); // TODO: Replace me
	}
}

void Program::Cleanup()
{
	// Save data
	std::ofstream output(m_dataPath);
	output << m_products.size() << std::endl; // TODO: Replace me

	for (size_t i = 0; i < m_products.size(); i++) // TODO: Replace me
	{
		output << m_products[i].GetName() << std::endl; // TODO: Replace me
		output << m_products[i].GetPrice() << std::endl; // TODO: Replace me
		output << m_products[i].GetQuantity() << std::endl; // TODO: Replace me
	}
}

void Program::Run()
{
	Setup();
	Menu_Main();
	Cleanup();
}

void Program::Menu_Main()
{
	bool done = false;
	while (!done)
	{
		Utility::Menu::Header("MAIN MENU");

		DisplayProducts(m_products);

		std::cout << std::endl << "OPTIONS" << std::endl;
		int choice = Utility::Menu::ShowIntMenuWithPrompt({
			"Add product",
			"Edit product",
			"Delete product",
			}, true, true);

		switch (choice)
		{
		case 0:
			done = true;
			break;

		case 1:
			Menu_AddProduct();
			break;

		case 2:
			Menu_EditProduct();
			break;

		case 3:
			Menu_DeleteProduct();
			break;
		}

	}
}

void Program::Menu_AddProduct()
{
	Utility::Menu::Header("ADD NEW PRODUCT");

	std::string name;
	float price;
	int quantity;

	std::cout << "Enter new product's NAME: ";
	std::cin.ignore();
	getline(std::cin, name);

	std::cout << "Enter new product's PRICE: $";
	std::cin >> price;

	std::cout << "Enter new product's QUANTITY: ";
	std::cin >> quantity;

	m_products.push_back(Product(name, price, quantity)); // TODO: Replace me

	std::cout << std::endl << "Product added." << std::endl;

	Utility::Menu::Pause();
}

void Program::Menu_EditProduct()
{
	Utility::Menu::Header("EDIT EXISTING PRODUCT");

	DisplayProducts(m_products);

	int index;
	index = Utility::Menu::GetValidChoice(0, m_products.size() - 1, "Enter ID of item to edit: "); // TODO: Replace me

	std::cout << std::endl << "Edit which field?" << std::endl;
	int field = Utility::Menu::ShowIntMenuWithPrompt({
		"Name",
		"Price",
		"Quantity"
		});

	switch (field)
	{
		case 1:
		{
			std::string newName;
			std::cout << "Enter new name: ";
			std::cin.ignore();
			std::getline(std::cin, newName);
			m_products[index].SetName(newName); // TODO: Replace me
			break;
		}

		case 2:
		{
			float newPrice;
			std::cout << "Enter new price: $";
			std::cin >> newPrice;
			m_products[index].SetPrice(newPrice); // TODO: Replace me
			break;
		}

		case 3:
		{
			int newQuantity;
			std::cout << "Enter new quantity: ";
			std::cin >> newQuantity;
			m_products[index].SetQuantity(newQuantity); // TODO: Replace me
			break;
		}
	}

	std::cout << std::endl << "Product updated." << std::endl;

	Utility::Menu::Pause();
}

void Program::Menu_DeleteProduct()
{
	Utility::Menu::Header("DELETE PRODUCT");

	DisplayProducts(m_products);

	int index;
	index = Utility::Menu::GetValidChoice(0, m_products.size() - 1, "Enter ID of item to remove: "); // TODO: Replace me

	m_products.erase(m_products.begin() + index); // TODO: Replace me

	std::cout << std::endl << "Product removed." << std::endl;

	Utility::Menu::Pause();
}

void Program::DisplayProducts(const std::vector<Product>& productList) // TODO: Replace me
{
	std::cout << std::left;
	std::cout << std::setw(5) << "ID"
		<< std::setw(50) << "NAME"
		<< std::setw(10) << "PRICE"
		<< std::setw(10) << "QUANTITY"
		<< std::endl << std::string(80, '-') << std::endl;
	for (size_t i = 0; i < m_products.size(); i++) // TODO: Replace me
	{
		std::cout << std::setw(5) << i;
		m_products[i].Display(); // TODO: Replace me
	}
}
