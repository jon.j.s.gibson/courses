#ifndef _OPERATION
#define _OPERATION

#include <string>

/*
* From: https://qz.com/819245/data-scientist-cathy-oneil-on-the-cold-destructiveness-of-big-data
One of them is, you call up customer service, and from your phone number they infer your value as a customer. 
If you�re a low-value customer you will wait on hold forever. If you�re a high-value customer you get to a
customer representative immediately. And if you�re low-value, you�re likely to be told by the rep, �Oh you�re 
low-value, you�re not going to get what you want.� That happens. I didn�t even know the rep knows your score,
but turns out that in that system they actually do. And they can say, �I�m not going to give you what you want.�
*/

struct Call
{
	std::string customerName;
	int customerValue;
	bool resolved;
	int minutesOnCall;

	void Setup(std::string newName, int newScore)
	{
		customerName = newName;
		customerValue = newScore;
		resolved = false;
		minutesOnCall = 0;
	}

	void Wait(int minutes)
	{
		minutesOnCall += minutes;
	}
};

#endif