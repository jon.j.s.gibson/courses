#ifndef _STRINGS
#define _STRINGS

// C++ Library includes
#include <string>
#include <sstream>
#include <vector>

namespace Utility
{

//! A class with std::string conversion and useful std::string operations
class StringUtil
{
    public:
    template <typename T>
    static std::string ToString( const T& value );
    static std::string BoolToText( const bool value );
    static int StringToInt( const std::string& str );
    static unsigned int StringToUInt( const std::string& str );
    static float StringToFloat( const std::string& str );
    static std::string ToUpper( const std::string& val );
    static std::string ToLower( const std::string& val );
    static std::string ColumnText( int colWidth, const std::string& text );
    static bool Contains( std::string haystack, std::string needle, bool caseSensitive = true );
    static int Find( std::string haystack, std::string needle );
    static std::string Replace( std::string fulltext, std::string findme, std::string replacewith );
    static std::vector<std::string> Split( std::string str, std::string delim );
    static std::vector<std::string> CsvSplit( std::string str, char delim );
};

template <typename T>
std::string Utility::StringUtil::ToString( const T& value )
{
    std::stringstream ss;
    ss << value;
    return ss.str();
}

} // End of namespace

#endif
