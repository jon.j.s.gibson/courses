#ifndef _PROGRAM
#define _PROGRAM

#include "../DataStructure/Queue/ArrayQueue.h"
#include "../DataStructure/Queue/LinkedQueue.h"
#include "../DataStructure/Stack/ArrayStack.h"
#include "../DataStructure/Stack/LinkedStack.h"
#include "../DataStructure/LinkedList/LinkedList.h"
#include "Message.h"
#include "Sandwich.h"

#include <vector> // temporary
#include <stack> // temporary
#include <queue> // temporary
#include <string>
#include <fstream>

class Program
{
public:
	void Run();
	void SetDataPath(std::string path);

private:
  void ReadMessages();
  void ProcessMessages();
  void SaveResults();

	std::queue<Message> m_messageQueue;    // TODO: UPDATE ME! Use either your ArrayQueue or LinkedQueue instead.

	std::stack<std::string> m_workingStack;    // TODO: UPDATE ME! Use either your ArrayStack or LinkedStack instead.

	std::vector<Sandwich> m_results;    // TODO: UPDATE ME! Use your LinkedList here

	std::string m_dataPath;
	std::ofstream m_log;
};

#endif
