var indexSectionsWithContent =
{
  0: "abcdefghilmnopqrstuw~",
  1: "adilmnpqst",
  2: "cdeu",
  3: "adilmnpqrst",
  4: "abcdefghilmnopqrstw~",
  5: "cfmnst",
  6: "loqs",
  7: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "related",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Friends",
  7: "Pages"
};

