# -*- mode: org -*-

#+TITLE: CS 134 Semester Project, Fall 2023
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----

* What to turn in

The following items should be *included* within your Semester Project
replit projet...

- *Code:* Make sure code is included in your replit project.
  - CODE SHOULD BUILD, at minimum! I'd rather have incomplete code that builds than a mass of "complete" code that doesn't build.
- *Design doc:* You should write a *Design Document*, and have it saved in your replit project as well.
  - Please upload as a *PDF file*! I only run Linux computers at home, and I don't pay for Microsoft Word, so those files maybe garbled if you give me the MS Word file. :) PDF is better.

** Design document

Your design document should be concise, but contain the following information:

1. *Instructions:* Write instructions for core program functionality,
   written in a way that a brand-new user to your program will understand. Make sure to cover all major features.
2. *Citations:* If you utilized code from elseware, list sources on the citations page.
3. *Postmortem:* Reflect on your work on this program (this is what we do on the software development side). Answer the following questions:
  - What went well?
  - What could have gone better?
  - What steps are you going to take next time (i.e., next project) to help mitigate the issues that you experienced?

** Code features

You should be utilizing each of the topics learned in this class, in a way that makes sense from a design perspective.
Your program should include at least one of each of the following. See the *Cookbook* section of the document
for code and how each of these features might be used.

- *Console input and output*
- *Variables*
- *If statement (or if/else if, or if/else)*
  This may include navigating to different parts of the program based on a user's menu selection.
- *While loop*
  This may include the *program loop*, *input validation*, etc.
- *List/vector*
  This will usually be the program's main data that is worked with in the program.

Additionally:

- *Function*
  Use at least one function from the *Cookbook*, or write your own. You can also ask the instructor for help
  with how to create a custom function.
- *Extra credit - Class/struct*
  You can earn some extra credit by utilizing a class (Python) or struct (C++) to organize related variables together.


** User interface and user experience

Program design should be easy to use for someone who has never seen the program before. Some tips:
- Make sure you tell the user what kind of input you're expecting before using an input/cin statement.
- Display confirmation messages when the user is expecting something to change and it changed (or error messages if something went wrong.)
- Validate user input to make sure they're not selecting something that will crash the program.


-----

* Program ideas

You can come up with a program on your own if you'd like, but here are some ideas that you can use or base your own ideas off of.

** Movie theater kiosk

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Create a list of ticket prices, such as for different ages or different times of day.
The program is where a user orders tickets, so they should be able
to choose several different types of tickets at a time.
The program should also be able to calculate a total price for
each of the tickets.

- *Variable:* User's menu choice selection.
- *If:* Check if user selected option 0 to finish checkout, ELSE they chose to add a ticket.
- *While loop:* Program loop
- *List/vector:* Could have a list of prices, or if using class/struct, a list of Ticket items.
- *Function:* Could use the =DisplayMenu=, =GetPricePlusTax=, =GetInput= functions.
- *Class/struct (optional):* Could have a Ticket class that contains a ticket "name" (Child) and "price" (7.50).

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

Example output:

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
MOVIE THEATER

Currently playing: My Neighbor Totoro

---------------------------------------
Your ticket total: $0

Tickets:
1. Child....... $7.50
2. Adult....... $9.50
3. Senior...... $6.25

0. Finish checkout

>> 1
Added 1 child ticket

---------------------------------------
Your ticket total: $7.50

Tickets:
1. Child....... $7.50
2. Adult....... $9.50
3. Senior...... $6.25

0. Finish checkout

>> 2
Added 1 adult ticket

---------------------------------------
Your ticket total: $17.00

Tickets:
1. Child....... $7.50
2. Adult....... $9.50
3. Senior...... $6.25

0. Finish checkout

>> 3
Added 1 senior ticket

---------------------------------------
Your ticket total: $23.25

Tickets:
1. Child....... $7.50
2. Adult....... $9.50
3. Senior...... $6.25

0. Finish checkout

>> 0

RECEIPT
Subtotal: $23.25
Tax:      9.1%
Total:    $25.37
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML





#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

** Dice game

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

For a simple dice game, you could have a "list" of dice rolls. Perhaps you and the computer player each
roll 3 dice, and whoever has a larger sum wins the round.

- *Variable:* Store the game's round #, store how many wins for the human player vs. computer player. Store the human player's name.
- *If:* If one winner has 2 wins more than the other, then they win the game.
- *While loop:* Use a while loop to keep the game going until there is a winner.
- *List/vector:* Store the 3 die rolls in a list - one for player, one for computer.
- *Function:* Use the random number functions to generate the die rolls.
- *Class/struct (optional):* Could use a basic class/struct to store player name and score; there will be a human player and a computer player.

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

Example output:

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
DICE GAME

Enter your name: Bob

Bob vs. Computer

Round 1
Bob rolls 2, 4, 5; Sum is 11.
Computer rolls 1, 5, 2; Sum is 8.
Bob wins round 1!

SCORE: Bob: 1, Computer: 0

Round 2
Bob rolls 4, 3, 1; Sum is 8.
Computer rolls 3, 4, 1; Sum is 8.
Tie!

SCORE: Bob: 1, Computer: 0

Round 3
Bob rolls 5, 2, 5; Sum is 12.
Computer rolls 2, 1, 4; Sum is 7.
Bob wins round 2!

SCORE: Bob: 2, Computer: 0

Bob wins!
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML





-----

* Cookbook

** Program loop
Use a program loop to keep your program running. Within the while loop you should have a menu displayed,
get the user's input, and perform some action based on their input.

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:
#+BEGIN_SRC python :class python
  running = True
  while ( running ):
      # 1. Display menu
      # 2. Get user input
      # 3. Use if/elif statements to
      # do an action based on input
  # End of while loop
  print( "Goodbye." )
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

C++:
#+BEGIN_SRC cpp :class cpp
  bool running = true;
  while ( running )
  {
    // 1. Display menu
    // 2. Get user input
    // 3. Use if/else if statements to
    // do an action based on the input
  }
  cout << "Goodbye." << endl;
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML





#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** Display menu

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:
#+BEGIN_SRC python :class python
  def DisplayMenu():
      print( "0. Quit" )
      print( "1. ActionA" )
      print( "2. ActionB" )
      print( "3. ActionC" )
#+END_SRC

Within the main program you would call this function like this:

=DisplayMenu()=

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

C++:
#+BEGIN_SRC cpp :class cpp
  void DisplayMenu()
  {
    cout << "0. Quit" << endl;
    cout << "1. ActionA" << endl;
    cout << "2. ActionB" << endl;
    cout << "3. ActionC" << endl;
  }
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML

Within the main program you would call this function like this:

=DisplayMenu();=





#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** Input validation

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:
#+BEGIN_SRC python :class python
  def GetInput( min, max ):
      choice = int( input( "Enter choice: " ) )
      while ( choice < min or choice > max ):
          choice = int( input( "Invalid, try again: " ) )
      return choice
#+END_SRC

Let's say your menu has options 0 through 4. You could call this function like:

=menu_choice = GetInput( 0, 4 )=

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

C++:
#+BEGIN_SRC cpp :class cpp
  int GetInput( int min, int max )
  {
    int choice;
    cout << "Enter choice: ";
    cin >> choice;

    while ( choice < min || choice > max )
    {
      cout << "Invalid, try again: ";
      cin >> choice;
    }

    return choice;
  }
#+END_SRC

Let's say your menu has options 0 through 4. You could call this function like:

=int menu_choice = GetInput( 0, 4 );=

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML





#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** Calculate price plus tax

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:
#+BEGIN_SRC python :class python
  def GetPricePlusTax( price, taxPercent ):
      return price + ( price * taxPercent / 100 )
#+END_SRC

Given example variables =item_price= and =ks_tax= in your main program, this function would be called like:

=total_price = GetPricePlusTax( item_price, ks_tax )=

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

C++:
#+BEGIN_SRC cpp :class cpp
  float GetPricePlusTax( price, taxPercent )
  {
    return price + ( price * taxPercent / 100 );
  }
#+END_SRC

Given example variables =item_price= and =ks_tax= in your main program, this function would be called like:

=float total_price = GetPricePlusTax( item_price, ks_tax );=

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML





#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** Display contents of list/vector

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:
#+BEGIN_SRC python :class python
  def DisplayList( my_list ):
      for i in range( len( my_list ) ):
          print( str( i ) + ". " + my_list[i] )
#+END_SRC

Given an example list named =prices= in your main program,
you would call this function like this:

=DisplayList( prices )=

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

C++:
#+BEGIN_SRC cpp :class cpp
  void DisplayList( vector<TYPE> my_list )
  {
    for ( size_t i = 0; i < my_list.size(); i++ )
    {
      cout << i << ". " << my_list[i] << endl;
    }
  }
#+END_SRC

Replace "=TYPE=" with the type of data in your vector (e.g., =vector<string>=, =vector<float>=, etc.).

Given an example list named =prices= in your main program,
you would call this function like this:

=DisplayList( prices );=


#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML



#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** Get random number within a range

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:
#+BEGIN_SRC python :class python
  # Need this at top of file
  import random

  # In main program...
  die_roll = random.randint( 1, 20 )
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

C++:
#+BEGIN_SRC cpp :class cpp
  // Make sure these are included to use random numbers
  #include <cstdlib>
  #include <ctime>
  using namespace std;

  void InitRandomizer()
  {
    srand( time( NULL ) );
  }

  int GetRandom( int min, int max )
  {
    return min + rand() % ( max - min + 1 );
  }
#+END_SRC

At the start of =main()=, make sure to call the InitRandomizer function:

=InitRandomizer();=

Then, any time you need a random number, call GetRandom:

=int result = GetRandom( 1, 20 );=


#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML



-----

* Simple example programs

These programs aren't full programs that would meet all of the requirements of this project,
but you can use them for reference on how different things might work.

You can see the example programs here: https://replit.com/@rsingh13/Simple-programs

-----
