# -*- mode: org -*-

It feels weird to start a collection of notes (or a "textbook")
without some sort of welcome, though at the same time I know that
people are probably /not/ going to read the introduction
(Unless I put some cute art and interesting footnotes, maybe.)

I think that I will welcome you to my notes by addressing anxiety.

*Belonging*

Unfortunately there is a lot of bias in STEM fields and over
decades there has been a narrative that computer science is /for/
a certain type of person - antisocial, nerdy, people who started coding
when they were 10 years old.

Because of this, a lot of people who don't fit this description can
be hesitant to get into computers or programming because they don't
see people like themselves in media portrayals. Or perhaps previous
professors or peers have acted like you're not a /real programmer/
if you didn't start programming as a child

#+ATTR_HTML: :class hint
#+NAME: content
#+BEGIN_HTML
If you want to learn about coding, then you belong here.

There are no prerequisites.
#+END_HTML

I will say from my own experience, I know developers who fell in
love with programming by accident as an adult after having to take
a computer class for a /different degree/. I know developers who
are into all sorts of sports, or into photography, or into fashion.
There is no specific "type" of programmer. You can be any religion,
any gender, any color, from any country, and be a programmer.

#+CAPTION: Don't be like Gatekeeper Baby. You can begin coding at any age!
#+ATTR_LATEX: :width 300px
[[file:images/u00_WelcomeAndSetup_gatekeepingbaby.png]]




#+BEGIN_LATEX
\vspace{0.2cm}
#+END_LATEX

*Challenge*

Programming can be hard sometimes. There are many aspects of learning
to write software (or websites, apps, games, etc.) and you *will*
get better at it with practice and with time. But I completely understand
the feeling of hitting your head against a wall wondering /why won't this work?!/
and even wondering /am I cut out for this?!/ - Yes, you are.



I will tell you right now, I /have/ cried over programming assignments,
over work, over software. I have taken my laptop with me to a family
holiday celebration because I /couldn't figure out this program/
and I /had to/ get it done!!

All developers struggle. Software is a really unique field. It's very
intangible, and there are lots of programming languages, and all sorts
of tools, and various techniques. Nobody knows everything, and there's
always more to learn.


Just because something is /hard/ doesn't mean that it is /impossible/.

[[file:images/u00_WelcomeAndSetup_justwork.png]]

It's completely natural to hit roadblocks. To have to step away from
your program and come back to it later with a clear head. It's natural
to be confused. It's natural to /not know/.

But some skills you will learn to make this process smoother are
how to plan out your programs, test and verify your programs,
how to phrase what you don't know as a question, how to ask for help.
These are all skills that you will build up over time.
Even if it feels like you're not making progress, I promise that you are,
and hopefully at the end of our class you can look back to the start of it
and realize how much you've learned and grown.


*Learning*

First and foremost, I am here to help you *learn*.

My teaching style is influenced on all my experiences throughout
my learning career, my software engineer career, and my teaching career.

I have personally met teachers who have tried to /scare me away/
from computers, I've had teachers who really encouraged me,
I've had teachers who barely cared, I've had teachers who made
class really fun and engaging.

I've worked professionally in software and web development, and
independently making apps and video games. I know what it's like
to apply for jobs and work with teams of people and experience
a software's development throughout its lifecycle.

And as a teacher I'm always trying to improve my classes -
making the learning resources easily available and accessible,
making assignments help build up your knowledge of the topics,
trying to give feedback to help you design and write good programs.

As a teacher, I am not here to trick you with silly questions
or decide whether you're a /real programmer/ or not;
I am here to help guide you to learn about programming,
learn about design, learn about testing, and learn how to teach yourself.

#+ATTR_HTML: :width 200px
#+ATTR_LATEX: :width 200px
#+ATTR_ORG: :width 100px
[[file:images/u00_WelcomeAndSetup_buildingblocks.png]]


*Roadmap*

#+ATTR_HTML: :width 600px
#+ATTR_LATEX: :width 300px
#+ATTR_ORG: :width 100px
[[file:images/u00_WelcomeAndSetup_roadmap.png]]

When you're just starting out, it can be hard to know what all
you're going to be learning about. I have certainly read course
descriptions and just thought to myself "I have no idea what
any of that meant, but it's required for my degree, so I guess
I'm taking it!"

Here's kind of my mental map of how the courses I teach work:

- CS 200: Concepts of Programming with C++ :: You're learning the language. Think of it like actually
  learning a human language; I'm teaching you words and
  the grammar, and at first you're just parroting what I say,
  but with practice you'll be able to build your own sentences.

- CS 235 Object-Oriented Programming with C++ :: You're learning more about software development practices, design, testing, as well as more advanced object oriented concepts.

- CS 250 Basic Data Structures with C++ :: You're learning about data, how to store data, how to assess how efficient algorithms are. Data data data.

In addition to learning about the language itself, I also try to
sprinkle in other things I've learned from experience that I think
you should know as a software developer (or someone who codes for
whatever reason), like

- How do you validate that what you wrote /actually works?/ (Spoilers: How to write tests, both manual and automated.)
- What tools can you use to make your programming life easier? (And are used in the professional world?)
- How do you design a solution given just some requirements?
- How do you network in the tech field?
- How do you find jobs?
- What are some issues facing tech fields today?

Something to keep in mind is that, if you're studying *Computer Science*
as a degree (e.g., my Bachelor's degree is in Computer Science),
technically that field is about /"how do computers work?"/,
not about /"how do I write software good?"/
but I still find these topics important to go over.


#+ATTR_LATEX: :width 250px
[[file:images/u00_WelcomeAndSetup_search.png]]

That's all I can really think of to write here. If you have any questions, let me know. Maybe I'll add on here.


#+BEGIN_LATEX
\vspace{0.2cm}
#+END_LATEX

*What is this weird webpage/"book"?*

In the past I've had all my course content available on the web on separate webpages.
However, maintaining the HTML, CSS, and JS for this over time is cumbersome. Throughout 2023 I've been
adapting my course content to /emacs orgmode/ documents, which allows me to export the course
content to HTML and PDF files. I have a few goals with this:

1. All course information is in one singular place
2. At the end of the semester, you can download the entire course's "stuff" in a single PDF file
   for easy referencing later on
3. Hopefully throughout this semester I'll get everything "moved over" to orgmode,
   and in Summer/Fall 2024 I can have a physical textbook printed for the courses,
   which students can use and take notes in so as to have most of their course
   stuff in one place as well
4. It's important to me to make sure that you have access to the course content
   even once you're not my student anymore - this resource is available publicly
   online, whether you're in the course or not. You can always reference it later.

I know a lot of text can be intimidating at first, but hopefully it will be less
intimidating as the semester goes and we learn our way around this page.


#+BEGIN_LATEX
\vspace{0.2cm}
#+END_LATEX
