# -*- mode: org -*-

*An empty C++ program*

The bare-minimum C++ program looks like this:

#+BEGIN_SRC cpp :class cpp
  int main()
  {

    return 0;
  }
#+END_SRC

No matter what all is in your program, your *compiler*
is going to expect that a =main= function exists,
and this function acts as the *starting point* of
a C++ program.

The opening curly-brace ={= and closing curly-brace =}=
denote the /start/ and /end/ of the =main= function.
The curly-braces and any code within is known as a *code-block*.
Program instructions will go within this code-block.

At the end of the =main= function we have =return 0;=.
In this context, it means "return (exit) the program with a status of 0",
with that basically meaning "return with no errors". If you ran into
an error while the program was running, you might do something like
=return 123;= instead, to mark the area with an "error code" (123),
which you can then find in the code.

For our programs starting off, we will be putting our instruction code
after the opening curly-brace ={= and before the =return 0;=, but the code
given above is the /absolute, bare-minimum required/ to write a C++ program.


*An introduction to syntax*

#+ATTR_HTML: :class hint
#+NAME: content
#+BEGIN_HTML
*Context:* What is /syntax/?

#+BEGIN_QUOTE
syntax, noun

The way in which linguistic elements (such as words) are put together to form constituents (such as phrases or clauses)
#+END_QUOTE

From the [[https://www.merriam-webster.com/dictionary/syntax][Merriam-Webster Dictionary]].
#+END_HTML




C++, Java, C#, and other "C-like" languages follow similar syntax rules.

*Lines of code:* A code statement ends with a semi-colon. Statements are single
commands, like using =cout= ("console out") to display text to the screen,
assigning a value to a variable, and other simple operations.

#+BEGIN_SRC cpp :class cpp
  string state;               // Variable declaration
  cout << "Enter state: ";    // Display text to screen
  cin >> state;               // Getting input from keyboard
  cout << state << endl;      // Displaying text to the screen
#+END_SRC

*Code blocks:* There are certain types of instructions that /contain/ additional code.
If statements, for example, contain a set of instructions to execute /only if/ the *condition* given
evalutes to =true=. Any time an instruction /contains/ additional instructions, we use
opening and closing curly braces ={ }= to contain this internal code. Note that an
if statement and other structures that /contain/ code don't end with a semicolon =;=.

*Comments:* Comments are notes left in the code by humans for humans. They can
help us remember what our code was doing later on, or help guide someone else reading
through our code. There are two ways we can write comments in C++.

If we use =//= then all text afterwards will be a comment. This is a single-line comment.

If we use =/*= then all text will be a comment, only ending once we reach a =*/=. This is a multi-line comment.

#+BEGIN_SRC cpp :class cpp
  string state;               // Variable declaration

  /*
    Next we're going to search for this state
    in the list of all states that match xyz criteria...
  */
#+END_SRC

*Syntax errors and logic errors*

#+ATTR_LATEX: :width 200px
[[file:images/c2_u01_debugging2.png]]

*Syntax errors* are errors in the language rules of your program code.
This include things like *typos*, *misspellings*, or just writing something
that isn't valid C++ code, such as forgetting a =;= at the end of some instructions.

Although they're annoying, syntax errors are probably the "best" type of error
because the program won't build while they're present. Your program won't build
if you have syntax errors.


*Logic errors* are errors in the programmer's logic. This could be something like
a wrong math formula, a bad condition on an if statement, or other things where
the programmer thinks /thing A/ is going to happen, but /thing B/ happens instead.

These don't lead to build errors, so the program can still be run, but it may result
in the program crashing, or invalid data being generated, or other problems.

*Minimizing bugs and debugging time*

#+ATTR_LATEX: :width 200px
[[file:images/coding-strategy.png]]

- Write a few lines of code and build after every few lines - this will help you detect
  syntax errors early.
- DON'T write the "entire program" before ever doing a build or a test - chances are
  you've written in some syntax errors, and now you'll have a long list of errors
  to sift through!
- If you aren't sure where an error is coming from, try /commenting out/ large chunks
  of your program until it's building again. Then, you can /un-comment-out/ small
  bits at a time to figure out where the problem is.

