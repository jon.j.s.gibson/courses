# -*- mode: org -*-

*=practice4_cinstream=:*

*Starter code:*

#+BEGIN_SRC cpp :class cpp
// PROGRAM: Console input and variable arithmetic practice

// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  // TODO: Declare a float variable for `width`.


  // TODO: Declare a float variable for `length`.


  // TODO: Use `cout` to display a prompt - tell the user - "Enter a width: "


  // TODO: Use `cin` to get keyboard input, store in `width` variable.


  // TODO: Use `cout` to display a prompt - tell the user - "Enter a length: "


  // TODO: Use `cin` to get keyboard input, store in `length` variable.


  // TODO: Declare a float variable `perimeter`, assign it the result of `2 * width + 2 * length`.


  // TODO: Declare a float variable `area`, assign it the result of `width * length`.


  // TODO: Use `cout` to display "Perimeter:", and then the value of the `perimeter` variable.


  // TODO: Use `cout` to display "Area:", and then the value of the `area` variable.


  // Return 0 means quit program with no errors, in this context.
  return 0;
}
#+END_SRC

For this program you will do all the steps to create variables to store a rectangle's
width and length, display messages to the screen, get input from the keyboard,
do the math, and display the result. You can always look at the previous programs
for reference as well.

The steps are:

1. *Declare* a float variable named =width=
2. *Declare* a float variable named =length=
3. *Display* a message to the user, saying "Enter a width:"
4. *Get input* from the keyboard, store the result in the =width= variable
5. *Display* a message to the user, saying "Enter a length:"
6. *Get input* from the keyboard, store the result in the =length= variable
7. *Declare* a float variable named =perimeter=, assign it the result of =2 * width + 2 * length=
8. *Declare* a float variable named =area=, assign it the result of =width * length=
9. *Display* "Perimeter:" and then the value of the =perimeter= variable
10. *Display* "Area:" and then the value of the =area= variable

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
$ g++ perimeter.cpp
$ ./a.out
Enter a width: 4
Enter a length: 3
Perimeter: 14
Area: 12
#+END_SRC
