// PROGRAM: Practice with function - NO input, YES output
#include <iostream>
#include <string>
using namespace std;

/*
 *Example program output:*
 ------------------------------------------
 Tax rate is: 9.61
 ------------------------------------------

 *Function:*
 Implement the GetTaxPercent function below.
 This function has NO input parameters so its parentheses ( ) will be empty
 and it has a float output RETURN so its return type is `float`.

 Within this function, return the number 9.61.

 *main:*
 Within main, set the `taxRate` variable to the result from calling the `GetTaxPercent` function.
 Afterward the tax rate will be displayed to the screen.
*/

// -- FUNCTION DEFINITION GOES HERE -------------------------------------------
/**
   GetTaxPercent function
   NO input parameters / YES output return
   @return   The tax percent is returned.
   The tax rate is 9.61.
*/
// TODO: Define the GetTaxPercent function here


// -- MAIN PROGRAM FUNCTION ---------------------------------------------------
int main()
{
  float taxRate;

  // TODO: Call the GetTaxPercent function

  cout << "Tax rate is: " << taxRate;

  return 0;
}
