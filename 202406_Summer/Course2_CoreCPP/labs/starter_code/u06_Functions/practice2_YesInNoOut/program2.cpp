// PROGRAM: Practice with function - YES input, NO output
#include <iostream>
#include <iomanip>
using namespace std;

/*
 *Example program output:*
 ------------------------------------------
 Enter an amount of money: 5.3
 $5.30
 ------------------------------------------

 *Function:*
 Implement the DisplayFormattedUSD function definition below.
 This function HAS input parameters so its parentheses ( ) will contain: `float price`
 and it has NO output return so its return type will be `void`.

 Within this function, set `cout << fixed << setprecision(2);`,
 and then use cout to display the `price` passed in as a dollar amount.

 *main:*
 Within main, call the DisplayFormattedUSD function.
 The function call will just be the function name and the `user_money`
 variable passed in as an argument. The function call will have a semicolon ; at the end.
*/

// -- FUNCTION DEFINITION GOES HERE -------------------------------------------
/**
   DisplayFormattedUSD function
   YES input parameters / NO output return
   @param price    The price to display to the screen
   Set up cout to have fixed and setprecision(2) formatting.
   Then, use cout to display the `price` parameter.
*/
// TODO: Implement DisplayFormattedUSD function
void DisplayFormattedUSD( float price )
{
  cout << fixed << setprecision( 2 );
  cout << "$" << price;
}

// -- MAIN PROGRAM FUNCTION ---------------------------------------------------
/**
   main function
   Program asks the user for an amount of money.
   Afterwards, call the DisplayFormattedUSD function to display
   the user's money in USD format.
*/
int main()
{
  float user_money;
  cout << "Enter an amount of money: ";
  cin >> user_money;

  // TODO: Call DisplayFormattedUSD

  return 0;
}
