# -*- mode: org -*-

*Graded program: Room builder*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter room's width: 5
Enter room's length: 7
RESULT:
 * width:     5
 * length:    7
 * area:      35
 * perimeter: 24
#+END_SRC

Within the *=Room.h=* file, a Room struct has been declared:

#+BEGIN_SRC cpp :class cpp
struct Room
{
  float width;
  float length;
  float area;
  float perimeter;
};
#+END_SRC

You'll work in the *=room_builder.cpp=* file. Within the StudentCode function,
it starts by creating a Room object, then it has a TODO comment, and then it returns the Room
object at the end:

#+BEGIN_SRC cpp :class cpp
Room StudentCode( float width, float length )
{
  Room my_room;

  // TODO: Finish setting up my_room

  return my_room;
}
#+END_SRC

Note that this function has access to a =float width= and a =float length= variable,
which you will be using when you write your code.

At the TODO line, work with the =my_room= variable to set its member variables.

| Member variable     | Assign to...             |
|---------------------+--------------------------|
| =my_room.width=     | =width=                  |
| =my_room.length=    | =length=                 |
| =my_room.area=      | =width * length=         |
| =my_room.perimeter= | =2 * width + 2 * length= |

Afterwards you can run the program to manually check your work
and you can run the AUTOMATED TESTS to verify that all the tests pass.
