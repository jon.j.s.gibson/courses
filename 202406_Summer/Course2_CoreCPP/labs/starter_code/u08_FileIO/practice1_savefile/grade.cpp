// PROGRAM: Practice saving text files

// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <fstream>    // Library that contains `ifstream` and `ofstream`
#include <iomanip>    // Library for formatting; `setprecision`
using namespace std;  // Using the C++ STanDard libraries


/*
 *Example program output:*
 ------------------------------------------
 = GRADE CRUNCHER =
 How much did you score on test 1 (out of 50?): 45
 How much did you score on test 2 (out of 25?): 20
 How much did you score on test 3 (out of 40?): 10
 Info saved to score.txt
 ------------------------------------------


 *Example input file:*
 ------------------------------------------
 45.00 out of 50.00 (90.00%)
 20.00 out of 25.00 (80.00%)
 10.00 out of 40.00 (25.00%)
 ------------------------------------------

 *Program requirements:*
 For this program the user will enter 3 test scores (already implemented).
 Next, ratios are calculated for each score, to give a grade percent for each one (already implemented).

 After this, we want to *create an output file* and write the test statistics out to it.

 Do the following:

 1. Create an output file stream (=ofstream=) variable named =output=.
 2. Use the ofstream's `open` function (https://cplusplus.com/reference/fstream/ofstream/open/) to open the file "score.txt".
 3. Set up text formatting to `fixed` and `setprecision(2)`.
 Instead of applying this to the `cout` operation, it should be applied to the `output` stream: `ofstream << fixed << setprecision(2);`

 4. For each of the scores, write the following to the `output` file.
 For example:
   [NUMBER]. [TESTSCORE] out of [TESTTOTAL] ([RATIO]%)
 Replace the items in [] with the appropriate variables.

 When the program is run the details will be calculated and written to an output file.
 Check the "score.txt" file on your computer after running the program to make sure
 its output looks like the example above.
*/

int main()
{
  float test1total = 50, test2total = 25, test3total = 40;
  float test1score, test2score, test3score;
  float ratio1, ratio2, ratio3;

  cout << "= GRADE CRUNCHER =" << endl;

  // Get user's test scores
  cout << "How much did you score on test 1 (out of " << test1total << "?): ";
  cin >> test1score;
  cout << "How much did you score on test 2 (out of " << test2total << "?): ";
  cin >> test2score;
  cout << "How much did you score on test 3 (out of " << test3total << "?): ";
  cin >> test3score;

  // Calculate score ratios
  ratio1 = test1score / test1total * 100;
  ratio2 = test2score / test2total * 100;
  ratio3 = test3score / test3total * 100;

  // TODO: Add your code here.





  cout << "Info saved to score.txt" << endl;

  return 0;
}
