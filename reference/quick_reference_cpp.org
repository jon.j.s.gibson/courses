# -*- mode: org -*-

* Starter C++ programs

*Without arguments:*

#+BEGIN_SRC cpp :class cpp
  int main()
  {
    return 0;
  }
#+END_SRC

*With arguments:*

#+BEGIN_SRC cpp :class cpp
  int main( int argCount, char* args[] )
  {
    return 0;
  }
#+END_SRC

* Variables and named constants

*Declare a variable*

=DATATYPE VARIABLENAME;=

#+BEGIN_SRC cpp :class cpp
  int myNumber;
  string myString;
  float myFloat;
#+END_SRC

*Declare a variable and initialize it*

=DATATYPE VARIABLENAME = VALUE;=

#+BEGIN_SRC cpp :class cpp
  int myNumber = 10;
  string myString = "Hello!";
  float myFloat = 9.99;
  char myChar = 'a';
  bool myBool = true;
#+END_SRC

*Declare a named constant*

=const DATATYPE NAME = VALUE;=

#+BEGIN_SRC cpp :class cpp
  const int NUMBER = 10;
  const string STATE = "Missouri";
#+END_SRC

*Assign a value to a variable that has already been declared*

=VARIABLENAME = VALUE;=

#+BEGIN_SRC cpp :class cpp
  myNumber = 100;
  myString = "Goodbye";
#+END_SRC

*Copy a value from one variable to another*

=UPDATEDVAR = COPYME;=

#+BEGIN_SRC cpp :class cpp
  vipStudent = student3;
#+END_SRC

* Console input and output

*Output text to the console*

=cout << ITEM;=

You can mix variables and string literals, as long as the stream operator =<<= is between each item.

#+BEGIN_SRC cpp :class cpp
  cout << "Text";
  cout << myVariable;
  cout << "Label: " << myVariable << endl;
#+END_SRC

*Get input from the keyboard and store it in a variable*

=cin >> VARIABLENAME;=

#+BEGIN_SRC cpp :class cpp
  cin >> myInteger;
  cin >> myString;
#+END_SRC

*Get a full line of text (with spaces) and store in a variable (string only)*

=getline( cin, VARIABLENAME );=

#+BEGIN_SRC cpp :class cpp
  getline( cin, myString );
#+END_SRC

Note that if you have a =cin >>= statement immediately before the getline statement, your input will get skipped.
In this case you need to add a =cin.ignore()= statement:

#+BEGIN_SRC cpp :class cpp
  cin >> myNumber;
  cin.ignore();
  getline( cin, myString );
#+END_SRC


* Branching

*If statement*

If the condition evaluates to true, then execute the code within the if statement. Otherwise, skip that code completely.

#+BEGIN_SRC cpp :class cpp
  if ( a == 1 )
  {
  }
#+END_SRC

*If/else statement*

If the condition from if statement results to false, then the code in the else case is executed instead. No condition is written with the else case. 

#+BEGIN_SRC cpp :class cpp
  if ( a == 1 )
  {
  }
  else
  {
  }
#+END_SRC

*If/else if statement*

Checks each condition in order. If one is true, then subsequent =else if= statements are ignored.

#+BEGIN_SRC cpp :class cpp
  if ( a == 1 )
  {
  }
  else if ( a == 2 )
  {
  }
#+END_SRC

*If/else if/else statement*

Similar to above, but if all =if= and =else if= statements are false, then =else= is executed.

#+BEGIN_SRC cpp :class cpp
   if ( a == 1 )
   {
   }
   else if ( a == 2 )
   {
   }
   else
   {
   }
#+END_SRC


* Switch

=switch( VARIABLE ) { case VALUE: break; }=

#+BEGIN_SRC cpp :class cpp
  switch( myNumber )
  {
    case 1:
      cout << "It's one!" << endl;
      break;

    case 2:
      cout << "It's two!" << endl;
      break;

    default:
      cout << "I don't know what it is!" << endl;
  }
#+END_SRC

* Loops

*Looping with While Loops*

Runs 0 or more times, depending on whether the CONDITION is true.

=while ( CONDITION ) { }=

#+BEGIN_SRC cpp :class cpp
  while ( a < b )
  {
    cout << a << endl;
    a++;
  }
#+END_SRC

*Looping with Do...While Loops*

Runs /at least once/ and then continues looping if the condition is true.

=do { } while ( CONDITION );=

#+BEGIN_SRC cpp :class cpp
  do {
    cout << "Enter a number: ";
    cin >> input;
   } while ( input < 0 || input > max );
#+END_SRC

*Looping with For Loops*

Iterate some amount of times through the loop based on your counter variable and range.

=for ( INIT; CONDITION; UPDATE ) { }=

#+BEGIN_SRC cpp :class cpp
  for ( int i = 0; i < 10; i++ )
  {
    cout << i << endl;
  }
#+END_SRC

*Looping with Range-Based For Loops*

Iterates over all elements in a collection. Only moves forward, doesn't give access to index

=for ( INIT : RANGE ) { }=

#+BEGIN_SRC cpp :class cpp
  vector<int> myVec = { 1, 2, 3, 4 };
  for ( int element : myVec )
  {
    cout << element << endl;
  }
#+END_SRC


* Arrays and Vectors

*Declare a traditional C-style array*

When declaring a vanilla array, you need to set its size. You can either hard-code the size with a number:

#+BEGIN_SRC cpp :class cpp
  string arr[10];
#+END_SRC

Or use a named constant:

#+BEGIN_SRC cpp :class cpp
  const int TOTAL_STUDENTS;
  string students[TOTAL_STUDENTS];
#+END_SRC


*Declare an STL array object*
 Remember that you will need =#include <array>= at the top of your file to use the array object.

Declare an array object, passing in what the data type of it values are, and the size of the array:

#+BEGIN_SRC cpp :class cpp
  array<float, 3> bankBalances;
#+END_SRC

Use the array documentation (https://cplusplus.com/reference/array/array/) for info on its functions.

*Declare an STL vector object*

#+BEGIN_SRC cpp :class cpp
  vector<string> studentList;
#+END_SRC

Use the vector documentation (https://cplusplus.com/reference/vector/) for info on its functions.

*Initialize array/vector with an initializer list*

#+BEGIN_SRC cpp :class cpp
  string cats1[] = { "Kabe", "Luna", "Pixel", "Korra" };
  array<string,4> cats2 = { "Kabe", "Luna", "Pixel", "Korra" };
  vector<string> cats3 = { "Kabe", "Luna", "Pixel", "Korra" };
#+END_SRC

*Iterate over an array/vector*

#+BEGIN_SRC cpp :class cpp
  // C-style array:
  for ( int i = 0; i < TOTAL_STUDENTS; i++ )
  {
    cout << "index: " << i << ", value: " << students[i] << endl;
  }

  // STL Array and STL Vector:
  for ( size_t i = 0; i < bankBalances.size(); i++ )
  {
    cout << "index: " << i << ", value: " << students[i] << endl;
  }
#+END_SRC

=size_t= is another name for an =unsigned int=, which allows values of 0 and above - no negatives.

* File I/O

Make sure to have =#include <fstream>= in any files using =ifstream= or =ofstream=!

*Create an output file and write*

#+BEGIN_SRC cpp :class cpp
  ofstream output;
  output.open( "file.txt" );

  // Write to text file
  output << "Hello, world!" << endl;
#+END_SRC

*Create an input file and read*

#+BEGIN_SRC cpp :class cpp
  ifstream input;
  input.open( "file.txt" );
  if ( input.fail() )
    {
      cout << "ERROR: could not load file.txt!" << endl;
    }
  string buffer;

  // read a word
  input >> buffer;

  // read a line
  getline( input, buffer );
#+END_SRC


* Functions

*Prevent file duplication*

When creating .h/.hpp files, it is important to include these lines to prevent file duplication, which happens if you #include this file in more than one place.

#+BEGIN_SRC cpp :class cpp
  #ifndef _FILENAME_H // LABEL SHOULD BE UNIQUE FOR EACH .h FILE!
  #define _FILENAME_H

  // Code goes here

  #endif
#+END_SRC

*Function declaration*

A function declaration contains the function header and no body. It declares that the function exists and will be defined elseware. 

#+BEGIN_SRC cpp :class cpp
  void DisplayMenu();
  int Sum(int a, int b);
#+END_SRC

*Function definition*

A function definition defines how the function operates. It includes the function header and a function body, which has commands in it. 

#+BEGIN_SRC cpp :class cpp
  void DisplayMenu()
  {
    cout << "1. Deposit" << endl;
    cout << "2. Withdraw" << endl;
    cout << "3. Quit" << endl;
  }

  int Sum(int a, int b)
  {
    return a + b;
  }
#+END_SRC

*Calling a function*

Calling a function requires invoking the function's name and passing in any arguments. 

#+BEGIN_SRC cpp :class cpp
  DisplayMenu();

  int result = Sum( 1, 2 ); // Passing in literal values

  int num1 = 2;
  int num2 = 5;
  int result = Sum( num1, num2 ); // Passing in variable values
#+END_SRC

* Classes

*Declaring a class*

Remember that classes must end with =;= on their closing curly brace!

#+BEGIN_SRC cpp :class cpp
  class CLASSNAME
  {
      public:
      // Public members

      protected:
      // Protected members

      private:
      // Private members
  };
#+END_SRC


#+BEGIN_SRC cpp :class cpp
  class Student
  {
  public:
    // Constructor functions
    Student();
    Student( string name, float gpa );

    // Member functions
    void Setup( string name, float gpa );
    void Display();

  private:
    // Member variables
    string m_name;
    float m_gpa;
  };
#+END_SRC

*Defining a class member function (method)*

A class' member functions must be *declared* within the class declaration.
Then, you *define* the member function within a .cpp file.

=RETURNTYPE CLASSNAME::FUNCTIONNAME( PARAMETERS )=

#+BEGIN_SRC cpp :class cpp
  // Function definition
  void Student::Setup( string name, float gpa )
  {
    m_name = name;
    m_gpa = gpa;
  }
#+END_SRC

*Getter and Setter patterns*

It is best practice to make member variables *private* to protect
the integrity of the data. Private members cannot be accessed
outside of the function, so instead you'll have to create
interfacing functions to access (get) and mutate (set) data.

- Getter function, aka Accessor ::
  - Doesn't take in any inputs (parameters)
  - Returns the value of the member variable (return type should match the member variable)
  - Getter functions can be good for formatting data before returning it

#+BEGIN_SRC cpp :class cpp
  // Basic getter function
  float Student::GetGPA()
  {
    return m_gpa;
  }

  // Example of formatting before returning data
  string Person::GetFullName()
  {
    return m_lastname + ", " + m_firstname;
  }
#+END_SRC

- Setter function, aka Mutator ::
  - Takes in an input value for a new value for the member variable - parameter should have the same data type as the member variable
  - Doesn't return any data - void return type
  - Setter functions should include any needed error checking and reject invalid data

#+BEGIN_SRC cpp :class cpp
  // Basic setter function
  void Student::SetGPA( float newGpa )
  {
    m_gpa = newGpa;
  }

  // Example of data validity check
  void Student::SetGPA( float newGpa )
  {
    if ( newGpa < 0 || newGpa > 4 )
    {
      cout << "Error! Invalid GPA!" << endl;
      return; // Return without saving data
    }
    else
    {
      m_gpa = newGpa;
    }
  }
#+END_SRC

* Pointers

*Declare a pointer*

=DATATYPE* PTRNAME = nullptr;=

#+BEGIN_SRC cpp :class cpp
#+END_SRC

*Assign an address to a pointer*

=PTRNAME = &VARIABLE;=

#+BEGIN_SRC cpp :class cpp
  int * ptrInt = nullptr;
  string * ptrString = nullptr;
#+END_SRC

Pointer variables store addresses as their value. To get the address of a variable, prefix the variable name with the address-of operator =&=.

#+BEGIN_SRC cpp :class cpp
  int myInteger = 5;
  int * ptrInt = &myInteger;

  Student studentA;
  Student * ptrStudent = nullptr;
  ptrStudent = &studentA;
#+END_SRC


*Dereference a pointer*

#+BEGIN_SRC cpp :class cpp
  cout << *ptrName; // Display contents that pointer is pointing to
  cin >> *ptrName; // Overwrite contents that pointer is pointing to
#+END_SRC

*Dereference an object pointer and access a member variable*

=PTR->MEMBER=

#+BEGIN_SRC cpp :class cpp
  Student* newStudent = new Student;
  newStudent->name = "Hikaru";
  cout << "Student name: " << newStudent->name << endl;
#+END_SRC

*Check if pointer is pointing to an address*

#+BEGIN_SRC cpp :class cpp
  if ( ptrName != nullptr )
  {
    // Pointer is safe to use...
  }
#+END_SRC

* Memory management

*Allocate memory for a single variable*

=DATATYPE* PTRNAME = NEW DATATYPE;=

#+BEGIN_SRC cpp :class cpp
  int * num = new int;
  Node* newNode = new Node;
#+END_SRC

*Deallocate memory of a single variable*

=DELETE PTRNAME;=

#+BEGIN_SRC cpp :class cpp
  delete num;
  delete newNode;
#+END_SRC

*Allocate memory for an array*

=DATATYPE* PTRNAME = new DATATYPE[ SIZE ];=

#+BEGIN_SRC cpp :class cpp
  int * numList = new int[100];
  Node* nodeList = new Node[10];
#+END_SRC

*Deallocate memory of an array*

=DELETE [] PTRNAME;=

#+BEGIN_SRC cpp :class cpp
  delete [] numList;
  delete [] nodeList;
#+END_SRC

