# -*- mode: org -*-

If you're using a school lab computer they will already have the software we need set up. Use these instructions for setting up
the programs on your own computer.

** *Installing a C++ compiler on Windows:*

On *Windows* we're going to use the *MinGW* compiler to build our C++ programs into executable (.exe) files.

#+BEGIN_LATEX
\vspace{0.2cm}
#+END_LATEX

*1.* Go to https://sourceforge.net/projects/mingw/ and click *Download*.

#+ATTR_LATEX: :width \textwidth
[[file:reference/images/mingw_webpage.png]]

*2.* Save the install file to your computer and run it.

*3.* On the first page, click the "*Install*" button.

*4.* Choose a path to install the compiler. You can leave the other items as the defaults.

#+ATTR_LATEX: :width \textwidth
[[file:reference/images/mingw_install1.png]]

*5.* It will download some needed items. Click "*Continue*" once it's done.

#+ATTR_LATEX: :width \textwidth
[[file:reference/images/mingw_install2.png]]

*6.* On the main installer page, click the checkbox next to the "*mingw32-gcc-g++*" option.

#+ATTR_LATEX: :width \textwidth
[[file:reference/images/mingw_install3.png]]

#+ATTR_LATEX: :width 200px
[[file:reference/images/mingw_install4.png]]

*7.* Then, click on the "*Installation*" dropdown menu and select "*Apply Changes*".

[[file:reference/images/mingw_install5.png]]

*8.* It will ask "Okay to proceed?". Click "*Apply*" to continue. Once all the changes are done, click "*Close*".
You can then close the MinGW Installation Manager.

*9.* Next, go to the path where MinGW was installed (by default, it's =C:\MinGW=). Go into the "*bin*" folder,
and then select the address bar to turn it into text. *Copy* this path. This is the directory where the
MinGW compiler program resides.

#+ATTR_LATEX: :width 250px
[[file:reference/images/mingw_path.png]]

*10.* Next, click on the Windows start bar and type in "Environment" and look for the option "*Edit the system environment variables*".

#+ATTR_LATEX: :width 250px
[[file:reference/images/windows_envar1.png]]

*11.* In the System Properties screen, click on the "*Environment Variables...*" button.

[[file:reference/images/windows_envar2.png]]

*12.* Highlight the *Path* field and then click the "*Edit...*" button.

#+ATTR_LATEX: :width \textwidth
[[file:reference/images/windows_envar3.png]]

*13.* Paste your MinGW bin path into this editor then click "*OK*". You can then close out of the rest of the windows.

#+ATTR_LATEX: :width \textwidth
[[file:reference/images/windows_envar4.png]]


Next we will build and run an example program. Skip the next setups for Linux and Mac to find the
next section, "*Build and run a test program*".


#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX


** *Installing a C++ compiler on Linux:*

From your package manager install the *g++* package.

#+ATTR_LATEX: :width \textwidth
[[file:reference/images/installing_compiler_synaptic.png]]


Next we will build and run an example program. Skip the next setup for Mac to find the
next section, "*Build and run a test program*".

#+BEGIN_LATEX
\vspace{1cm}
#+END_LATEX

** *Installing a C++ compiler on Mac:*

TODO: DOCUMENT MAC COMPILER SETUP PROCESS
