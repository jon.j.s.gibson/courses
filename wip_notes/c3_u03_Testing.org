# -*- mode: org -*-

#+TITLE: Q&A Notes: Testing
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----

When writing test cases, we identify a set of inputs going into a function, and then we compare actual output to expected output to determine whether the test passed.

Given some example test cases for the following function:

1. =int Add( int a, int b );=

   This function takes in two numbers and returns the sum of them

   | Test # | Inputs | Expected output |
   |--------+--------+-----------------|
   |      1 |        |                 |
   |      2 |        |                 |

2. =int Max( int a, int b );=

   This function takes in two numbers and returns the larger of the two

   | Test # | Inputs | Expected output |
   |--------+--------+-----------------|
   |      1 |        |                 |
   |      2 |        |                 |

3. =int Sum( int arr[], int arrSize );=

   This function takes in an array of numbers and returns the sum

   | Test # | Inputs | Expected output |
   |--------+--------+-----------------|
   |      1 |        |                 |
   |      2 |        |                 |
 
