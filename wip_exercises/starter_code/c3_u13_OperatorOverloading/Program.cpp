#include "Program.h"
#include "tester/Fraction_Tester.hpp"
#include "tester/Helper.hpp"

#include <iostream>
#include <iomanip>
using namespace std;

Program::Program() : FRACTION_COUNT( 2 )
{
}

void Program::Run()
{
  Tester tester;
  bool done = false;
  while ( !done )
  {
    Helper::ClearScreen();
    Helper::Header( "MAIN MENU" );
    DisplayFractions();

    cout << "OPTIONS" << endl;
    cout << endl;
    int choice = Helper::ShowMenuGetInt( { "Quit program", "Run unit tests", "Initialize fraction", "Arithmetic", "Relational" }, true, false );

    switch( choice )
    {
      case 0: done = true; break;
      case 1: tester.Start(); break;
      case 2: Menu_Initialization(); break;
      case 3: Menu_Arithmetic(); break;
      case 4: Menu_Relational(); break;
    }

    Helper::Pause();
  }
}

void Program::Menu_Initialization()
{
  Helper::Header( "INITIALIZATION" );
  DisplayFractions();

  cout << "Update which fraction? #" << endl;
  int index = Helper::ShowMenuGetInt( { "Fraction 0", "Fraction 1" }, true, false );

  cout << "Enter new fraction with NUMERATOR DENOMINATOR" << endl;
  cout << "Make sure to separate the numerator/denominator with a SPACE" << endl;
  cout << ">> ";
  cin >> m_fractions[index];

  cout << endl << "Fraction is now: " << m_fractions[index] << endl;

  cin.ignore();
}

void Program::Menu_Arithmetic()
{
  Helper::Header( "ARITHMETIC" );
  DisplayFractions();

  cout << "Perform which operation?" << endl;
  int choice = Helper::ShowMenuGetInt( {
    "Go back",
    "Add        " + Helper::ToString( m_fractions[0] ) + " + " + Helper::ToString( m_fractions[1] ),
    "Subtract   " + Helper::ToString( m_fractions[0] ) + " - " + Helper::ToString( m_fractions[1] ),
    "Multiply   " + Helper::ToString( m_fractions[0] ) + " * " + Helper::ToString( m_fractions[1] ),
    "Divide     " + Helper::ToString( m_fractions[0] ) + " / " + Helper::ToString( m_fractions[1] )
  }, true, false );

  Fraction result;

  switch( choice )
  {
    case 1:
      result = m_fractions[0] + m_fractions[1];
      cout << m_fractions[0] << " + " << m_fractions[1] << " = " << result;
    break;

    case 2:
      result = m_fractions[0] - m_fractions[1];
      cout << m_fractions[0] << " - " << m_fractions[1] << " = " << result;
    break;

    case 3:
      result = m_fractions[0] * m_fractions[1];
      cout << m_fractions[0] << " * " << m_fractions[1] << " = " << result;
    break;

    case 4:
      result = m_fractions[0] / m_fractions[1];
      cout << m_fractions[0] << " / " << m_fractions[1] << " = " << result;
    break;
  }

  cout << endl;
  cin.ignore();
}

void Program::Menu_Relational()
{
  Helper::Header( "RELATIONAL" );
  DisplayFractions();

  cout << "Perform which operation?" << endl;
  int choice = Helper::ShowMenuGetInt( {
    "Go back",
    "Is " + Helper::ToString( m_fractions[0] ) + " == " + Helper::ToString( m_fractions[1] ) + "?",
    "Is " + Helper::ToString( m_fractions[0] ) + " != " + Helper::ToString( m_fractions[1] ) + "?",
    "Is " + Helper::ToString( m_fractions[0] ) + " <= " + Helper::ToString( m_fractions[1] ) + "?",
    "Is " + Helper::ToString( m_fractions[0] ) + " >= " + Helper::ToString( m_fractions[1] ) + "?",
    "Is " + Helper::ToString( m_fractions[0] ) + " <  " + Helper::ToString( m_fractions[1] ) + "?",
    "Is " + Helper::ToString( m_fractions[0] ) + " >  " + Helper::ToString( m_fractions[1] ) + "?",
  },
  true, false );

  bool result;
  string strResult;

  switch( choice )
  {
    case 1:
      result = ( m_fractions[0] == m_fractions[1] );
      strResult = ( result ) ? "TRUE" : "FALSE";
      cout << m_fractions[0] << " == " << m_fractions[1] << " is " << strResult << endl;
    break;

    case 2:
      result = ( m_fractions[0] != m_fractions[1] );
      strResult = ( result ) ? "TRUE" : "FALSE";
      cout << m_fractions[0] << " != " << m_fractions[1] << " is " << strResult << endl;
    break;

    case 3:
      result = ( m_fractions[0] <= m_fractions[1] );
      strResult = ( result ) ? "TRUE" : "FALSE";
      cout << m_fractions[0] << " <= " << m_fractions[1] << " is " << strResult << endl;
    break;

    case 4:
      result = ( m_fractions[0] >= m_fractions[1] );
      strResult = ( result ) ? "TRUE" : "FALSE";
      cout << m_fractions[0] << " >= " << m_fractions[1] << " is " << strResult << endl;
    break;

    case 5:
      result = ( m_fractions[0] < m_fractions[1] );
      strResult = ( result ) ? "TRUE" : "FALSE";
      cout << m_fractions[0] << " < " << m_fractions[1] << " is " << strResult << endl;
    break;

    case 6:
      result = ( m_fractions[0] == m_fractions[1] );
      strResult = ( result ) ? "TRUE" : "FALSE";
      cout << m_fractions[0] << " > " << m_fractions[1] << " is " << strResult << endl;
    break;
  }

  cin.ignore();
}

void Program::DisplayFractions()
{
  cout << fixed << setprecision( 3 );
  for ( int i = 0; i < FRACTION_COUNT; i++ )
  {
    cout << "Fraction " << i << ": " << m_fractions[i] << " (" << m_fractions[i].GetDecimal() << ")" << endl;
  }
  cout << endl;
}
