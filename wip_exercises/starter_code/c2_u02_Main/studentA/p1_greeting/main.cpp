/* STUDENT NAME: 
 * SEMESTER/YEAR: 
*/

#include <iostream>  // able to use cout to display text
using namespace std; // use the standard C++ library

int main()  // C++ programs begin here
{ // Code region open

  // -- PROGRAM CONTENTS GO HERE --------------------------------------------------

  cout << "Hi, I'm [name]. ";
  cout << "My favorite color is [color]. ";
  cout << "My favorite food is [food]. ";
  cout << "Some of my hobbies are [hobby1] and [hobby2]." << endl;
  
  return 0; // end program with no errors
} // Code region close