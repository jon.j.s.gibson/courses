/* STUDENT NAME: 
 * SEMESTER/YEAR: 
*/

#include <iostream>  // able to use cout to display text
using namespace std; // use the standard C++ library

int main( int argCount, char* args[] )  // C++ programs begin here
{ // Code region open

  // -- INITIALIZE PROGRAM --------------------------------------------------------
  // Do we have enough information?
  if ( argCount < 6 )
  {
    cout << "EXPECTED: " << args[0] << " NAME COLOR FOOD HOBBY1 HOBBY2" << endl;
    return 1; // Exit with error: Not enough arguments
  }

  // -- PROGRAM CONTENTS GO HERE --------------------------------------------------
  cout << "Hi, I'm " << args[1] << ". ";
  cout << "My favorite color is " << args[2] << ". ";
  cout << "My favorite food is " << args[3] << ". ";
  cout << "Some of my hobbies are " << args[4] << " and " << args[5] << "." << endl;
  
  return 0; // end program with no errors
} // Code region close