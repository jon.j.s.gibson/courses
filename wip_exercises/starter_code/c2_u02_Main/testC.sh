#! /bin/bash
#clear
# $? = Specifies the exit status of the last command or the most recent execution process.
RED='\033[0;31m'; GREEN='\033[0;32m'; NC='\033[0m' # No Color
ACOL='\033[0;34m'; BCOL='\033[0;35m'; CCOL='\033[0;36m'
STUDENTCOL=CCOL
student='studentC'

programs=("${student}/p1_greeting/" "${student}/p2_arguments/")
executables=()

declare -A TESTS
TOTALTESTS=3
# TEST 0
TESTS[0,0]=0  # programs index
TESTS[0,1]="" # arguments
TESTS[0,2]="Hi, I'm [NAME]. My favorite color is [COLOR]. My favorite food is [FOOD]. Some of my hobbies are [HOBBY1] and [HOBBY2]." # expected output
# TEST 1
TESTS[1,0]=1  # programs index
TESTS[1,1]="Rachel purple biryani coding games" # arguments
TESTS[1,2]="Hi, I'm Rachel. My favorite color is purple. My favorite food is biryani. Some of my hobbies are coding and games." # expected output
# TEST 2
TESTS[2,0]=1  # programs index
TESTS[2,1]="Rai green momos CSGO hookah" # arguments
TESTS[2,2]="Hi, I'm Rai. My favorite color is green. My favorite food is momos. Some of my hobbies are CSGO and hookah." # expected output

# ---------------------------------------------------------------------- WHICH STUDENT?
echo -e "\n TESTS FOR ${student}"; head -1 "${programs[0]}main.cpp"
# ---------------------------------------------------------------------- BUILD PROGRAMS
echo -e "\n === BUILD PROGRAMS ==="
for i in ${!programs[@]}; do
  niceIndex=$((i+1))
  outname="${programs[i]}${student}_program$niceIndex.out"
  g++ ${programs[i]}*.cpp -o ${outname}
  if [ "$?" -eq 0 ]; then
    echo -e "${GREEN}Successfully built [${outname}] ${NC}"
    executables+=(${outname})
  else
    echo -e "${RED}FAILED to build [${outname}]${NC}"
  fi  
done
# ---------------------------------------------------------------------- TEST PROGRAMS
echo -e "\n === TEST PROGRAMS ==="
for ((i=0; i<${TOTALTESTS}; i++))
do  
  PROGRAMI=${TESTS[$i,0]}
  PROGARGS=${TESTS[$i,1]}
  EXPECTED=${TESTS[$i,2]}
  EXECUTBL=${executables[${PROGRAMI}]}

  RUNNER="./$EXECUTBL"
  if [ "$PROGARGS" != "" ]; then
    RUNNER="./$EXECUTBL $PROGARGS"
  fi
    
  ACTUAL=$(eval "$RUNNER")  
  if [ "${ACTUAL}" == "${EXPECTED}" ]; then
    echo -e "* ${GREEN}TEST ${i} SUCCESS! PROGRAM:[$RUNNER] OUTPUT: [${ACTUAL}]${NC}"
  else
    echo -e "* ${RED}TEST #${i} FAIL! PROGRAM:[$RUNNER] \n\t* EXPECTED OUTPUT: [${EXPECTED}] \n\t* ACTUAL OUTPUT:   [$ACTUAL]${NC}"
  fi
done
