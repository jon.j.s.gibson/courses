#ifndef _CRITTER_MANAGER
#define _CRITTER_MANAGER

#include <vector>
#include <string>
using namespace std;

#include "Critter.hpp"

class CritterManager
{
    public:
    CritterManager();

    void Run();
    void Update();

    private:
    void Setup();
    void Draw();
    void Delay();

    int GetRandomX();
    int GetRandomY();

    int GetClosestWormIndex();
    float GetDistance( float x1, float y1, float x2, float y2 );

    int m_mapWidth;
    int m_mapHeight;
    int m_timeStamp;
    const int REFRESH_RATE;
    vector<Critter> m_critters;
};

#endif
