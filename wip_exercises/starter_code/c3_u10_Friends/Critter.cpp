#include "Critter.hpp"

#include <iostream>
using namespace std;

Critter::Critter()
{
    m_alive = true;
    m_goalX = -1;
    m_goalY = -1;
    m_eaten = 0;
}

void Critter::Draw()
{
    if ( m_alive )
    {
        cout << m_symbol;
    }
}

void Critter::Update()
{
    if ( !m_alive )
    {
        // Doesn't do anything
        m_log = "[" + m_name + " is dead.]";
        return;
    }

    if ( m_goalX != -1 && m_goalX < m_x )
    {
        m_x--;
        m_log = "[" + m_name + " moved left!]";
    }
    else if ( m_goalX != -1 && m_goalX > m_x )
    {
        m_x++;
        m_log = "[" + m_name + " moved right!]";
    }

    else if ( m_goalY != -1 && m_goalY < m_y )
    {
        m_y--;
        m_log = "[" + m_name + " moved up!]";
    }
    else if ( m_goalY != -1 && m_goalY > m_y )
    {
        m_y++;
        m_log = "[" + m_name + " moved down!]";
    }
    else
    {
        m_log = "[" + m_name + " rested.]";
    }

    if ( m_x == m_goalX && m_y == m_goalY )
    {
        // Reset goal
        m_goalX = m_goalY = -1;
    }
}

void Critter::SetGoal( int x, int y )
{
    m_goalX = x;
    m_goalY = y;
}

bool Critter::HasGoal()
{
    return ( m_goalX != -1 && m_goalY != -1 );
}

void Critter::CaughtWorm()
{
    m_eaten++;
    m_log = "[" + m_name + " caught a worm!]";
    m_goalX = -1;
    m_goalY = -1;
}

void Critter::OutputLog()
{
    cout << m_log << endl;
}
