# Each student enter their inventory...

studentName1 = "YOUR NAME"
studentOx1 = 0
studentFish1 = 0
studentBread1 = 0
studentGrain1 = 0

studentName2 = "YOUR NAME"
studentOx2 = 0
studentFish2 = 0
studentBread2 = 0
studentGrain2 = 0

studentName3 = "YOUR NAME"
studentOx3 = 0
studentFish3 = 0
studentBread3 = 0
studentGrain3 = 0

print( "" )
print( "INVENTORY PROGRAM" )
print( "" )

print( "STUDENT 1:", studentName1 )
print( "* OX:     ", studentOx1 )
print( "* FISH:   ", studentFish1 )
print( "* BREAD:  ", studentBread1 )
print( "* GRAIN:  ", studentGrain1 )
print( "" )
print( "STUDENT 2:", studentName2 )
print( "* OX:     ", studentOx2 )
print( "* FISH:   ", studentFish2 )
print( "* BREAD:  ", studentBread2 )
print( "* GRAIN:  ", studentGrain2 )
print( "" )
print( "STUDENT 3:", studentName3 )
print( "* OX:     ", studentOx3 )
print( "* FISH:   ", studentFish3 )
print( "* BREAD:  ", studentBread3 )
print( "* GRAIN:  ", studentGrain3 )
print( "" )