--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

For this assignment we will be exploring already existing software
and comparing how the users interface with them.

As you work through this assignment, make sure to fill out the
questions.txt file in Replit.


--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

PROGRAM 1: CONTINUOUSLY RUNNING - ZORK

Run the game at this link:
https://classicreload.com/zork-i.html

Zork is a game that came out in the 1980s. It runs in the console (the black window with white text) and is text-only. Spend a few minutes playing the game and exploring. 

You can type in commands like: NORTH, SOUTH, EAST, WEST to move around,
OPEN (object), TAKE (object), READ (object) and more to interact with the environment.
(You can see a list of commands here: https://zork.fandom.com/wiki/Command_List)

Answer the questions in questions.txt before continuing.

--------------------------------------------------------

PROGRAM 2: CONTINUOUSLY RUNNING - SALE

Run the sale program by typing the following in the SHELL or CONSOLE:

./sale

This will begin the program, which looks like this:

  TOTAL PRICE:     $0.00
  TAX:             $0.00
  PRICE AFTER TAX: $0.00

  1. Clear shopping cart
  2. Add another item
  3. Exit

  >> 


Answer the questions in questions.txt before continuing.

If you would like, you can view the source code for this program. It is the "sale.cpp" file.

--------------------------------------------------------

PROGRAM 3: COMMAND LINE WITH ARGUMENTS - TEMPERATURE CONVERSION (C++)

Zork and the Sale programs run until the user decides to quit. With these console utility programs they usually take in some input, run an operation, and immediately quit.

For these programs, open up the Replit assignment and click on the SHELL or CONSOLE view (they are both the same).

This program can be run with the same `./` command, but it also requires two input arguments:

  ./temp TEMPERATURE C/F
  
If you don't give any information besides the program name, it will complain:

  $ ./temp
  NOT ENOUGH ARGUMENTS! Expected form:
  ./temp temp C/F


Try running the program to convert 0 C to Farenheit like this:

  ./temp 0 C
  0 C = 32 F

Or try to convert 100 F to Celsius like this:

  ./temp 100 F
  100 F = 37.7778 C


Answer the questions in questions.txt before continuing.

If you would like, you can view the source code for this program. It is the "temp.cpp" file.




