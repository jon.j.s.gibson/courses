name = input( "What is your name? " )
print( "Hello,", name )
print()

x = float( input( "Enter a first number: " ) )
y = float( input( "Enter a second number: " ) )

sum = x + y
difference = x - y
product = x * y
quotient = x / y

print( x, "+", y, "=", sum )
print( x, "-", y, "=", difference )
print( x, "*", y, "=", product )
print( x, "/", y, "=", quotient )