--------------------------------------------------------

INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- STRUCTS --
Structs are a way we can group several variables together under one name (the struct's name). This essentially also means we can create our OWN data types, because after a struct is declared, we can declare variables with that struct as the variable's type.

Declaring a struct:

```
struct Day
{
  int num;
  string month;
}; // << IMPORTANT! Put ; at the end of all STRUCT declarations!
```

The variables declared INSIDE the struct are known as its MEMBER VARIABLES.

Creating a variable object and assigning its internal variables:

```
Day today;
today.num = 2;
today.month = "August";
cout << "Today is " << today.month << " " << today.num << endl;
```

-- UML DIAGRAMS --
UML diagrams help us visualize a struct (or class) and its contents. You will be seeing these as ways to express how you should be declaring your structs (and classes) going forward.

-----------------------       This is a UML diagram.

- Day                 -       << This line gives you the NAME of the struct

-----------------------

- + num: int          -       << These are variables. Ignore the "+" for now, but it has a meaning.
- + month: string     -       << (We'll learn more about UML diagrams in the CLASS section.)
- + dayOfWeek: string -       << Variables in UML are given as "NAME:TYPE".
- + year: int         -       << So this would be declared as `int year;`.

-----------------------

-- PASS-BY-REFERENCE --
By default when we create a FUNCTION any parameter variables are "pass-by-value". This means a copy of the data is made and passed to the function, so any changes to that data WITHIN the function are NOT reflected back out in the original variable (i.e., back in main()).
Sometimes, however, we want to make changes to some data from within a function. In this case, we can put `&` as a suffix on the data type of the variable to mark it as "pass-by-reference", which allows us to make changes to that variable from within the function.
Additionally, sometimes we will pass objects as a "const pass-by-reference" by using `const TYPE& VAR`. This is because objects (variables we create from structs and classes) tend to be much bigger in memory than a simple integer, float, etc. - because they're bigger, the copy time is just wasted time when we could just give direct access to the object itself. However, we don't always want to be able to overwrite our object's data, so if we pass it as `const`, we give access to READ from it, but not CHANGE anything.

-- BUILDING YOUR PROGRAM --
use `cd` to navigate to the folder
use `g++` to build the code:

g++ *.cpp *.h -o PROGRAMNAME.out

-- EXAMPLE PROGRAM OUTPUT --
./program.out

EMPLOYEES
ID   LOCATION  WAGE      NAME                                              
--------------------------------------------------------------------------------
1    2         15.57     Rhonda W.                                         
2    1         16.97     Fady A.                                           
3    1         15.09     Neha G.     

--------------------------------------------------------

PROGRAM INSTRUCTIONS
--------------------------------------------------------

-- STRUCT DECLARATION --
Within Employee.h, declare an `Employee` struct. It should have the following:

-----------------------

- Employee            -

-----------------------

- + employeeId: int   -
- + locationId: int   -
- + name: string      -
- + hourlyWage: float -

-----------------------

TESTING THE STRUCT: Before continuing on with the program, let's take a moment to test out the struct within main().

  Declare a variable using this form:
  `STRUCTNAME VARNAME;`
  This is exactly the same as a normal variable declaration - Our Struct is the DATA TYPE, and we give the variable a name.
  With the "Day" struct from above, it would look like:
  `Day today;`

  Next, assign values to its member variables:
  `VARNAME.MEMBERVAR = VALUE;`
  So with the "Day" struct from above, it would look like:

```
today.num = 2;
today.month = "August";
// etc.
```

  Then, use `cout` statements to display all the information. For example:

```
cout << "today.num is " << today.num << endl;
cout << "today.month is " << today.month << endl;
// etc.
```

  Do this except with your own struct and its member variables to make sure everything BUILDS AND RUNS before continuing. If there are any build errors, fix them now before working on the next thing.


-- FUNCTIONS --
Write the FUNCTION DECLARATIONS within the Employee.h file.
Write the FUNCTION DEFINITIONS within the Employee.cpp file.

1. SetupEmployees function
    Input parameters: e1 - an Employee&, e2 - an Employee&, e3 - an Employee&
    Output return: void
    Functionality: Set up values for each of the Employee variables, `e1`, `e2`, and `e3`. For example:

| Variable | employeeId | locationId | name      | hourlyWage |
| -------- | ---------- | ---------- | --------- | ---------- |
| e1       | 1          | 2          | Rhonda W. | 15.57      |
| e2       | 2          | 1          | Fady A.   | 16.97      |
| e3       | 3          | 1          | Neha G.   | 15.09      |



2. PrintEmployeeTable function
    Input parameters: e1 - a const Employee&, e2 - a const Employee&, e3 - a const Employee&
    Output return: void
    Functionality: Use `cout` statements as well as `setw` to display each employee's data in a nice table.
    The header, for example, could be written like this:
    
    ```
    cout << left;
    cout << endl << "EMPLOYEES" << endl;
    cout
      << setw( 5 ) << "ID"
      << setw( 10 ) << "LOCATION"
      << setw( 10 ) << "WAGE"
      << setw( 50 ) << "NAME"
      << endl << string( 80, '-' ) << endl;
    ```
    
    then you can use the same column widths for each field (ID = employeeId, LOCATION = locationId, etc.)


-- MAIN PROGRAM --

Within main(), create three Employee variables: `employee1`, `employee2`, `employee3`.

Then, call the `SetupEmployees` function, passing in the variables.

Finally, call the `PrintEmployeeTable` function, again passing in the variables.
