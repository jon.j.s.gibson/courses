#ifndef _EXCEPTION
#define _EXCEPTION

#include <stdexcept>
#include <iostream>

//! EXCEPTION for "not enough friends"
class NotEnoughFriendsException : public std::runtime_error
{
public:
    NotEnoughFriendsException(std::string message)
        : std::runtime_error(message) { 
        std::cout << "You should really make more friends." << std::endl;
    }
};

//! EXCEPTION for "not enough pizza"
class NotEnoughPizzaException : public std::runtime_error
{
public:
    NotEnoughPizzaException(std::string message)
        : std::runtime_error(message) { 
    std::cout << "You can't have a pizza party without pizza!" << std::endl;
    }
};


#endif

