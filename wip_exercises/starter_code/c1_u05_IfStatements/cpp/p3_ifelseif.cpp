#include <iostream>
using namespace std;

int main()
{
  // 1. Ask the user to "Enter their battery charge %". Get their input as an int an store it in a variable `charge`.

  
  // 2. If the `charge` is 90 or above, display: 
  //         [####}
  
  
  // 3. Else, if `charge` is 75 or above, display: 
  //         [###_}
  
  
  // 4. Else, if `charge` is 50 or above, display: 
  //         [##__}
  
  
  // 5. Else, if `charge` is 25 or above, display: 
  //         [#___}
  
  
  // 6. Else, display [____}

  // 7. Display "Goodbye" at the end of the program.
  cout << endl << "Goodbye." << endl;
  
  return 0; // program end
}