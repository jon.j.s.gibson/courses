#include <iostream>
using namespace std;

// FUNCTION DEFINITIONS:
float GetTaxRate()
{
  return 0.091;
}

float GetPricePlusTax( float price, float tax )
{
  return price + (price*tax);
}

// MAIN PROGRAM
int main()
{
  cout << "What is the item's price? $";
  float my_price;
  cin >> my_price;

  float my_tax = GetTaxRate();

  float total_price = GetPricePlusTax( my_price, my_tax );

  cout << "Total price is: $" << total_price << endl;
}