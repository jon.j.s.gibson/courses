def GetTaxRate():
  return 0.091

def GetPricePlusTax( price, tax ):
  return price + (price * tax)


my_price = float( input ("What is the item's price? $" ) )
my_tax = GetTaxRate()

total_price = GetPricePlusTax( my_price, my_tax )

print( "Total price is: $", total_price )