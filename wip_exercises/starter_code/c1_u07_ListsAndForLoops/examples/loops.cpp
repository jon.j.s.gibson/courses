#include <iostream>
using namespace std;

int main()
{
  int start, end;
  cout << "Enter start #: ";
  cin >> start;

  cout << "Enter end #: ";
  cin >> end;
  
  for ( int i = start; i < end; i++ )
  {
    cout << i << endl;
  }
  
  return 0;
}