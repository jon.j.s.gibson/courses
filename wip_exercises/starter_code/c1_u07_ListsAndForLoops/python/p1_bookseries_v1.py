# 1. Create a LIST named "books_in_series". Within the list, store several STRINGS: The title of several books in a series (e.g., "Dawn", "Adulthood Rites", "Imago")


# 2. DISPLAY to the screen the name of the book series (e.g., "Lilith's Brood")


# 3. DISPLAY to the screen "There are ____ books in this series". Use len( books_in_series ) to get the # of books in the list.


# 4. DISPLAY each # and each book title (e.g., "0 is Dawn") WITHOUT A LOOP. You can print out the number "0" and then to get the specific book, use books_in_series[0] (or 1, or 2, etc.)