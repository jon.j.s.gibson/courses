# 1. Create a LIST named "books_in_series". Within the list, store several STRINGS: The title of several books in a series (e.g., "Dawn", "Adulthood Rites", "Imago")


# 2. DISPLAY to the screen the name of the book series (e.g., "Lilith's Brood")


# 3. DISPLAY to the screen "There are ____ books in this series". Use len( books_in_series ) to get the # of books in the list.


# 4. Use the FOR LOOP WITH INDEX (i) to display each book's # (i) and name (books_in_series[i])