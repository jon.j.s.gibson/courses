#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
  // 1. Create a VECTOR of STRINGS named "books_in_series". Within the list, store several STRINGS: The title of several books in a series (e.g., "Dawn", "Adulthood Rites", "Imago")
  
  
  // 2. DISPLAY to the screen the name of the book series (e.g., "Lilith's Brood")
  
  
  // 3. DISPLAY to the screen "There are ____ books in this series". Use books_in_series.size() to get the # of books in the vector.


  // 4. DISPLAY each # and each book title (e.g., "0 is Dawn") WITHOUT A LOOP. You can print out the number "0" and then to get the specific book, use books_in_series[0] (or 1, or 2, etc.)
  
  
  
  
  // Say Goodbye at the end of the program!
  cout << "\n GOODBYE!" << endl;
  
  return 0;
}