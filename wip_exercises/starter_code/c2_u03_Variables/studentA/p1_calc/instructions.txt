--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------
Variables are like a "box" where we can store stuff in for later. We can create a variable, give it a name to describe what it holds, and put different items in it at different times.

Let's say we were checking out items as a cashier. The first item is $2.39, so we put `2.39` in a `price` variable and perhaps do some computations. The next item is $3.45, so we put `3.45` in `price` and do the next computation. We can keep reusing the `price` variable, putting different values in it throughout the program's lifetime.


-- BUILDING YOUR PROGRAM --
cd studentA/p1_calc
g++ sub.cpp -o sub.out

The default program name is "a.out", but we can change the program's name by putting the "-o" flag, followed by our custom program name.


-- EXAMPLE PROGRAM OUTPUT --
When we run this program, we will provide two arguments, two different numbers. The program will then do a mathematical operation and display the result.

$ ./sub.out 3 2
3-2=1

$ ./sub.out 6 4
6-4=2

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------
We can use the `stof` function to convert one of the `args[]` from a string to a float variable:

float varname = stof( args[1] );

Create two float variables, such as `num1` and `num2`, and then load the following arguments into each one:

args[1]     args[2]   
num1        num2     

Create a third float variable to store the `result` of the math operation. Calculate the DIFFERENCE of the two numbers with this:

num1 - num2

And store the result in your `result` variable.

Finally, display the result to the screen, like this:

3-2=1






