#! /bin/bash
#clear
# $? = Specifies the exit status of the last command or the most recent execution process.
RED='\033[0;31m'; GREEN='\033[0;32m'; NC='\033[0m' # No Color
ACOL='\033[0;34m'; BCOL='\033[0;35m'; CCOL='\033[0;36m'
STUDENTCOL=ACOL
student='studentA'

programs=("${student}/p1_calc/" "${student}/p2_formula/")
executables=()

declare -A TESTS
TOTALTESTS=4
# TEST 0
TESTS[0,0]=0  # programs index
TESTS[0,1]="3 2" # arguments
TESTS[0,2]="3-2=1" # expected output
# TEST 1
TESTS[1,0]=0  # programs index
TESTS[1,1]="6 4" # arguments
TESTS[1,2]="6-4=2" # expected output
# TEST 2
TESTS[2,0]=1  # programs index
TESTS[2,1]="3 4" # arguments
TESTS[2,2]="perimeter=14" # expected output
# TEST 3
TESTS[3,0]=1  # programs index
TESTS[3,1]="3 5" # arguments
TESTS[3,2]="perimeter=16" # expected output

# ---------------------------------------------------------------------- WHICH STUDENT?
echo -e "\n TESTS FOR ${student}"; head -1 "${programs[0]}sub.cpp"
# ---------------------------------------------------------------------- BUILD PROGRAMS
echo -e "\n === BUILD PROGRAMS ==="
for i in ${!programs[@]}; do
  niceIndex=$((i+1))
  outname="${programs[i]}${student}_program$niceIndex.out"
  g++ ${programs[i]}*.cpp -o ${outname}
  if [ "$?" -eq 0 ]; then
    echo -e "${GREEN}Successfully built [${outname}] ${NC}"
    executables+=(${outname})
  else
    echo -e "${RED}FAILED to build [${outname}]${NC}"
  fi  
done
# ---------------------------------------------------------------------- TEST PROGRAMS
echo -e "\n === TEST PROGRAMS ==="
for ((i=0; i<${TOTALTESTS}; i++))
do  
  PROGRAMI=${TESTS[$i,0]}
  PROGARGS=${TESTS[$i,1]}
  EXPECTED=${TESTS[$i,2]}
  EXECUTBL=${executables[${PROGRAMI}]}

  RUNNER="./$EXECUTBL"
  if [ "$PROGARGS" != "" ]; then
    RUNNER="./$EXECUTBL $PROGARGS"
  fi
    
  ACTUAL=$(eval "$RUNNER")  
  if [ "${ACTUAL}" == "${EXPECTED}" ]; then
    echo -e "* ${GREEN}TEST ${i} SUCCESS! PROGRAM:[$RUNNER] OUTPUT: [${ACTUAL}]${NC}"
  else
    echo -e "* ${RED}TEST #${i} FAIL! PROGRAM:[$RUNNER] \n\t* EXPECTED OUTPUT: [${EXPECTED}] \n\t* ACTUAL OUTPUT:   [$ACTUAL]${NC}"
  fi
done
