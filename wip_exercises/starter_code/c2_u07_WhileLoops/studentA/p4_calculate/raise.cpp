/* STUDENT NAME: 
 * SEMESTER/YEAR: 
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main( int argCount, char* args[] ) 
{
  if ( argCount < 2 ) { cout << "Not enough arguments! Give: basepay raisepercent yearcount" << endl; return 1; }
  
  cout << fixed << setprecision( 2 );
  
  float basepay = stof( args[1] );
  float raise = stof( args[2] );
  int years = stoi( args[3] );
  
  // -- PROGRAM CONTENTS GO HERE --------------------------------------------------


  return 0;
}
