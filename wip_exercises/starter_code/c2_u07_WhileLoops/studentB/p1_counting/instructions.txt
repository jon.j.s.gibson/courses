--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- WHILE LOOPS --
While loops are similar to if statements in that you specify a CONDITION. With an if statement, if that CONDITION evaluates to true then the code within the if statement's codeblock is executed - once.
If the CONDITION for a while loop evaluates to true, it will run the while loop's inner codeblock REPEATEDLY, until the CONDITION evaluates to false. Because of this, it is easy to write an INFINITE LOOP with while loops.
While loops take this form:

while ( CONDITION )
{
  // This code is ran while the CONDITION is true.
}


-- FORCING THE PROGRAM TO STOP --
While a program is running in the console, you can use "CTRL+C" to force the program to stop execution. This might be useful if you accidentally make a infinite loop.


-- BUILDING YOUR PROGRAM --
g++ countdown.cpp -o countdown.out


-- EXAMPLE PROGRAM OUTPUT --
./countup.out 20 14
20 18 16 14

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

ARGUMENTS: This program expects additional arguments:
1. low                The lower integer for the number sequence
2. high               The higher integer for the number sequence

Use a while loop to count from (high) to (low), going down by 2 each time.
