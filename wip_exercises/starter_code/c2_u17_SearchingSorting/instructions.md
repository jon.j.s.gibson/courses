# About this program

This program loads in a list of courses. A `Course` class contains the following important functions:
* GetDepartment()
* GetTitle()
* GetHours()
* GetDescription()

When you run the program, there will be a main menu. 
* Option 1, Run unit tests - Allows you to run the automated tests for all students.
* Option 2, View full course list - Allows you to view all the data (optional).
* Option 3, View course by index - Allows you to view one course by its index ID (optional).
* Options 4-6, Search functionality - You'll be implementing one of these.
* Options 7-9, Sort functionality - You'll be implementing one of these. 

----------------------------------------------

# Student work

## Algorithms:
Make sure to reference the reading material (https://moosadee.gitlab.io/courses/wip_reading/c2_u17_SearchingSorting.html) for the specific algorithms.

## Running the program:
Use the "Run" button in Replit to BUILD the program. Then, use 
./program.out
to run the program.

## String contains:
The Helper.h file has a function you can use for the search:

`bool StringContains( std::string haystack, std::string needle, bool caseSensitive = true );`

To call it, you can use

```
if ( StringContains( FULLWORD, SEARCHTERM ) )
```

## Student A

You'll be working in the `studentA_Functions.cpp` file.

### SearchCoursesByDepartment
Your Search function will search the original list of Courses (`original`), look for the `searchTerm`, which contains all or part of a department name. 
You will use the GetDepartment() function to compare an item of the list to the `searchTerm`. If there's a match, then you add the item to the `matches` vector.
At the end of this function, you return the `matches` - vector of matching items.

### SelectionSortCoursesByDepartment
Utilize the Selection Sort algorithm to sort all the courses based on the **DepartmentName**.

**Remember to do your comparisons on sortCourses[j].GetDepartment() and sortCourses[minIndex].GetDepartment()**!

----------------------------------------------

## Student B

### SearchCoursesByHours
Your Search function will search the original list of Courses (`original`), look for the `searchTerm`, and compare to the course's hours. 
You will use the GetHours() function to compare an item of the list to the `searchTerm`. If there's a match, then you add the item to the `matches` vector. (Just use == here, not the StringContains function.)
At the end of this function, you return the `matches` - vector of matching items.

### InsertionSortCoursesByHours
Utilize the Selection Sort algorithm to sort all the courses based on the **Hours**.

**Remember to do your comparisons on sortCourses[j-1].GetHours() and sortCourses[j].GetHours()**!

----------------------------------------------

## Student C

### SearchCoursesByTitle
Your Search function will search the original list of Courses (`original`), look for the `searchTerm`, which contains all or part of a course title. 
You will use the GetTitle() function to compare an item of the list to the `searchTerm`. If there's a match, then you add the item to the `matches` vector.
At the end of this function, you return the `matches` - vector of matching items.

### BubbleSortCoursesByTitle
Utilize the Selection Sort algorithm to sort all the courses based on the **Title**.

**Remember to do your comparisons on sortCourses[j].GetTitle() and sortCourses[j+1].GetTitle()**!
