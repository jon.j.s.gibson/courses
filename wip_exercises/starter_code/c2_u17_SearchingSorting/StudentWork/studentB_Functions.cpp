/* STUDENT NAME: 
 * SEMESTER/YEAR: 
*/

#include "../Courses/CourseManager.h"
#include "../Utilities/Helper.h"
#include <utility>
using namespace std;

vector<Course> CourseManager::SearchCoursesByHours(const vector<Course> &original, string searchTerm)
{
  vector<Course> matches;

  // TODO: IMPLEMENT LINEAR SEARCH, SEARCH BY HOURS
  // 1. Use a for loop to iterate over all the items in the "original" vector
  // 2. Inside the for loop, use an if statement to check if the hours of the current item is equal to the search term (use the GetHours() function on the course).
  // 2a. Within the if statement, if the text is found, then push the current element onto the "matches" vector.
  // 3. Before the function ends, return the "matches" vector.

  return matches;
}

vector<Course> CourseManager::InsertionSortCoursesByHours(const vector<Course> &original)
{
  vector<Course> sortCourses = original;

  // TODO: IMPLEMENT INSERTION SORT HERE, SORT BY COURSE HOURS
  // See reading material for the algorithm!

  return sortCourses;
}
