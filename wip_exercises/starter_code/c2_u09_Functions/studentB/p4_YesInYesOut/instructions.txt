--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- FUNCTIONS: YES INPUTS, YES OUTPUTS -- 
Functions like these often take in some data and use it in a formula or algorithm and return some sort of result.

These are most similar to the functions you learned about in algebra, like f(x)=x^2.

-- BUILDING YOUR PROGRAM --
You will need to make sure to build ALL header and source files now.

cd studentB/p4_YesInYesOut
g++ *.h *.cpp -o program.out


-- EXAMPLE PROGRAM OUTPUT --
./program.out 
Enter a number: 4
Square is: 16

./program.out
Enter a number: 5
Square is: 25

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

Within the Functions.h file, create a function DECLARATION that meets the following criteria:
* Name: GetSquare
* Input Parameters: num - float
* Output Return: float

Within the Functions.cpp file, you will add your function DEFINITION. The function header needs to match the header from the .h file exactly, except with no `;` at the end. Instead, you'll have a code block { } and put program logic within.
Within the function, calculate the SQUARE of the `num` given. You can do this with `num*num`. Make sure to return the result.


Within main() do the following:
1. Create a float variable `input`.
2. Ask the user to enter a number and store it in the `input` variable.
3. Create an float variable named `result`. Call the `GetSquare` function, passing in your input. Store the result in the `result` variable.
4. Display "Square is: " and then the value of the `result` variable.






