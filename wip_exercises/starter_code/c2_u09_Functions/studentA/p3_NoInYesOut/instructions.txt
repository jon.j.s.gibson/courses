--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- FUNCTIONS: NO INPUTS, YES OUTPUTS -- 
Functions like these are used to retrieve data. Sometimes it will be from other places in the program, or from a data file or database. In this case we're just returning some hard-coded data.

These functions take in no inputs so what they output is not affected by any input; their output is the same every time.


-- BUILDING YOUR PROGRAM --
You will need to make sure to build ALL header and source files now.

cd studentA/p3_NoInYesOut
g++ *.h *.cpp -o program.out


-- EXAMPLE PROGRAM OUTPUT --
./program.out 
Tax rate is: 9.61

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

Within the Functions.h file, create a function DECLARATION that meets the following criteria:
* Name: GetTaxPercent
* Input Parameters: NONE
* Output Return: floats

Within the Functions.cpp file, you will add your function DEFINITION. The function header needs to match the header from the .h file exactly, except with no `;` at the end. Instead, you'll have a code block { } and put program logic within.
Within the function, just return the tax rate value: 9.61.


Within main() do the following:
1. Create a float variable to store the `taxRate`.
2. Call the `GetTaxPercent` function. It will have no input arguments in () but it returns a float, so assign its return value to your `taxRate` variable.
3. Display the `taxRate` to the screen.






