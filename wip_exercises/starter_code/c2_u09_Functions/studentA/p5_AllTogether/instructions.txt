--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- BUILDING YOUR PROGRAM --
You will need to make sure to build ALL header and source files now.

cd studentA/p5_AllTogether/
g++ *.h *.cpp -o program.out


-- EXAMPLE PROGRAM OUTPUT --
./program.out 
What is the price of the item? $23.50
What is the sale percent? %15
Price after sale: $19.98

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

FUNCTIONS - Declare in Functions.h, define in Functions.cpp

1. GetInput function
    Input parameters: message - string
    Output return: float
    Functionality: Use `cout` to display the `message`, then use a `cin` statement and have the user enter a float value. `return` that float.

2. IsValid function
    Input parameters: value - float
    Output return: bool
    Functionality: If the `value` passed in is less than or equal to 0, then return `false`. Otherwise, return `true`.

3. GetPriceOnSale function
    Input parameters: price - float, salePercent - float
    Output return: float
    Functionality: Calculate the price of the item after the sale amount is taken off. Return the calculated result.
    DOLLARS OFF: price * salePercent/100
    PRICE AFTER SALE: price - DOLLARS OFF
  

MAIN PROGRAM - Do the following:
1. Create a float `price` variable and call the `GetInput` function to initialize its value - ask the user "What is the price of the item? $"
2. Create a float `sale` variable and call the `GetInput` function to initialize its value - ask the user "What is the sale percent? %"
3. Do an error check - if `price` is INVALID, or if `sale` is INVALID, then display "INVALID VALUE!" and `return 1;` to quit the program, symbolizing an error has occurred.
4. Create a float `priceOnSale` variable. Call the `GetPriceOnSale` function, passing in the `price` and `sale`. Store its result in your `priceOnSale` variable.
5. Use a `cout` statement to display the resulting price after the sale.
