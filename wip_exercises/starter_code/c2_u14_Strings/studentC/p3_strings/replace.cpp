/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ replace.cpp -o replace.exe
 * RUN PROGRAM: ./replace.exe string1 string2 index length
 *
 * 
 * */
 
 #include <iostream>
 #include <string>
 using namespace std;
 
 int main( int argCount, char* args[] )
 {
   if ( argCount < 5 )
   {
     cout << "EXPECTED FORM: " << args[0] << " string1 string2 index length" << endl;
     return 1;
   }
   
   string string1 = string( args[1] );
   string string2 = string( args[2] );
   int index = stoi( args[3] );
   int length = stoi( args[4] );
   
   // TODO: Use string replace

   cout << "string1 is now " << string1 << endl;
   
   return 0;
 }
