#include <map>
#include <iostream>
#include <string>
using namespace std;

void MapProgram()
{
	cout << endl << string(60, '-') << endl << "MAP PROGRAM" << endl << endl;

	// 1. DECLARE A MAP with int keys and string values.


	// INITIALIZE THE MAP WITH THE DATA FROM THE DOCUMENTATION.


	// 2. DISPLAY ALL KEY-VALUE PAIRS IN THE MAP


	// 3. ASK THE USER TO ENTER AN ID


	// 4. TRY TO ACCESS THE STUDENT AT THAT KEY


	// 5. CATCH ANY EXCEPTIONS AND DISPLAY AN ERROR MESSAGE

}
