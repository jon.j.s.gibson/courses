#include <iostream>
#include <vector>
using namespace std;
#include "Ingredient.h"
#include "Functions.h"

int main()
{
  vector<Ingredient> ingredients;

  bool done = false;
  while ( !done )
    {
      cout << endl << string( 80, '-' ) << endl;
      DisplayAllIngredients( ingredients );
      DisplayMainMenu();
      int choice = GetChoice( 1, 3 );

      switch( choice )
        {
        case 1: // Add ingredient
          AddIngredient( ingredients );
          break;

        case 2: // Edit ingredient
          EditIngredient( ingredients );
          break;

        case 3: // Exit
          done = true;
          break;
        }
    }


  return 0;
}
