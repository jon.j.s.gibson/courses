#ifndef _FUNCTIONS
#define _FUNCTIONS

#include <vector>
using namespace std;

#include "Ingredient.h"

void DisplayAllIngredients( const vector<Ingredient>& ingredients );

void DisplayMainMenu();

int GetChoice( int min, int max );

void AddIngredient( vector<Ingredient>& ingredients );

void EditIngredient( vector<Ingredient>& ingredients );

void PressEnterToContinue();

#endif
