/* STUDENT NAME:
 * SEMESTER/YEAR:
 * */
#include "Product.h"
#include <iostream>
#include <iomanip>
using namespace std;

// -- CLASS FUNCTION DEFINITIONS -----------------------------------------------------

void Product::Setup( int newProdId, string newName, float newPrice, int newCalories )
{
  // TODO: Implement me!
}

void Product::PrintTableRow() const
{
  // TODO: Implement me!
}

// -- ADD FUNCTION DEFINITIONS HERE --------------------------------------------------
void SetupProducts( Product& p1, Product& p2, Product& p3 )
{
  // TODO: Implement me!
}

void PrintProductTable( const Product& p1, const Product& p2, const Product& p3 )
{
  cout << left;
  cout << "PRODUCTS" << endl;
  cout
    << setw( 5 ) << "ID"
    << setw( 10 ) << "PRICE"
    << setw( 10 ) << "CALORIES"
    << setw( 50 ) << "NAME"
    << endl << string( 80, '-' ) << endl;
  
  // TODO: Implement me!
}

