--------------------------------------------------------

# INTRODUCTION

(Scroll down further for program instructions)

## CLASSES

Like structs, classes are a way we can create our own data type and group related variables together under one name. With classes, we can also add MEMBER FUNCTIONS (aka METHODS) to the class declaration. These functions belong to the class, and are there to operate on the MEMBER VARIABLES within it. (Structs also support functions, but the best practice is to only use structs for very small structures).

Class declarations look like this:

```
class CLASSNAME
{
  public:
    // PUBLIC MEMBERS
  private:
    // PRIVATE MEMBERS
}; // << DON'T FORGET THE ;
```

## UML DIAGRAMS FOR CLASSES

| Day                               | << Class name                            |
| --------------------------------- | ---------------------------------------- |
| `- m_num : int`                   | << Member variables go here              |
| `-m_month : string`               | << "`-`" denotes "private" accessibility |
| --------------------------------- |                                          |
| `+ SetNum( newNum : int ) : void` | << Member functions (aka methods)        |
| `+GetNum() : int`                 |                                          |
| `+Display() : void`               |                                          |

**Parts of a UML diagram:** With UML diagrams, the top section is for the CLASS NAME. The middle contains the MEMBER VARIABLES. And the bottom contains the MEMBER FUNCTIONS (aka METHODS).

**Accessibility levels:** The "-" sign denotes that the member variable or function should be `private`, and the "+" sign denotes `public`.

**NAME : TYPE**  UML items are written in this format:  `VARNAME : TYPE` , this is to be
programming language agnostic, so it can be translated into other languages. You will
have to "translate" the UML to C++ code, this is an important part of software development.

| UML                               | C++ translation            |
| --------------------------------- | -------------------------- |
| `- total_cats : int`              | `int total_cats;`          |
| `+ Display() : void`              | `void Display();`          |
| `+ GetCatCount() : int`           | `int GetCatCount();`       |
| `+ Add( a : int, b : int ) : int` | `int Add( int a, int b );` |

## CLASS MEMBER VARIABLE NAMES

While not required, I prefix my CLASS MEMBER VARIABLES with the `m_` prefix (which stands for "member"). There are a couple of benefits to this:

* Easily tell what variables are MEMBER VARIABLES vs. function parameters or local variables.
* If a function contains a parameter with the same name ("month") you don't have a naming conflict with the member variable ("m_month").

## CLASS MEMBER FUNCTIONS (METHODS) DEFINITIONS

The function definitions for class' member functions looks a little different from functions we've used so far. In particular, EVERY FUNCTION NAME needs to be prefixed with the CLASS NAME and then `::`, the SCOPE RESOLUTION OPERATOR.
For example:

```
void Day::Display()
{
  cout << m_num << " of " << m_month << endl;
}
```

## BUILDING YOUR PROGRAM

use `cd` to navigate to the folder
use `g++` to build the code:

`g++ *.cpp *.h -o PROGRAMNAME.out`

## EXAMPLE PROGRAM OUTPUT

Runner:

```
./program.out run
Pull how many cards? 5
Card #0: 8-D
Card #1: 9-D
Card #2: Q-D
Card #3: A-H
Card #4: 2-C
```

Tester:

```
./program.out test

CARD TESTS
 [PASS] TEST 9: Use card.Setup( "A", 'D' ); and check rank and suit.
	 Card card;      card.Setup( "A", 'D' ); 
	 EXPECTATION:    card.m_rank is "A".
	 ACTUAL:         card.m_rank is "A".
	 EXPECTATION:    card.m_suit is 'D'.
	 ACTUAL:         card.m_suit is 'D'.

 [PASS] TEST 10: Use card.Setup( "5", 'H' ); and check rank and suit.
	 Card card;      card.Setup( "5", 'H' ); 
	 EXPECTATION:    card.m_rank is "5".
	 ACTUAL:         card.m_rank is "5".
	 EXPECTATION:    card.m_suit is 'H'.
	 ACTUAL:         card.m_suit is 'H'.
```

--------------------------------------------------------

# PROGRAM INSTRUCTIONS

## CLASS DECLARATION

Within **Card.h** a class declaration has already been written:

```
class Card
{
    private:
    string m_rank;    // Rank (A, 2-10, J, Q, K)
    char m_suit;      // Suit (Diamonds, Spades, Hearts, Clubs)

    public:
    void Setup( string newRank, char newSuit ); // Set up a card's values
    void Display();                             // Show a card's info

    friend void CardTest();                     // Helps the tester (ignore)
};
```

You will be implementing the CLASS METHODS, which are in the corresponding SOURCE (.cpp) FILE.

## CLASS MEMBER FUNCTION DEFINITIONS

1. `Card::Setup function`
    **Input parameters:** `newRank` - a string, `newSuit` - a char
    **Output return:** void
    **Functionality:** The Card class has the member variables `m_rank` and `m_suit`.
      This function takes in parameters `newRank` and `newSuit`, which are meant to give new values to these variables.
      Copy the values from the parameters to the member variables with a simple assignment statement:
      `m_rank = newRank;`

2. `Card::Display function`
    **Input parameters:** none
    **Output return:** void
    **Functionality:** Use a `cout` statement to display the values of the member variables `m_rank` and `m_suit`.

## MAIN PROGRAM

The contents of `main()` have also already been written. Running the program requires a second argument, either "run" to run a simple program or "test" to run the unit tests.
You do not need to make any modifications to main.cpp, but look through the file and look at how the class variable object is created and used.
