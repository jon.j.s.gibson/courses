/*
YOU DO NOT NEED TO MODIFY THIS FILE.
*/

#include "CardTests.h"

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

#include "Card.h"

void CardTest()
{
    cout << endl << "CARD TESTS" << endl;

    int testsPass = 0;
    int totalTests = 0;

    {
        Card card;
        card.Setup( "A", 'D' );
        if      ( card.m_rank != "A" )    { cout << "\033[0;31m [FAIL] "; }
        else if ( card.m_suit != 'D' )    { cout << "\033[0;31m [FAIL] "; }
        else                            { testsPass++; cout << "\033[0;32m [PASS] "; }
        totalTests++;
        cout << "TEST 9: Use card.Setup( \"A\", 'D' ); and check rank and suit." << endl;
        cout << "\033[0m";
        cout << "\t Card card;      card.Setup( \"A\", 'D' ); " << endl;
        cout << "\t EXPECTATION:    card.m_rank is \"A\"." << endl;
        cout << "\t ACTUAL:         card.m_rank is \"" << card.m_rank << "\"." << endl;
        cout << "\t EXPECTATION:    card.m_suit is 'D'." << endl;
        cout << "\t ACTUAL:         card.m_suit is '" << card.m_suit << "'." << endl;
        cout << endl;
    }

    {
        Card card;
        card.Setup( "5", 'H' );
        if      ( card.m_rank != "5" )    { cout << "\033[0;31m [FAIL] "; }
        else if ( card.m_suit != 'H' )    { cout << "\033[0;31m [FAIL] "; }
        else                            { testsPass++; cout << "\033[0;32m [PASS] "; }
        totalTests++;
        cout << "TEST 10: Use card.Setup( \"5\", 'H' ); and check rank and suit." << endl;
        cout << "\033[0m";
        cout << "\t Card card;      card.Setup( \"5\", 'H' ); " << endl;
        cout << "\t EXPECTATION:    card.m_rank is \"5\"." << endl;
        cout << "\t ACTUAL:         card.m_rank is \"" << card.m_rank << "\"." << endl;
        cout << "\t EXPECTATION:    card.m_suit is 'H'." << endl;
        cout << "\t ACTUAL:         card.m_suit is '" << card.m_suit << "'." << endl;
        cout << endl;
    }

    cout << endl << string( 80, '*' ) << endl
        << testsPass << " tests passed out of " << totalTests << " total tests"
        << endl << string( 80, '*' ) << endl;
}
