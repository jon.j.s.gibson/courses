/*
YOU DO NOT NEED TO MODIFY THIS FILE.
*/

#ifndef _COIN
#define _COIN

#include <cstdlib> // rand()
#include <string>
using namespace std;

// -- CLASS DECLARATION GOES HERE --------------------------------------------------

class Coin
{
    public:
    Coin( string side1, string side2 ); // The text on each side of the coin
    string Flip();                      // Gets the result of flipping the coin

    private:
    string m_side1, m_side2; // What's on side 1 and side 2

    friend void CoinTest();   // This helps the tester (ignore)
};

#endif
