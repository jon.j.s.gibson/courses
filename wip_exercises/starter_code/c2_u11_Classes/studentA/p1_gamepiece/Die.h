/*
YOU DO NOT NEED TO MODIFY THIS FILE.
*/

#ifndef _DIE
#define _DIE

#include <cstdlib> // rand()

// -- CLASS DECLARATION GOES HERE --------------------------------------------------

class Die
{
    public:
    Die();                    // Initialize die with default info
    Die( int sideCount );     // Initialize die with some # of sides
    int Roll();               // Roll the die, return the result

    private:
    int m_sides;              // The amount of sides this die has


    friend void DiceTest();   // This just gives access to the UNIT TESTS, don't worry about it. (CS235 topic).
};

#endif
