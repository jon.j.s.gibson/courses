--------------------------------------------------------
ABOUT AUTOMATED TESTS
--------------------------------------------------------
For these exercise assignments, I've written AUTOMATED TESTS using BASH SCRIPTS (those .sh files) to automate building and running the programs.

You should start by BUILDING AND TESTING MANUALLY, according to the instructions.txt files within each program's folder, but you can also use the AUTOMATED TESTS once you're done to see what the instructor will initially see during the first phase of grading.

The instructor will also look at your code, but the automated tests help with making sure the functionality works as intended without the instructor having to manually build and run each program.


--------------------------------------------------------
HOW TO RUN AUTOMATED TESTS
--------------------------------------------------------
Just run it like a program:
./testA.sh

If it doesn't let you run it and gives you an error message like this:
~/U02EX-main$ ./testA.sh
bash: ./testA.sh: Permission denied

Then first run `chmod` to CHange the file MODe to allow execution:
chmod +x *.sh

Then run it again:
