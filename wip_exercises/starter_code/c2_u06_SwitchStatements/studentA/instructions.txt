--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- SWITCH STATEMENTS  --
Switch statements are a way you can branch your code based on the VALUE of a given VARIABLE. The Switch statement itself takes in a variable, then each CASE represents a value it could have.

switch( var )
{
  case 1:
    // CODE FOR var == 1
  break;
  
  case 2:
    // CODE FOR var == 2
  break;
  
  case 3:
    // CODE FOR var == 3
  break;
  
  default:
    // CODE FOR else
}

-- BUILDING YOUR PROGRAM --
cd studentA
g++ translator.cpp -o translator.out


-- EXAMPLE PROGRAM OUTPUT --
./translator.out e
cat = kato

./translator.out s
cat = gato

./translator.out m
cat = mao

./translator.out h
cat = billee

./translator.out x
cat = ?

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

ARGUMENTS: This program expects additional arguments:
1. language           A language code

Get args[1][0] and store it as a char variable - the language code.

Create a string for english and assign it "cat".

Create a string for the translated text.

Use a switch statement to assign a value to the translate variable, based on the language code:
  language code is 'e': then store "kato" in the translate variable.
  language code is 's': then store "gato" in the translate variable.
  language code is 'm': then store "mao" in the translate variable.
  language code is 'h': then store "billee" in the translate variable.
  otherwise: store "?" in the translate variable.

Afterwards, display english = translated.
