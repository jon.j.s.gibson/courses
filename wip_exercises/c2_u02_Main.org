# -*- mode: org -*-

#+TITLE: Unit 02: main()
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----

* Introduction

- About ::
- Goals ::
  - Practice creating C++ programs
  - Practice building and running programs
  - Practice finding and fixing bugs
- Setup ::

-----

* Overview

** Code files

** Building and running your program

** main() - with and without arguments

** The iostream library

** Types of errors

** Tips

-----

* Student A Instructions

** Program 1: Greeting

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porta nibh ut nibh rutrum condimentum. Quisque at dolor sem. Integer quis dapibus dolor, volutpat egestas orci. Suspendisse eget volutpat arcu, vitae sollicitudin ligula. Morbi gravida eros ac augue mollis mattis. Vivamus sed feugiat felis. Ut eros quam, posuere at arcu sed, blandit gravida dui. Suspendisse nec est finibus, elementum quam non, pharetra arcu. Aliquam varius semper tellus. Pellentesque porttitor bibendum massa at sollicitudin. Praesent sit amet vehicula sapien. Pellentesque eget ante imperdiet, posuere tortor vitae, elementum neque. Pellentesque lacinia eget dui ac scelerisque. 

** Program 2: Arguments

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porta nibh ut nibh rutrum condimentum. Quisque at dolor sem. Integer quis dapibus dolor, volutpat egestas orci. Suspendisse eget volutpat arcu, vitae sollicitudin ligula. Morbi gravida eros ac augue mollis mattis. Vivamus sed feugiat felis. Ut eros quam, posuere at arcu sed, blandit gravida dui. Suspendisse nec est finibus, elementum quam non, pharetra arcu. Aliquam varius semper tellus. Pellentesque porttitor bibendum massa at sollicitudin. Praesent sit amet vehicula sapien. Pellentesque eget ante imperdiet, posuere tortor vitae, elementum neque. Pellentesque lacinia eget dui ac scelerisque. 

-----

* Student B Instructions

** Program 1: Greeting

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porta nibh ut nibh rutrum condimentum. Quisque at dolor sem. Integer quis dapibus dolor, volutpat egestas orci. Suspendisse eget volutpat arcu, vitae sollicitudin ligula. Morbi gravida eros ac augue mollis mattis. Vivamus sed feugiat felis. Ut eros quam, posuere at arcu sed, blandit gravida dui. Suspendisse nec est finibus, elementum quam non, pharetra arcu. Aliquam varius semper tellus. Pellentesque porttitor bibendum massa at sollicitudin. Praesent sit amet vehicula sapien. Pellentesque eget ante imperdiet, posuere tortor vitae, elementum neque. Pellentesque lacinia eget dui ac scelerisque. 

** Program 2: Arguments

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porta nibh ut nibh rutrum condimentum. Quisque at dolor sem. Integer quis dapibus dolor, volutpat egestas orci. Suspendisse eget volutpat arcu, vitae sollicitudin ligula. Morbi gravida eros ac augue mollis mattis. Vivamus sed feugiat felis. Ut eros quam, posuere at arcu sed, blandit gravida dui. Suspendisse nec est finibus, elementum quam non, pharetra arcu. Aliquam varius semper tellus. Pellentesque porttitor bibendum massa at sollicitudin. Praesent sit amet vehicula sapien. Pellentesque eget ante imperdiet, posuere tortor vitae, elementum neque. Pellentesque lacinia eget dui ac scelerisque. 

-----

* Student C Instructions

** Program 1: Greeting

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porta nibh ut nibh rutrum condimentum. Quisque at dolor sem. Integer quis dapibus dolor, volutpat egestas orci. Suspendisse eget volutpat arcu, vitae sollicitudin ligula. Morbi gravida eros ac augue mollis mattis. Vivamus sed feugiat felis. Ut eros quam, posuere at arcu sed, blandit gravida dui. Suspendisse nec est finibus, elementum quam non, pharetra arcu. Aliquam varius semper tellus. Pellentesque porttitor bibendum massa at sollicitudin. Praesent sit amet vehicula sapien. Pellentesque eget ante imperdiet, posuere tortor vitae, elementum neque. Pellentesque lacinia eget dui ac scelerisque. 

** Program 2: Arguments

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porta nibh ut nibh rutrum condimentum. Quisque at dolor sem. Integer quis dapibus dolor, volutpat egestas orci. Suspendisse eget volutpat arcu, vitae sollicitudin ligula. Morbi gravida eros ac augue mollis mattis. Vivamus sed feugiat felis. Ut eros quam, posuere at arcu sed, blandit gravida dui. Suspendisse nec est finibus, elementum quam non, pharetra arcu. Aliquam varius semper tellus. Pellentesque porttitor bibendum massa at sollicitudin. Praesent sit amet vehicula sapien. Pellentesque eget ante imperdiet, posuere tortor vitae, elementum neque. Pellentesque lacinia eget dui ac scelerisque. 

-----


#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
bash
#+END_SRC

- Python :: 
#+BEGIN_SRC python :class python
#+END_SRC

- Lua :: 
#+BEGIN_SRC lua :class lua
#+END_SRC

- C++ :: 
#+BEGIN_SRC cpp :class cpp
#+END_SRC

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
*Task:* asdfasdf
#+END_HTML

#+ATTR_HTML: :class hint
#+NAME: content
#+BEGIN_HTML
*Context:* asdfasdf
#+END_HTML


#+BEGIN_QUOTE
Quote thing
#+END_QUOTE

-----
