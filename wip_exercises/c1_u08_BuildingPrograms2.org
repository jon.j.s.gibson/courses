# -*- mode: org -*-

#+TITLE: CS 134 Unit 08: Building Programs 2
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----

Create your own source code files in your replit project. Remember to use the correct file extension:

| File extension | Language | Run command                                    |
|----------------+----------+------------------------------------------------|
| .py            | Python   | python3 FILENAME.py                            |
| .cpp           | C++      | g++ FILENAME.cpp -o program1 && ./program1.out |

This will all be one program, but building up on each other, so at the end you should only have one file in the project.

*As you're working on the program, make sure to try to build (for C++) and run regularly!* It's easier to fix syntax errors if you catch them early!
I care much more about /working code/, even if it's incomplete. I do not care for "complete" code that doesn't actually run.


-----


* Program part 1: Variables

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
RECIPE: Granny Cream's Hot Butter Ice Cream
LIKES:  80

MOST LIKES FOR A RECIPE: 100
#+END_SRC

([[https://www.youtube.com/watch?v=1ErixjISdtA][Joke reference]])

- When the program starts, display a name for your program, such as "RECIPE WEBSITE EXCEPT IT'S 1985".
- Create a string variable called =recipe_name=, set to whatever you'd like, and an int variable called =total_likes=, initialized to any positive number.
- Create an integer variable called =top_likes=, and set it to some other positive number. (We don't have a recipe for this yet, just pretend it's the treshold to become most popular recipe.)
- Display the =recipe_name= and =total_likes= to the screen, as well as the =top_likes=.

-----

* Program part 2: Getting input

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
RECIPE: Granny Cream's Hot Butter Ice Cream
LIKES:  80

MOST LIKES FOR A RECIPE: 100

Do you like the recipe?
1. Like, 0. Don't like
Your choice: 1

Likes is now 81
#+END_SRC

- Create an integer variable =new_likes= and initialize it to 0.
- Ask the user "Do you like the recipe? 1. Like, 0. Don't like"
- Get the user's input as an integer (1 or 0), store it in the =new_likes= variable.
- Add the =new_likes= onto the =total_likes= variable, store the sum back in the =total_likes= variable.
- Display the updated =total_likes= afterward.

-----

* Program part 3: If statements

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
RECIPE: Granny Cream's Hot Butter Ice Cream
LIKES:  100

MOST LIKES FOR A RECIPE: 100

Do you like the recipe?
1. Like, 0. Don't like
Your choice: 1

Likes is now 101
TOP RATED RECIPE!

MOST LIKES FOR A RECIPE: 101
#+END_SRC

After the user selects whether to add 1 "like" to the recipe, use an if statement to compare the recipe's likes to the top likes - if the recipe now has /more/ likes than the maximum one, do the following:
1. Display "TOP RATED RECIPE!"
2. Update the =top_likes= variable with the amount of likes our recipe has (=total_likes=).
3. Display the updated top likes of a recipe

-----

* Program part 4: While loops

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
------------------------
RECIPE: Granny Cream's Hot Butter Ice Cream
LIKES:  100
MOST LIKES FOR A RECIPE: 100

OPTIONS:
1. Like recipe
2. Quit

Your choice: 1

Likes is now 101
TOP RATED RECIPE!

------------------------
RECIPE: Granny Cream's Hot Butter Ice Cream
LIKES:  101
MOST LIKES FOR A RECIPE: 101

OPTIONS:
1. Like recipe
2. Quit

Your choice: 1

Likes is now 101
TOP RATED RECIPE!

#+END_SRC

Add a *program while loop*, so your program will be structured like this...

Pseudocode:

#+BEGIN_SRC pseudocode :class pseudocode
  INIT PROGRAM

  WHILE ( RUNNING )

     MAIN MENU
     GET INPUT
     MAKE UPDATES

  END WHILE

  GOODBYE
#+END_SRC

Your *Initialization* will be where you create your variables. Beyond what you already have, you'll also need
a boolean variable like =running= and initialize it to =True= (Python) or =true= (C++).

Next, you'll have a *while loop*, and it will continue running *while the =running= variable is true*.

Within the while loop is where most of the program goes... Display the recipe, the top likes, and then a main menu.
For now, the menu will let the user *like* the recipe or *quit*. If they select the quit option, then
set =running= to =False= (Python) or =false= (C++), and then the program will end when it hits the next loop.

-----

* Program part 5: Lists/vectors

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
------------------------
RECIPE 0: Granny Cream's Hot Butter Ice Cream, LIKES: 100
RECIPE 1: Chocolate Chip Cookies, LIKES: 50
RECIPE 2: Instant Hot Cocoa, LIKES: 14

MOST LIKES FOR A RECIPE: 100

OPTIONS:
0. Like recipe 0
1. Like recipe 1
2. Like recipe 2
3. Quit

Your choice: 1

Likes is now 51

------------------------
RECIPE 0: Granny Cream's Hot Butter Ice Cream, LIKES: 100
RECIPE 1: Chocolate Chip Cookies, LIKES: 50
RECIPE 2: Instant Hot Cocoa, LIKES: 14

MOST LIKES FOR A RECIPE: 100

OPTIONS:
0. Like recipe 0
1. Like recipe 1
2. Like recipe 2
3. Quit

Your choice: 0

Likes is now 101
TOP RATED RECIPE!

#+END_SRC


** Replacing the single recipe with a list of recipes
During the INIT PHASE of the program, instead of storing a single recipe name, create
a list/vector of =recipe_names=. These will be strings. Create at least 3 recipe names in the list/vector.
Replace the =total_likes= variable with a =recipe_likes= integer list/vector. Initialize 3 values.

** Displaying all the recipe names
Within the while loop instead of displaying one recipe, display all the recipes and their corresponding likes.
For example, =recipe_names[0]= and =recipe_likes[0]= are related so they're displayed together.
*You don't need to use a for loop here, but you can if you want to. Start with the hard-coded version first to make sure you have that working first.*

** Add more menu options
Change up the menu to allow the user to add a like to any of the three recipes. You'll need to update your if/else if statements after the user input.

- If choice is 0, add 1 like to =recipe_likes[0]=
- Else, if choice is 1, add 1 like to =recipe_likes[1]=
- Else, if choice is 2, add 1 like to =recipe_likes[2]=
- Else, if choice is 3, then set =running= to False/false.


Note: To add 1 to a variable and save the change, make sure you're writing it as an assignment statement:

General form: =num = num + 1=









-----
