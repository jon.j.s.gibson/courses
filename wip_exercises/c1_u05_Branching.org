# -*- mode: org -*-

#+TITLE: CS134 Unit 05 Exercise: Branching with if statements
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----

* Introduction

*About:*
If statements are a way we can tell a program to only execute certain instructions /if/ some condition is TRUE.

*Goals:*
  - Practice with *if* statements
  - Practice with *if/else* statements
  - Practice with *if/else if* statements
  - Practice with logic operators: *and* =&&=, *or* =||=, *not* =!=
    
*Solo/group:* On Replit this assignment is made so that you work in your own project. However, you can still collaborate with classmates and ask questions to help figure out how to finish the program.

*Running your program:*
- Python ::
  - =cd python=
  - =python3 PROGRAM.py=       
- C++ ::
  - =cd cpp=
  - =g++ PROGRAM.cpp=
  - =./a.out=

-----

* Reference: Writing if, if/else, and if/else if statements

** If statements

- Adding an =if= statement allows us to execute some special code /only if/ some condition is met.
- If that *condition* evaluates to =false=, then the if statement's codeblock is skipped.

#+ATTR_HTML: :class left-side
#+NAME: left-side
#+BEGIN_HTML

- Python ::
  An if statement ends with a colon =:= and anything INSIDE the code block is tabbed forward
  by 1 tab. Code that retursn to the original tab position is AFTER the code block.
  
#+BEGIN_SRC python :class python
  statement
  statement

  if ( CONDITION ):
      condition_true_inside_statement
      condition_true_inside_statement

  statement
  statement
#+END_SRC


- C++ :: With C++, a code block begins at the opening curly brace ={= and ends at the closing
  curly brace =}=:
#+BEGIN_SRC cpp :class cpp
  statement
  statement

  if ( CONDITION )
  {
      condition_true_inside_statement
      condition_true_inside_statement
  }

  statement
  statement
#+END_SRC


#+END_HTML

#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

[[file:images/c1_u05_Branching_if.png]]

#+END_HTML

#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML

#+END_HTML


** If/else statements

- The =else= statement is executed when the =if= statement's CONDITION evaluate to =false=.
- This is usually a way to have some "default" functionality if the if statement doesn't get triggered.
- Note that =else= statements DO NOT HAVE A CONDITION, so no =( CONDTIION )= portion!


#+ATTR_HTML: :class left-side
#+NAME: left-side
#+BEGIN_HTML

- Python ::
#+BEGIN_SRC python :class python
  statement
  statement

  if ( CONDITION ):
      condition_true_inside_statement
      condition_true_inside_statement
      
  else:
      condition_false_inside_statement
      condition_false_inside_statement

  statement
  statement
#+END_SRC

- C++ :: 
#+BEGIN_SRC cpp :class cpp
  statement
  statement

  if ( CONDITION )
  {
      condition_true_inside_statement
      condition_true_inside_statement
  }
  else
  {
      condition_false_inside_statement
      condition_false_inside_statement
  }

  statement
  statement
#+END_SRC


#+END_HTML

#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

[[file:images/c1_u05_Branching_ifelse.png]]

#+END_HTML

#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML

#+END_HTML



** If/else if statements

- if/else if statements are a way we can check for multiple *conditions*, one after another.
- The =if= condition is always evaluated FIRST, with each subsequent =else if= condition being
  evaluated in order (from top-to-bottom).
- The =else= case is optional and can be left off if desired.
- The =else= case gets executed if all =if= and =else if= statements' conditions evaluated to =false=.
- Note that Python uses "=elif=" and C++ uses "=else if=".

#+ATTR_HTML: :class left-side
#+NAME: left-side
#+BEGIN_HTML


- Python ::
#+BEGIN_SRC python :class python
  statement
  statement

  if ( CONDITION ):
      condition_true_inside_statement
      condition_true_inside_statement

  elif ( CONDITION2 ):
      condition2_true_inside_statement
      condition2_true_inside_statement

  elif ( CONDITION3 ):
      condition3_true_inside_statement
      condition3_true_inside_statement
      
  else:
      all_conditions_false_inside_statement
      all_conditions_false_inside_statement

  statement
  statement
#+END_SRC

- C++ :: 
#+BEGIN_SRC cpp :class cpp
  statement
  statement

  if ( CONDITION )
  {
      condition_true_inside_statement
      condition_true_inside_statement
  }
  else if ( CONDITION2 )
  {
      condition2_true_inside_statement
      condition2_true_inside_statement
  }
  else if ( CONDITION3 )
  {
      condition3_true_inside_statement
      condition3_true_inside_statement
  }
  else
  {
      all_conditions_false_inside_statement
      all_conditions_false_inside_statement
  }

  statement
  statement
#+END_SRC


#+END_HTML

#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

[[file:images/c1_u05_Branching_ifelseif.png]]

#+END_HTML

#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML

#+END_HTML

-----

* Program 1: If statement

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
*Task:* Write a program that asks the user for the points possible
on an assignment and how many points they earned. Display their score %
and if that score is 100%, also display "PERFECT!".

Steps are below.
#+END_HTML


#+ATTR_HTML: :class left-side
#+NAME: left-side
#+BEGIN_HTML

1. Ask the user "How many points possible?". Get their input as a float, store in =points_possible=.
2. Ask the user "How many points earned?". Get their input as a float, store in =earned_points=.
3. Calculate their score percent with:
   =earned_points / points_possible * 100=. Store this in a new float variable, =score=.
4. Display =score_percent= to the screen.
5. If the =score_percent= is greater than or equal to 100, then also display "PERFECT!".
6. Display "Goodbye" at the end of the program.


*Example output - not a perfect score:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
How many points possible? 80
How many poins did you earn? 75
Result: 93.75%
#+END_SRC

*Example output - perfect score:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
How many points possible? 60
How many poins did you earn? 60
Result: 100% (PERFECT!)
#+END_SRC


#+END_HTML

#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML
[[file:images/c1_u05_Branching_p1.png]]
#+END_HTML

#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML

#+END_HTML

-----

* Program 2: If/else statement
  
#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
*Task:* Write a program that asks the user to enter a number.
If that number is greater than 0, then display "POSITIVE".
Otherwise, display "NEGATIVE OR ZERO".

Steps are below.
#+END_HTML


#+ATTR_HTML: :class left-side
#+NAME: left-side
#+BEGIN_HTML

1. Ask the user to "Enter a number". Get their input as an int an store it in a variable =num=.
2. If the `num` they entered is greater than 0, then display "POSITIVE".
3. Else, display "NEGATIVE OR ZERO".
4. Display "Goodbye" at the end of the program.

*Example output - positive number:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter a number: 10
POSITIVE
#+END_SRC

*Example output - negative number:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter a number: -50
NEGATIVE OR ZERO
#+END_SRC

*Example output - zero:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter a number: 0
NEGATIVE OR ZERO
#+END_SRC



#+END_HTML

#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML
[[file:images/c1_u05_Branching_p2.png]]
#+END_HTML

#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML

#+END_HTML

-----

* Program 3: If/else if statement

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
*Task:* Write a program that asks the user for the % charge on their phone battery.
Display an "image" of a battery based on the charge level:

| Charge        | Image    |
|---------------+----------|
| 90% and above | =[####}= |
| 75% and above | =[###_}= |
| 50% and above | =[##__}= |
| 25% and above | =[#___}= |
| below 25%     | =[____}= |

Steps are below.
#+END_HTML


[[file:images/c1_u05_Branching_p3.png]]

1. Ask the user to "Enter their battery charge %". Get their input as an int an store it in a variable `charge`.
2. If the =charge= is 90 or above, display: =[####}=
3. Else, if =charge= is 75 or above, display: =[###_}=
4. Else, if =charge= is 50 or above, display: =[##__}=
5. Else, if =charge= is 25 or above, display: =[#___}=
6. Else, display =[____}=
7. Display "Goodbye" at the end of the program.


*Example output - full battery:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter your battery charge %: 95
Battery: [####}
#+END_SRC

*Example output - 75% battery:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter your battery charge %: 79
Battery: [###_}
#+END_SRC

*Example output - 50% battery:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter your battery charge %: 53
Battery: [##__}
#+END_SRC

*Example output - 25% battery:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter your battery charge %: 27
Battery: [#___}
#+END_SRC

*Example output - 0% battery:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter your battery charge %: 5
Battery: [____}
#+END_SRC



-----

* Program 4: Operators
  
#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
*Task:* Write a program that asks the user for an *x* and a *y* coordinate.
Based on their response, it will display what *quadrant* that point is in,
or if it is an intercept.

[[file:images/c1_u05_Branching_coordinateplane.png]]

Steps are below.
#+END_HTML


[[file:images/c1_u05_Branching_p4.png]]

1. Ask the user "Enter an X coordinate" and store their choice as a float variable named =x=.
2. Ask the user "Enter a Y coordinate" and store their choice as a float variable named =y=.
3. If =x= is positive AND =y= is positive, then display "Point is in quadrant I"
4. If =x= is negative AND =y= is positive, then display "Point is in quadrant II"
5. If =x= is negative AND =y= is negative, then display "Point is in quadrant III"
6. If =x= is positive AND =y= is negative, then display "Point is in quadrant IV"
7. OTHERWISE, display "Intercept"
8. Display "Goodbye" at the end of the program.


*Example output - quadrant I:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter an x coordinate: 10
Enter a y coordinate: 20
Point is in Quadrant I
#+END_SRC

*Example output - quadrant II:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter an x coordinate: -5
Enter a y coordinate: 20
Point is in Quadrant II
#+END_SRC

*Example output - quadrant III:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter an x coordinate: -9
Enter a y coordinate: -6
Point is in Quadrant III
#+END_SRC

*Example output - quadrant IV:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter an x coordinate: 10
Enter a y coordinate: -5
Point is in Quadrant IV
#+END_SRC

*Example output - intercept:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter an x coordinate: 10
Enter a y coordinate: 0
Point is an intercept
#+END_SRC



-----

* Program 5: Freeform

Implement whatever you'd like, as long as it meets the following criteria:

1. Program should contain at least one =if/else= OR =if/else if= statement.
2. Program should display a TITLE at the start and a GOODBYE at the end.
3. Make sure to clearly *prompt* the user before getting input (that is, saying "please enter yadayadayada" before getting input).



#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
MY PROGRAM

(Put your program logic here)

GOODBYE
#+END_SRC


-----
