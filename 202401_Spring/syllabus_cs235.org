# -*- mode: org -*-

#+LATEX_HEADER: \hypersetup{colorlinks=true,linkcolor=blue}

* Course information

| College    | Johnson County Community College                               |
| Division   | CSIT (Computer Science/Information Technology)                 |
| Instructor | Rachel Wil Singh (they/them)                                   |
| Semester   | Spring 2024 (01/22/2024 - 05/13/2024)                          |
| Course     | CS 235: Object-Oriented Programming with C++ (4 credit hours) |
| Section 1  | Section 300 / CRN 10965 / Online only                          |
| Section 2  | Section 301 / CRN 10966 / Online only                          |

- Course description :: This course emphasizes programming methodology and problem solving using the object-oriented paradigm. Students will develop software applications using the object-oriented concepts of data abstraction, encapsulation, inheritance, and polymorphism. Students will apply the C++ techniques of dynamic memory, pointers, built-in classes, function and operator overloading, exception handling, recursion and templates. 3 hrs. lecture, 2 hrs. lab by arrangement/wk.
  Catalog link: https://catalog.jccc.edu/coursedescriptions/cs/#CS_235
- Prerequisites :: CS 200 or CS 201 or CS 205.
- Drop deadlines :: To view the deadline dates for dropping this course, please refer to the schedule on the JCCC website under Admissions>Enrollment Dates> Dropping Credit Classes. After the 100% refund date, you will be financially responsible for the tuition charges; for details, search on Student Financial Responsibility on the JCCC web page. Changing your schedule may reduce eligibility for financial aid and other third party funding. Courses not dropped will be graded. For questions about dropping courses, contact the Student Success Center at 913-469-3803.
- Auto-withdraws :: Attendance for the first week of classes at JCCC are recorded and students marked as NOT IN ATTENDANCE get auto-withdrawn from the course. Please pay attention to course announcements / emails from the instructor for instructions on how to make sure you are marked as IN ATTENDANCE.
  - To learn more about reinstatement, please visit: https://www.jccc.edu/admissions/enrollment/reinstatement.html

*Instructor information*
- Name: Rachel Wil Singh, aka R.W.
- Pronouns: they/them
- Office: RC 348 H
- Email: rsingh13@jccc.edu (please use Canvas email instead)
- Office phone: (913) 469-8500 x3671

*Course delivery*

This course is set up as an *Online* course.
This means the following:
- There is no scheduled class time for this course.
- Instructions and assignments are given totally online
- You can utilize *office hours* to meet with the instructor,
  ask questions, and get help.

To see more about JCCC course delivery options, please visit: https://www.jccc.edu/student-resources/course-delivery-methods.html

#+INCLUDE: "shared/syllabus_shared1.org"

** Course supplies

1) *Textbook:* Rachel's CS 250 course notes (https://moosadee.gitlab.io/courses/202401_Spring/book_datastructurescpp.html)
2) *Zoom:* Needed for remote attendance / office hours
3) *Tools:* See the UNIT 00 SETUP assignments for steps on installation.
   - An Integrated Development Environment, such as:
     - Visual Studio Community Edition (Windows only for C++) (https://visualstudio.microsoft.com/vs/community/)
     - Code::Blocks (multiplatform and lightweight, Windows users download the "mingw-setup.exe" version) (https://www.codeblocks.org/downloads/binaries/)
     - XCode (Mac) (https://developer.apple.com/xcode/)
     - You can use whatever you prefer, or even compile from the command line.
     - The instructor builds all assignments with *g++* from a *Linux* platform.
   - Git Bash (https://git-scm.com/) for source code management.
   - You may need a document editor, such as LibreOffice Writer, MS Word, or Google Documents.
4) *Accounts:* See the UNIT 00 SETUP assignments for steps on set up.
   - GitLab (https://gitlab.com/) for code storage
5) *Optional:* Things that might be handy
   - Dark Reader plugin (Firefox/Chrome/Safari/Edge) to turn light-mode webpages into dark-mode. (https://darkreader.org/)

** Recommended experience

#+INCLUDE: "shared/syllabus_experience.org"

-----

* Course policies

** Grading breakdown

Assessment types are given a certain weight in the overall class.
Breakdown percentages are based off the course catalog requirements
(https://catalog.jccc.edu/coursedescriptions/cs/#CS_235)

- *Labs:* Weekly programming assignments to help practice new concepts (20%)
- *Exercises:* A variety of additional topics to help students learn and practice:
  - *Debugging practice:* Assignments to help students with diagnosing and debugging skills
- *Tech Literacy discussions:* Discussions about tech related areas (5%)
- *Check-ins:* Weekly Check-Ins to let me know how you're doing in the course and if you need anything additional (5%)
- *Quizzes:* Concept Introductions help students review concepts read about in the textbook (10%)
- *Projects:* Larger programming assignments that combine multiple topics (15%)
- *Exams:* Mastery Checks to help the student and instructor assess how much about a topic has been learned (40%)

*Final letter grade*: JCCC uses whole letter grades for final course grades: F, D, C, B, and A. The way I break down what your receive at the end of the semester is as follows:

| Total score                    | Letter grade |
|--------------------------------+--------------|
| 89.5% $\leq$ grade $\leq$ 100% | A            |
| 79.5% $\leq$ grade < 89.5%     | B            |
| 69.5% $\leq$ grade < 79.5%     | C            |
| 59.5% $\leq$ grade < 69.5%     | D            |
| 0% $\leq$ grade < 59.5%        | F            |

** Tentative schedule

| Week # | Monday | Topic A                                | Topic B                            | Topic C                       |
|--------+--------+----------------------------------------+------------------------------------+-------------------------------|
|      1 | Jan 15 | Unit 00: Setup                         | Unit 01: Exploring design          | Unit 02: Review - Basics      |
|      2 | Jan 22 | Unit 03: Review - Functions/classes    | Unit 04: Debugging/testing         | Unit 05: Algorithm efficiency |
|      3 | Jan 29 | Unit 06: Review - Pointers             | Unit 07: Standard Template Library |                               |
|      4 | Feb 5  | Unit 08: Recursion                     | Unit 09: Searching & sorting       |                               |
|      5 | Feb 12 | Unit 10: Templates                     | Unit 11: Exceptions                |                               |
|      6 | Feb 19 | Unit 12: Overloading functions & ctors |                                    |                               |
|      7 | Feb 26 | Unit 14: Static members                | Unit 15: Friends                   |                               |
|      8 | Mar 4  | Unit 16: Anonymous functions           | Unit 17: Polymorphism              |                               |
|      9 | Mar 11 | SPRING BREAK                           |                                    |                               |
|     10 | Mar 18 | Unit 18: Operator overloading          | Unit 19: Third party libraries     |                               |
|     11 | Mar 25 | Semester project v1                    |                                    |                               |
|     12 | Apr 1  | Semester project v1                    |                                    |                               |
|     13 | Apr 8  | Semester project v2                    |                                    |                               |
|     14 | Apr 15 | Semester project v2                    |                                    |                               |
|     15 | Apr 22 | (Catch up week)                        |                                    |                               |

** Due dates, late assignments, re-submissions
#+INCLUDE: "shared/syllabus_duedates.org"

** Attendance
#+INCLUDE: "shared/syllabus_attendance.org"

** Academic honesty
#+INCLUDE: "shared/syllabus_academichonesty.org"

** Student success tips
#+INCLUDE: "shared/syllabus_studentsuccess.org"

** Accommodations and life help
#+INCLUDE: "shared/syllabus_accommodations.org"

-----

* Additional information
#+INCLUDE: "shared/syllabus_shared.org"

-----

* Course catalog info
*Objectives:*
1. Develop C++ programs using a disciplined object-oriented approach to software development.
2. Create C++ classes using the concepts of encapsulation and data abstraction.
3. Create programs that integrate advanced programming topics.
4. Develop new classes by inheriting existing classes.
5. Implement polymorphism to produce dynamic, run-time applications.
6. Employ commonly accepted programming standards for code and documentation.

*Content Outline and Competencies:*

I.  Software Development

A. Identify C++ features needed to solve problems in C++.

B. Define the problem and identify the classes.

C. Develop a solution.

D. Code the solution.

E. Test the solution.

II  Encapsulation and Data Abstraction

A. Apply the C++ syntax to define classes.

B. Use the string class and the vector template classes.

C. Create copy constructors.

D. Create friend functions.

E. Create overloaded operators including the >>, << and = operators.

F. Implement class composition.

G. Describe and create constructors and destructors.

H. Describe use of the “this” pointer.

I. Implement classes that contain arrays of objects.

J. Compare static and instance data members in a class.

K. Compare private and public access specifiers.

III. Advanced Programming Topics

A. Establish pointers to manage dynamic memory.

1. Use new and delete operators.

2. Declare dynamic memory arrays.

3. Define and use pointers to objects.

4. Define objects containing pointers.

B. Create recursive functions.

C. Organize projects into multiple files.

D. Handle exceptions caused by unusual or error conditions during program execution.

E.  Describe and use the C++ I/O system.

F.  Create templates to develop data independent classes.

IV. Inheritance

A. Explain how inheritance can be used to develop new classes.

B. Explain the “is-a” and the “has-a” relationships between classes.

C. Describe and use constructors and destructors for inherited classes.

D. Define the base class.

E. Use inherited classes.

F. Create overloaded, inherited functions.

V. Polymorphism

A. Explain how polymorphism is used to solve programming problems.

B. Create virtual functions.

C. Create abstract classes using pure virtual functions.

D.  Explain static binding and dynamic binding.

VI. Code Standards

A. Create descriptive identifiers according to language naming conventions.

B. Write structured and readable code.

C. Create documentation.
