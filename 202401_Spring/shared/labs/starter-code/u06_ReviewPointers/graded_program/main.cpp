#include <iostream>
#include <string>
using namespace std;

#include "Array.h"

void Program();
void Tester();

int main()
{
  std::cout << "1. Run AUTOMATED TESTS" << std::endl;
  std::cout << "2. Run PROGRAMS" << std::endl;
  int choice;
  std::cout << ">> ";
  std::cin >> choice;

  switch( choice )
  {
    case 1:
    Tester();
    break;

    case 2:
    Program();
    break;
  }

    return 0;
}

void Program()
{
    cout << endl << "DYNAMIC ARRAY" << endl;
    string options[] = { "BASIC", "COBOL", "Java", "C#", "C++" };

    cout << "1. Create array" << endl;
    Array arr;
    cout << "2. Allocate space" << endl;
    arr.AllocateSpace( 3 );

    cout << "3. Add items to array, trigger resize" << endl;
    for ( int i = 0; i < 5; i++ )
    {
        arr.AddItem( options[i] );
    }

    cout << "4. Display array" << endl;
    arr.Display();

    cout << "5. Deallocate space" << endl;
    arr.DeallocateSpace();
}

void Tester()
{
    Test_Array_Constructor();
    Test_Array_AllocateSpace();
    Test_Array_DeallocateSpace();
    Test_Array_ResizeArray();
    Test_Array_AddItem();
}
