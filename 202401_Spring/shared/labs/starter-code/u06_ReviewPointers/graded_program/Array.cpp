#include "Array.h"
#include <iostream>
using namespace std;

/**
Default constructor: Initialize m_ptr to nullptr,
initialize m_arraySize and m_itemCount to 0.
*/
Array::Array()
{
    cout << "FUNCTION NOT YET IMPLEMENTED!" << endl;
}

/**
Parametersized constructor: Initialize m_ptr to nullptr,
then call AllocateSpace with the `arraySize` passed in.
*/
Array::Array( int arraySize )
{
    cout << "FUNCTION NOT YET IMPLEMENTED!" << endl;
}

/**
Destructor: Call the DeallocateSpace function.
*/
Array::~Array()
{
    cout << "FUNCTION NOT YET IMPLEMENTED!" << endl;
}

/**
If `m_ptr` is not pointing to nullptr then return early; we can't use this pointer.
Allocate space via the `m_ptr` pointer, using the `arraySize` given.
Also initialize `m_itemCount` to 0 and `m_arraySize` to the `arraySize` passed in.
*/
void Array::AllocateSpace( int arraySize )
{
    cout << "FUNCTION NOT YET IMPLEMENTED!" << endl;
}

/**
If `m_ptr` is not pointing to nullptr, then free that memory
and reset `m_ptr` to nullptr.
*/
void Array::DeallocateSpace()
{
    cout << "FUNCTION NOT YET IMPLEMENTED!" << endl;
}

/**
Allocate space for a new pointer using a size larger than `m_arraySize`.
Copy the data over from the old array to the new array.
Free the memory at the old `m_ptr` location.
Update `m_ptr` to point to the new memory address.
Update `m_arraySize` to the new size.
*/
void Array::ResizeArray()
{
    cout << "FUNCTION NOT YET IMPLEMENTED!" << endl;
}

/**
If the array is full then call the ResizeArray function.
Afterwards, add a new item to the `m_ptr` array at the `m_itemCount` position.
Increment `m_itemCount` afterwards.
*/
void Array::AddItem( string value )
{
    cout << "FUNCTION NOT YET IMPLEMENTED!" << endl;
}

/**
Display the index and element of each item in the `m_ptr` array.
*/
void Array::Display()
{
    cout << "FUNCTION NOT YET IMPLEMENTED!" << endl;
}



//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// - AUTOMATED TESTER -------------------------------------------------------//
#include "INFO.h"
void Test_Array_Constructor()
{
  cout << "\n** Test_Array_Constructor ** STUDENT: " << STUDENT_NAME << endl;
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";

  string testName;
  {
    cout << CLR;
    testName = "Default constructor sets m_ptr to nullptr?";
    Array arr;
    if ( arr.m_ptr != nullptr )
    {
        cout << RED << "[FAIL] " << testName << endl;
    }
    else if ( arr.m_arraySize != 0 )
    {
        cout << RED << "[FAIL] " << testName << " - m_arraySize is wrong!" << endl;
        cout << "m_arraySize......... " << arr.m_arraySize << " (expecting 0!)" << endl;
    }
    else if ( arr.m_itemCount != 0 )
    {
        cout << RED << "[FAIL] " << testName << " - m_itemCount is wrong!" << endl;
        cout << "m_itemCount......... " << arr.m_itemCount << " (expecting 0!)" << endl;
    }
    else
    {
        cout << GRN << "[PASS] " << testName << endl;
    }
    cout << CLR;
  }

  {
    testName = "Parameterized constructor sets initializes size?";
    Array arr( 5 );
    int expected_arraySize = 5;
    int actual_arraySize = arr.m_arraySize;

    if ( arr.m_ptr == nullptr )
    {
        cout << RED << "[FAIL] " << testName << " - m_ptr is nullptr!" << endl;
        cout << "m_ptr......... " << arr.m_ptr << endl;
        cout << "m_arraySize... " << actual_arraySize << " (expecting " << expected_arraySize << ")" << endl;
    }
    else if ( actual_arraySize != expected_arraySize )
    {
        cout << RED << "[FAIL] " << testName << " - m_arraySize is NOT " << expected_arraySize << "!" << endl;
        cout << "m_ptr......... " << arr.m_ptr << endl;
        cout << "m_arraySize... " << actual_arraySize << " (expecting " << expected_arraySize << ")" << endl;
    }
    else
    {
        cout << GRN << "[PASS] " << testName << endl;
    }
    cout << CLR;
  }

  {
    testName = "Parameterized constructor sets initializes size?";
    Array arr( 10 );
    int expected_arraySize = 10;
    int actual_arraySize = arr.m_arraySize;

    if ( arr.m_ptr == nullptr )
    {
        cout << RED << "[FAIL] " << testName << " - m_ptr is nullptr!" << endl;
        cout << "m_ptr......... " << arr.m_ptr << endl;
        cout << "m_arraySize... " << actual_arraySize << " (expecting " << expected_arraySize << ")" << endl;
    }
    else if ( actual_arraySize != expected_arraySize )
    {
        cout << RED << "[FAIL] " << testName << " - m_arraySize is NOT " << expected_arraySize << "!" << endl;
        cout << "m_ptr......... " << arr.m_ptr << endl;
        cout << "m_arraySize... " << actual_arraySize << " (expecting " << expected_arraySize << ")" << endl;
    }
    else
    {
        cout << GRN << "[PASS] " << testName << endl;
    }
    cout << CLR;
  }
}

void Test_Array_AllocateSpace()
{
  cout << "\n** Test_Array_AllocateSpace ** STUDENT: " << STUDENT_NAME << endl;
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  string testName;

  {
    testName = "Allocate space and set array size?";
    Array arr;
    arr.AllocateSpace( 5 );
    int expected_arraySize = 5;
    int actual_arraySize = arr.m_arraySize;
    int expected_itemCount = 0;
    int actual_itemCount = arr.m_itemCount;

    if ( arr.m_ptr == nullptr )
    {
        cout << RED << "[FAIL] " << testName << " - m_ptr is nullptr!" << endl;
        cout << "m_ptr......... " << arr.m_ptr << endl;
        cout << "m_arraySize... " << actual_arraySize << " (expecting " << expected_arraySize << ")" << endl;
    }
    else if ( actual_arraySize != expected_arraySize )
    {
        cout << RED << "[FAIL] " << testName << " - m_arraySize is NOT " << expected_arraySize << "!" << endl;
        cout << "m_ptr......... " << arr.m_ptr << endl;
        cout << "m_arraySize... " << actual_arraySize << " (expecting " << expected_arraySize << ")" << endl;
    }
    else if ( actual_itemCount != expected_itemCount )
    {
        cout << RED << "[FAIL] " << testName << " - m_itemCount is NOT " << expected_itemCount << "!" << endl;
        cout << "m_ptr......... " << arr.m_ptr << endl;
        cout << "m_itemCount... " << actual_itemCount << " (expecting " << expected_itemCount << ")" << endl;
    }
    else
    {
        cout << GRN << "[PASS] " << testName << endl;
    }
    cout << CLR;
  }

  {
    testName = "Allocate space and set array size?";
    Array arr;
    arr.AllocateSpace( 10 );
    int expected_arraySize = 10;
    int actual_arraySize = arr.m_arraySize;

    if ( arr.m_ptr == nullptr )
    {
        cout << RED << "[FAIL] " << testName << " - m_ptr is nullptr!" << endl;
        cout << "m_ptr......... " << arr.m_ptr << endl;
        cout << "m_arraySize... " << actual_arraySize << " (expecting " << expected_arraySize << ")" << endl;
    }
    else if ( actual_arraySize != expected_arraySize )
    {
        cout << RED << "[FAIL] " << testName << " - m_arraySize is NOT " << expected_arraySize << "!" << endl;
        cout << "m_ptr......... " << arr.m_ptr << endl;
        cout << "m_arraySize... " << actual_arraySize << " (expecting " << expected_arraySize << ")" << endl;
    }
    else
    {
        cout << GRN << "[PASS] " << testName << endl;
    }
    cout << CLR;
  }
}

void Test_Array_DeallocateSpace()
{
  cout << "\n** Test_Array_DeallocateSpace ** STUDENT: " << STUDENT_NAME << endl;
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  string testName;

  {
    testName = "Sets ptr to nullptr?";
    Array arr;
    arr.m_ptr = new string[10];
    arr.DeallocateSpace();

    if ( arr.m_ptr != nullptr )
    {
        cout << RED << "[FAIL] " << testName << " - m_ptr is nullptr!" << endl;
        cout << "m_ptr......... " << arr.m_ptr << " (expecting nullptr!)" << endl;
    }
    else
    {
        cout << GRN << "[PASS] " << testName << endl;
    }
    cout << CLR;
  }
}

void Test_Array_ResizeArray()
{
  cout << "\n** Test_Array_ResizeArray ** STUDENT: " << STUDENT_NAME << endl;
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  string testName;

  {
    testName = "Resize works?";
    string itemList[2] = { "A", "B" };
    Array arr;
    arr.m_ptr = new string[2];
    arr.m_ptr[0] = itemList[0];
    arr.m_ptr[1] = itemList[1];
    int originalArraySize = arr.m_arraySize = 2;
    int originalItemCount = arr.m_itemCount = 2;
    string* originalPtr = arr.m_ptr;
    arr.ResizeArray();

    bool allItemsCopied = true;
    for ( int i = 0; i < arr.m_itemCount; i++ )
    {
        if ( arr.m_ptr[i] != itemList[i] )
        {
            allItemsCopied = false;
        }
    }

    if ( arr.m_ptr == originalPtr )
    {
        cout << RED << "[FAIL] " << testName << " - m_ptr is still pointing to old location!" << endl;
        cout << "m_ptr......... " << arr.m_ptr << " (expecting new location!)" << endl;
    }
    else if ( arr.m_itemCount != originalItemCount )
    {
        cout << RED << "[FAIL] " << testName << " - m_itemCount has changed!" << endl;
        cout << "m_itemCount......... " << arr.m_itemCount << " (it shouldn't change!)" << endl;
    }
    else if ( arr.m_arraySize == originalArraySize )
    {
        cout << RED << "[FAIL] " << testName << " - m_arraySize is still the same!" << endl;
        cout << "m_arraySize......... " << arr.m_arraySize << " (should be a bigger size!)" << endl;
    }
    else if ( !allItemsCopied )
    {
        cout << RED << "[FAIL] " << testName << " - Not all items were copied over?" << endl;
        cout << "Expected contents of array: ";
        for ( int i = 0; i < arr.m_itemCount; i++ ) { cout << itemList[i] << "\t"; }
        cout << endl;
        cout << "Actual contents of array: ";
        for ( int i = 0; i < arr.m_itemCount; i++ ) { cout << arr.m_ptr[i] << "\t"; }
        cout << endl;
    }
    else
    {
        cout << GRN << "[PASS] " << testName << endl;
    }
    cout << CLR;
  }
}

void Test_Array_AddItem()
{
  cout << "\n** Test_Array_AddItem ** STUDENT: " << STUDENT_NAME << endl;
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  string testName;

  {
    testName = "New item added to array?";
    Array arr;
    arr.m_ptr = new string[2];
    arr.m_itemCount = 0;
    arr.m_arraySize = 2;
    arr.AddItem( "AAA" );

    if ( arr.m_itemCount != 1 )
    {
        cout << RED << "[FAIL] " << testName << " - m_itemCount is wrong!" << endl;
        cout << "m_itemCount......... " << arr.m_itemCount << " (expecting 1!)" << endl;
    }
    else if ( arr.m_ptr[0] != "AAA" )
    {
        cout << RED << "[FAIL] " << testName << " - m_ptr[0] is wrong!" << endl;
        cout << "m_ptr[0]......... " << arr.m_ptr[0] << " (expecting \"AAA\"!)" << endl;
    }
    else
    {
        cout << GRN << "[PASS] " << testName << endl;
    }
    cout << CLR;
  }

  {
    testName = "Two items added to array?";
    Array arr;
    arr.m_ptr = new string[2];
    arr.m_itemCount = 0;
    arr.m_arraySize = 2;
    arr.AddItem( "AAA" );
    arr.AddItem( "BBB" );

    if ( arr.m_itemCount != 2 )
    {
        cout << RED << "[FAIL] " << testName << " - m_itemCount is wrong!" << endl;
        cout << "m_itemCount......... " << arr.m_itemCount << " (expecting 2!)" << endl;
    }
    else if ( arr.m_ptr[0] != "AAA" )
    {
        cout << RED << "[FAIL] " << testName << " - m_ptr[0] is wrong!" << endl;
        cout << "m_ptr[0]......... " << arr.m_ptr[0] << " (expecting \"AAA\"!)" << endl;
    }
    else if ( arr.m_ptr[1] != "BBB" )
    {
        cout << RED << "[FAIL] " << testName << " - m_ptr[1] is wrong!" << endl;
        cout << "m_ptr[1]......... " << arr.m_ptr[1] << " (expecting \"BBB\"!)" << endl;
    }
    else
    {
        cout << GRN << "[PASS] " << testName << endl;
    }
    cout << CLR;
  }

  {
    testName = "Adding more items triggerse resize?";
    Array arr;
    arr.m_ptr = new string[2];
    arr.m_itemCount = 0;
    arr.m_arraySize = 2;
    arr.AddItem( "AAA" );
    arr.AddItem( "BBB" );
    arr.AddItem( "CCC" );

    if ( arr.m_itemCount != 3 )
    {
        cout << RED << "[FAIL] " << testName << " - m_itemCount is wrong!" << endl;
        cout << "m_itemCount......... " << arr.m_itemCount << " (expecting 3!)" << endl;
    }
    else if ( arr.m_ptr[0] != "AAA" )
    {
        cout << RED << "[FAIL] " << testName << " - m_ptr[0] is wrong!" << endl;
        cout << "m_ptr[0]......... " << arr.m_ptr[0] << " (expecting \"AAA\"!)" << endl;
    }
    else if ( arr.m_ptr[1] != "BBB" )
    {
        cout << RED << "[FAIL] " << testName << " - m_ptr[1] is wrong!" << endl;
        cout << "m_ptr[1]......... " << arr.m_ptr[1] << " (expecting \"BBB\"!)" << endl;
    }
    else if ( arr.m_ptr[2] != "CCC" )
    {
        cout << RED << "[FAIL] " << testName << " - m_ptr[2] is wrong!" << endl;
        cout << "m_ptr[2]......... " << arr.m_ptr[2] << " (expecting \"CCC\"!)" << endl;
    }
    else
    {
        cout << GRN << "[PASS] " << testName << endl;
    }
    cout << CLR;
  }
}

