#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

// TODO: Convert this into a templated class
struct Node
{
  Node()
  {
    ptr_next = nullptr;
    ptr_prev = nullptr;
  }
  
  string data;
  Node* ptr_next;
  Node* ptr_prev;
};

// TODO: Convert this into a templated function
void PrintTable( Node* ptr1, Node* ptr2, Node* ptr3 )
{
  cout << setw( 10 ) << "NODE" << setw( 10 ) << "DATA" << setw( 20 ) << "ADDR" << setw( 20 ) << "PREV" << setw( 20 ) << "NEXT" << endl;
  cout << string( 80, '-' ) << endl;
  cout << setw( 10 ) << "Ptr 1"  << setw( 10 ) <<  ptr1->data << setw( 20 ) << &ptr1 << setw( 20 ) <<  ptr1->ptr_prev  << setw( 20 ) <<  ptr1->ptr_next << endl;
  cout << setw( 10 ) << "Ptr 2" << setw( 10 ) <<  ptr2->data << setw( 20 ) << &ptr2 << setw( 20 ) <<  ptr2->ptr_prev  << setw( 20 ) <<  ptr2->ptr_next << endl;
  cout << setw( 10 ) << "Ptr 3" << setw( 10 ) <<  ptr3->data << setw( 20 ) << &ptr3 << setw( 20 ) <<  ptr3->ptr_prev  << setw( 20 ) <<  ptr3->ptr_next << endl;
}

int main()
{
  cout << left;
  
  // TODO: Make these Node<string> types
  Node* a = new Node;
  Node* b = new Node;
  Node* c = new Node;
  
  a->ptr_next = b;
  b->ptr_next = c;
  c->ptr_prev = b;
  b->ptr_prev = a;
  
  a->data = "a";
  b->data = "b";
  c->data = "c";
  
  PrintTable( a, b, c );
  
  // TODO: Create a set of Nodes of some non-string data type
  //        and set them up similar to above.
  
  /*
  Node<char>* x = new Node<char>;
  Node<char>* y = new Node<char>;
  Node<char>* z = new Node<char>;
  x->ptr_next = y;
  y->ptr_next = z;
  z->ptr_prev = y;
  y->ptr_prev = x;
  
  x->data = 'x';
  y->data = 'y';
  z->data = 'z';
  
  PrintTable( x, y, z );
  * */
    
  // FREE THE MEMORY BEFORE EXITING PROGRAM
  delete a;
  delete b;
  delete c;
  /*
  delete x;
  delete y;
  delete z;
  * */
  
  return 0;
}






