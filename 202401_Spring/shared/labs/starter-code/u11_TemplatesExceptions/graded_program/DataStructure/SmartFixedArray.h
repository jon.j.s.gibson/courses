#ifndef _ARRAY_H
#define _ARRAY_H

// Custom exceptions:
#include "../Exception/NotImplementedException.h"
#include "../Exception/InvalidIndexException.h"
#include "../Exception/StructureEmptyException.h"
#include "../Exception/StructureFullException.h"
#include "../Exception/ItemNotFoundException.h"

#include <string>

template <typename T>
class SmartFixedArray
{
  public:
  SmartFixedArray();
  ~SmartFixedArray();

  void PushBack( T newItem );
  void PopBack();
  T& GetBack();

  size_t Search( T item ) const;

  size_t Size() const;
  bool IsEmpty() const;
  bool IsFull() const;
  void Clear();

  private:
  T data[100];
  const size_t ARRAY_SIZE;
  size_t item_count;

  void ShiftLeft( size_t from_index );
  void ShiftRight( size_t from_index );

  friend class SmartFixedArrayTester;
};

template <typename T>
SmartFixedArray<T>::SmartFixedArray()
    : ARRAY_SIZE( 100 )
{
    throw Exception::NotImplementedException( "SmartFixedArray<T>::SmartFixedArray" ); // Erase this once you work on this function
}

template <typename T>
SmartFixedArray<T>::~SmartFixedArray()
{
    // THis function intentionally left blank
}

template <typename T>
void SmartFixedArray<T>::Clear()
{
    throw Exception::NotImplementedException( "SmartFixedArray<T>::Clear" ); // Erase this once you work on this function
}

template <typename T>
size_t SmartFixedArray<T>::Size() const
{
    return this->item_count;
//    throw Exception::NotImplementedException( "SmartFixedArray<T>::Size" ); // Erase this once you work on this function
}

template <typename T>
bool SmartFixedArray<T>::IsFull() const
{
    throw Exception::NotImplementedException( "SmartFixedArray<T>::IsFull" ); // Erase this once you work on this function
}

template <typename T>
bool SmartFixedArray<T>::IsEmpty() const
{
    throw Exception::NotImplementedException( "SmartFixedArray<T>::IsEmpty" ); // Erase this once you work on this function
}

template <typename T>
size_t SmartFixedArray<T>::Search( T item ) const
{
    throw Exception::NotImplementedException( "SmartFixedArray<T>::Search" ); // Erase this once you work on this function
}


template <typename T>
void SmartFixedArray<T>::ShiftLeft( size_t index )
{
    throw Exception::NotImplementedException( "SmartFixedArray<T>::ShiftLeft" ); // Erase this once you work on this function
}

template <typename T>
void SmartFixedArray<T>::ShiftRight( size_t index )
{
    throw Exception::NotImplementedException( "SmartFixedArray<T>::ShiftRight" ); // Erase this once you work on this function
}

template <typename T>
void SmartFixedArray<T>::PushBack( T newItem )
{
    throw Exception::NotImplementedException( "SmartFixedArray<T>::PushBack" ); // Erase this once you work on this function
}

template <typename T>
void SmartFixedArray<T>::PopBack()
{
    throw Exception::NotImplementedException( "SmartFixedArray<T>::PopBack" ); // Erase this once you work on this function
}

template <typename T>
T& SmartFixedArray<T>::GetBack()
{
    throw Exception::NotImplementedException( "SmartFixedArray<T>::GetBack" ); // Erase this once you work on this function
}

#endif
