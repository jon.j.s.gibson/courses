#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
  cout << "QUEUE PROGRAM" << endl;
  
  // TODO: Declare a queue
  
  bool done = false;
  while ( !done )
  {
    cout << string( 50, '-' ) << endl;
    // TODO:  If queue is empty, display "QUEUE IS EMPTY".
    //        Otherwise display the front of the queue

    cout << string( 50, '-' ) << endl;
    cout << "0. Quit" << endl;
    cout << "1. PUSH item" << endl;
    cout << "2. POP item" << endl;
    cout << ">> ";
    
    int choice;
    cin >> choice;
    
    switch( choice )
    {
      case 0:
      done = true;
      break;
      
      case 1:
      {
        string text;
        cout << "Enter new text to push on queue: ";
        cin >> text;
        // TODO: Push the text item onto the queue
        
      }
      break;
      
      case 2:
        cout << "Popped top item off queue" << endl;
        // TODO: Pop the front of the queue
        
      break;
    }
  }
  
  cout << endl << string( 50, '-' ) << endl;
  
  // TODO: While the queue is not empty...:
    // TODO: Display the item at the front of the queue
    // TODO: Pop the front of the queue
  
  cout << endl << "THE END" << endl;
  return 0;
}
