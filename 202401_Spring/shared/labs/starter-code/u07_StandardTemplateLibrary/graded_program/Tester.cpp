#include "GroceryStoreProgram.h"

#include <iostream>
#include <iomanip>
using namespace std;

//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// - AUTOMATED TESTER -------------------------------------------------------//
#include "INFO.h"
void Tester_GroceryStoreProgram()
{
  cout << "\n** Test_Array_Constructor ** STUDENT: " << STUDENT_NAME << endl;
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";

  {
    cout << CLR;
    string testName = "Stock - Check that product & price are added";
    GroceryStoreProgram gsp;
    string input1 = "pizza";
    float input2 = 9.75;
    int expected_size = 1;
    gsp.Stock( input1, input2 );

    if ( gsp.product_prices.size() != expected_size )
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << " gsp.Stock( \"" << input1 << "\", " << input2 << " ) called." << endl;
      cout << " `product_prices` size expected to be " << expected_size << ", but was " << gsp.product_prices.size() << "!" << endl;
    }
    else if ( gsp.product_prices.find( input1 ) == gsp.product_prices.end() )
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << " gsp.Stock( \"" << input1 << "\", " << input2 << " ) called." << endl;
      cout << " \"" << input1 << "\" not found in `product_prices`!" << endl;
    }
    else if ( gsp.product_prices.at( input1 ) != input2 )
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << " gsp.Stock( \"" << input1 << "\", " << input2 << " ) called." << endl;
      cout << " \"" << input1 << "\" is mapped to " << gsp.product_prices.at( input1 ) << endl;
    }
    else
    {
      cout << GRN << "[PASS] " << testName << endl;
    }
  }

  {
    cout << CLR;
    string testName = "CustomerEnterStore";
    bool allPass = true;
    GroceryStoreProgram gsp;
    string input = "rebekah";
    int expected_size = 1;
    gsp.CustomerEnterStore( input );

    if ( gsp.customers_in_store.size() != expected_size )
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << " gsp.CustomerEnterStore( \"" << input << "\" ) called." << endl;
      cout << " `customers_in_store` size expected to be " << expected_size << ", but was " << gsp.customers_in_store.size() << "!" << endl;
    }
    else if ( gsp.customers_in_store.find( input ) == gsp.customers_in_store.end() )
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << " gsp.CustomerEnterStore( \"" << input << "\" ) called." << endl;
      cout << " \"" << input << "\" not found in `customers_in_store`!" << endl;
    }
    else if ( gsp.customers_in_store.at( input ).name != input )
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << " gsp.CustomerEnterStore( \"" << input << "\" ) called." << endl;
      cout << " The Customer name isn't set to \"" << input << "\"! It's \"" << gsp.customers_in_store.at( input ).name << "\"!" << endl;
    }
    else
    {
      cout << GRN << "[PASS] " << testName << endl;
    }

  }

  {
    cout << CLR;
    string testName = "CustomerCartAdd";
    bool allPass = true;
    GroceryStoreProgram gsp;
    string input1 = "rebekah";
    string input2 = "taco";
    string input3 = "burrito";
    int expected_size = 2;
    gsp.customers_in_store["rebekah"].name = "rebekah";
    gsp.CustomerCartAdd( input1, input2 );
    gsp.CustomerCartAdd( input1, input3 );

    if ( gsp.customers_in_store[ input1 ].products_in_cart.size() != 2 )
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << " gsp.CustomerCartAdd( \"" << input1 << "\", \"" << input2 << "\" ) called." << endl;
      cout << " gsp.CustomerCartAdd( \"" << input1 << "\", \"" << input3 << "\" ) called." << endl;
      cout << " `products_in_cart` size expected to be " << expected_size << ", but was " << gsp.customers_in_store[ input1 ].products_in_cart.size() << "!" << endl;
      allPass = false;
    }
    if ( gsp.customers_in_store[ input1 ].products_in_cart.top() != input3 )
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << " gsp.CustomerCartAdd( \"" << input1 << "\", \"" << input2 << "\" ) called." << endl;
      cout << " gsp.CustomerCartAdd( \"" << input1 << "\", \"" << input3 << "\" ) called." << endl;
      cout << " expected \"" << input3 << "\" to be on top of `products_in_cart`; instead it's \"" << gsp.customers_in_store[ input1 ].products_in_cart.top() << "\"!" << endl;
      allPass = false;
    }

    gsp.customers_in_store[ input1 ].products_in_cart.pop();
    if ( gsp.customers_in_store[ input1 ].products_in_cart.top() != input2 )
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << " gsp.CustomerCartAdd( \"" << input1 << "\", \"" << input2 << "\" ) called." << endl;
      cout << " gsp.CustomerCartAdd( \"" << input1 << "\", \"" << input3 << "\" ) called." << endl;
      cout << " expected \"" << input2 << "\" to be on bottom of `products_in_cart`; instead it's \"" << gsp.customers_in_store[ input1 ].products_in_cart.top() << "\"!" << endl;
      allPass = false;
    }

    if ( allPass )
    {
      cout << GRN << "[PASS] " << testName << endl;
    }
  }

  {
    cout << CLR;
    string testName = "CustomerOops";
    bool allPass = true;
    GroceryStoreProgram gsp;
    string input1 = "rebekah";
    string input2 = "taco";
    string input3 = "burrito";
    int expected_size = 1;
    gsp.customers_in_store["rebekah"].name = "rebekah";
    gsp.customers_in_store["rebekah"].products_in_cart.push( input2 );
    gsp.customers_in_store["rebekah"].products_in_cart.push( input3 );
    gsp.CustomerOops( input1 );

    if ( gsp.customers_in_store[ input1 ].products_in_cart.size() != 1 )
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << " Put 2 items in cart." << endl;
      cout << " gsp.CustomerOops( \"" << input1 << "\" ) called." << endl;
      cout << " `products_in_cart` size expected to be " << expected_size << ", but was " << gsp.customers_in_store[ input1 ].products_in_cart.size() << "!" << endl;
      allPass = false;
    }
    if ( gsp.customers_in_store[ input1 ].products_in_cart.top() != input2 )
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << " Put 2 items in cart." << endl;
      cout << " gsp.CustomerOops( \"" << input1 << "\" ) called." << endl;
      cout << " expected \"" << input2 << "\" to be on top of `products_in_cart`; instead it's \"" << gsp.customers_in_store[ input1 ].products_in_cart.top() << "\"!" << endl;
      allPass = false;
    }

    if ( allPass )
    {
      cout << GRN << "[PASS] " << testName << endl;
    }
  }

  {
    cout << CLR;
    string testName = "CustomerLineup";
    bool allPass = true;
    GroceryStoreProgram gsp;
    string input1 = "rebekah";
    string input2 = "suzanne";
    int expected_size = 2;
    gsp.customers_in_store[ input1 ].name = input1;
    gsp.customers_in_store[ input2 ].name = input2;
    gsp.CustomerLineup( input1 );
    gsp.CustomerLineup( input2 );

    if ( gsp.checkout_queue.size() != expected_size )
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << " Put 2 customers in line queue." << endl;
      cout << " gsp.CustomerLineup( \"" << input1 << "\" ) called." << endl;
      cout << " gsp.CustomerLineup( \"" << input2 << "\" ) called." << endl;
      cout << " `checkout_queue` size expected to be " << expected_size << ", but was " << gsp.customers_in_store.size() << "!" << endl;
      allPass = false;
    }

    if ( gsp.checkout_queue.front().name != input1 )
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << " gsp.CustomerLineup( \"" << input1 << "\" ) called." << endl;
      cout << " gsp.CustomerLineup( \"" << input2 << "\" ) called." << endl;
      cout << " expected \"" << input1 << "\" to be on top of `checkout_queue`; instead it's \"" << gsp.checkout_queue.front().name << "\"!" << endl;
      allPass = false;
    }

    gsp.checkout_queue.pop();
    if ( gsp.checkout_queue.front().name != input2 )
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << " gsp.CustomerLineup( \"" << input1 << "\" ) called." << endl;
      cout << " gsp.CustomerLineup( \"" << input2 << "\" ) called." << endl;
      cout << " expected \"" << input2 << "\" to be on bottom of `checkout_queue`; instead it's \"" << gsp.checkout_queue.front().name << "\"!" << endl;
      allPass = false;
    }

    if ( allPass )
    {
      cout << GRN << "[PASS] " << testName << endl;
    }
  }

  {
    cout << CLR;
    string testName = "Process";
    bool allPass = true;
    GroceryStoreProgram gsp;
    gsp.Stock( "aaa", 111 );
    gsp.CustomerEnterStore( "bbb" );
    gsp.CustomerCartAdd( "bbb", "aaa" );
    gsp.CustomerLineup( "bbb" );
    gsp.Process();

    if ( gsp.checkout_queue.size() != 0 )
    {
      cout << RED << "[FAIL] " << testName << endl;
      cout << " expected `checkout_queue.size()` to be 0, but it's " << gsp.checkout_queue.size() << endl;
      allPass = false;
    }

    if ( allPass )
    {
      cout << GRN << "[PASS] " << testName << endl;
    }
  }

  cout << endl << BOLD
    << "** NOTE: void GroceryStoreProgram::Process() cannot be" << endl
    << "         fully tested with unit tests!" << endl
    << "         There may be existing issues in the program," << endl
    << "         even if the unit tests pass!" << endl;


//        cout << " ACTUAL   front customer's cart: " << ( [=]() -> string { string S; for ( auto& p : gsp.checkout_queue.front().products_in_cart )  { S += p + " "; } return S; }() ) << endl;


      // Anonymous functions in C++ are UGLY!!!
}
