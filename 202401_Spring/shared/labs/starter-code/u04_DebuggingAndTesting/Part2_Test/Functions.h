// You do not need to modify this file.
#ifndef _FUNCTIONS_H
#define _FUNCTIONS_H

#include <string>
using namespace std;

//! Returns the area of a rectangle given the `width` and `length`
int GetArea( int width, int length );
//! Unit tests for the GetArea function
void Test_GetArea();

//! Returns the amount of characters in the `text` string
int GetStringLength( string text );
//! Unit tests for the GetStringLength function
void Test_GetStringLength();

//! Calculates and returns the perimeter of a rectangle given the `width` and the `length`
int GetPerimeter( int width, int length );
//! Unit tests for the GetPerimeter function
void Test_GetPerimeter();

//! Returns the updated balance after `amount` is taken out from the original `balance`
float Withdraw( float balance, float amount );
//! Unit tests for the Withdraw function
void Test_Withdraw();

void Program();

#endif
