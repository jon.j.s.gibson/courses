#ifndef _GAME_HPP
#define _GAME_HPP

#include "Player.h"

#include <vector>
#include <string>
using namespace std;

class Game
{
    public:
    Game();

    void Run();

    private:
    void EnterToContinue();
    void Menu_GameStart();
    void Menu_Main();
    void Menu_Scavenge();
    void Menu_Travel();
    void DisplayMenu();
    void DisplayLocations();
    int GetChoice( int min, int max );
    void RandomEvent_Scavenging( int event );
    void RandomEvent_Moving( int event );
    void EndOfDayStats();

    Player m_player;
    int m_day;
    bool m_done;
    vector<string> m_locations;
};

#endif
