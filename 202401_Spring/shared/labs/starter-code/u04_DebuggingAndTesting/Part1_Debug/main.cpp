#include <cstdlib>
#include <ctime>
using namespace std;

#include "Game.h"
#include "engine/ScreenDrawer.h"
#include "engine/Log.h"

int main()
{
    srand( time( NULL ) );
    Log::Setup();
    ScreenDrawer::Setup( 80, 15 );
    ScreenDrawer::SetBackgroundColors( "yellow", "red", '/' );
    ScreenDrawer::SetBorderColors( "black", "black" );
    ScreenDrawer::SetWindowColors( "white", "black" );
    ScreenDrawer::SetWinTitleColors( "white", "black" );

    Game game;
    game.Run();

    ScreenDrawer::Teardown();
    Log::Teardown();

    return 0;
}
