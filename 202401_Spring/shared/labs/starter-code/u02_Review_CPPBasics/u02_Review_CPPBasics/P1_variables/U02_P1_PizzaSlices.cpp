#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types


int U02_Program1_Function( int guest_count, int pizza_slices )
{
  // The variable `guest_count` has an UNKNOWN NUMBER in it.
  // The variable `pizza_slices` has an UNKNOWN NUMBER in it.

  // TODO: Create an integer variable `slices_per_person`.
  

  // TODO: Calculate how many slices of pizza each guest gets (slices divided by guests).
  // Assign the result to the `slices_per_person` variable.
  

  // TODO: Use `cout` to display "Total guests:", and then the value from the `guest_count` variable.
  

  // TODO: Use `cout` to display "Total slices of pizza:", and then the value from the `pizza_slices` variable.
  

  // TODO: Use `cout` to display a message of how many slices of pizza each guest gets


  // TODO: Use the `return` keyword to return the amount of slices (e.g., `return slices_per_person;`).
  return -1; // TEMP - REMOVE ME!!
}


//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// - PROGRAM STARTER --------------------------------------------------------//
void U02_Program1_Program()
{
  int total_guests, total_slices;

  std::cout << "PIZZA PARTY PLANNER" << std::endl;

  std::cout << "How many guests will you have at your party? ";
  std::cin >> total_guests;
  std::cout << "How many slices of pizza are available? ";
  std::cin >> total_slices;

  int spp = U02_Program1_Function( total_guests, total_slices );
  std::cout << "Each guest can have " << spp << " slices of pizza" << std::endl;
}

// - AUTOMATED TESTER -------------------------------------------------------//
#include "../../INFO.h"
void U02_Program1_Tester()
{
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  std::cout << std::endl << std::string( 80, '-' ) << std::endl;
  std::cout << "2024-01-U02-P1-TEST; STUDENT: " << STUDENT_NAME << std::endl;

  // (Automated test):

  const int TOTAL_TESTS = 2;
  int    in1[TOTAL_TESTS]; // inputs 1
  int    in2[TOTAL_TESTS]; // inputs 2
  int    exo[TOTAL_TESTS]; // expected output
  int    aco[TOTAL_TESTS]; // actual output

  // Setup test 1
  in1[0] = 50;
  in2[0] = 100;
  exo[0] = 2;

  // Setup test 2
  in1[1] = 10;
  in2[1] = 75;
  exo[1] = 7;

  // Run tests
  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    aco[i] = U02_Program1_Function( in1[i], in2[i] );

    if ( aco[i] == exo[i] )
    {
      // PASS
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 << ", StudentCode(" << in1[i] << ", " << in2[i] << ") = " << aco[i] << std::endl;
    }
    else
    {
      // FAIL
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << ", StudentCode(" << in1[i] << ", " << in2[i] << ")" << std::endl;
      std::cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << std::endl;
      std::cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << std::endl;
    }
  }
  std::cout << CLR;
}
