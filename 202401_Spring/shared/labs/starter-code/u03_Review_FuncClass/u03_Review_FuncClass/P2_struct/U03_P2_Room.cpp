// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types

namespace U03
{

struct Room
{
  float width;
  float length;
  float area;
  float perimeter;
};

Room BuildRoom( float width, float length )
{
  Room newRoom;

  // TODO: Implement me

  return newRoom;
}

// - PROGRAM STARTER --------------------------------------------------------//
void U03_Program2_Program()
{
  std::cout << "ROOM BUILDER" << std::endl;
  // TODO: Implement me
}

//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// - AUTOMATED TESTER -------------------------------------------------------//
#include "../../INFO.h"
void U03_Program2_Tester()
{
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  std::cout << std::endl << std::string( 80, '-' ) << std::endl;
  std::cout << "2024-01-U03-P2-TEST; STUDENT: " << STUDENT_NAME << std::endl;

  // (Automated test):

  const int TOTAL_TESTS = 2;
  float    in1[TOTAL_TESTS]; // inputs 1
  float    in2[TOTAL_TESTS]; // inputs 2
  Room     exo[TOTAL_TESTS]; // expected output
  Room     aco[TOTAL_TESTS]; // actual output

  // Setup test 1
  in1[0] = 5;
  in2[0] = 7;
  exo[0].width = 5;
  exo[0].length = 7;
  exo[0].area = 35;
  exo[0].perimeter = 24;

  // Setup test 2
  in1[1] = 3;
  in2[1] = 11;
  exo[1].width = 3;
  exo[1].length = 11;
  exo[1].area = 33;
  exo[1].perimeter = 28;

  // Run tests
  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    aco[i] = BuildRoom( in1[i], in2[i] );

    if (
        aco[i].width     == exo[i].width &&
        aco[i].length    == exo[i].length &&
        aco[i].area      == exo[i].area &&
        aco[i].perimeter == exo[i].perimeter
         )
    {
      // PASS
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 << ", StudentCode(" << in1[i] << ", " << in2[i] << ") = [";

      std::cout << "width=" << aco[i].width << ", ";
      std::cout << "length=" << aco[i].length << ", ";
      std::cout << "area=" << aco[i].area << ", ";
      std::cout << "perimeter=" << aco[i].perimeter;
      std::cout << "]" << std::endl;
    }
    else
    {
      // FAIL
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << ", StudentCode(" << in1[i] << ", " << in2[i] << ")" << std::endl;
      std::cout << "   EXPECTED OUTPUT: [";
      std::cout << "width=" << exo[i].width << ", ";
      std::cout << "length=" << exo[i].length << ", ";
      std::cout << "area=" << exo[i].area << ", ";
      std::cout << "perimeter=" << exo[i].perimeter;
      std::cout << "]" << std::endl;
      std::cout << "   ACTUAL OUTPUT:   [" ;
      std::cout << "width=" << aco[i].width << ", ";
      std::cout << "length=" << aco[i].length << ", ";
      std::cout << "area=" << aco[i].area << ", ";
      std::cout << "perimeter=" << aco[i].perimeter;
      std::cout << "]" << std::endl;
    }
  }
  std::cout << CLR;
}

}

