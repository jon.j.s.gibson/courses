#include <iostream>
#include "u03_Review_FuncClass/U03_Headers.h"

int main()
{
  std::cout << "1. Run AUTOMATED TESTS" << std::endl;
  std::cout << "2. Run PROGRAMS" << std::endl;
  int choice;
  std::cout << ">> ";
  std::cin >> choice;

  switch( choice )
  {
    case 1:
    U03_Tester();
    break;

    case 2:
    U03_Program();
    break;
  }

  return 0;
}
