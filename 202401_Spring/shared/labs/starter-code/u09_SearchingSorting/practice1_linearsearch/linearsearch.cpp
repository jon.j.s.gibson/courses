#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

/**
 * @param   arr       The array of items to search through
 * @param   find_me   The data we're trying to find in the array
 * @return            Returns the index where the item was found, or -1 for not found.
 * */
int LinearSearch( vector<string>& arr, string find_me )
{
  // TODO: Implement me
  return -1;
}

//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
string VectorToString( const vector<string>& arr )
{
  string built_string = "{ ";
  for ( size_t i = 0; i < arr.size(); i++ )
  {
    if ( i != 0 ) { built_string += ", "; }
    built_string += "" + to_string( i ) + "=\"" + arr[i] + "\"";
  }
  built_string += " }";
  return built_string;
}

void Tester();

int main()
{
  cout << "MANUAL TESTS:" << endl;
  vector<string> my_vector = { "ice cream", "cake", "cookies", "pie" };
  
  cout << " Array:\t" << VectorToString( my_vector ) << endl;
  
  cout << " Search for \"cookies\":  " << LinearSearch( my_vector, "cookies" ) << endl;
  cout << " Search for \"rasgulla\": " << LinearSearch( my_vector, "rasgulla" ) << endl;



  cout << endl << endl << "AUTOMATED TESTS:" << endl;
  Tester();


  return 0;
}

void Tester()
{
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  
  const int TOTAL_TESTS = 3;
  vector<string>  in1[TOTAL_TESTS] = { { "dog", "rat", "cat" }, { "Anuj", "Rai", "Asha" }, { "One", "Two", "Three" } };
  string          in2[TOTAL_TESTS] = { "cat", "Rai", "Four" };
  int  exo[TOTAL_TESTS] = { 2, 1, -1 };
  int  aco[TOTAL_TESTS];
  
  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    aco[i] = LinearSearch( in1[i], in2[i] );
    
    if ( aco[i] == exo[i] )
    {
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 
        << ", LinearSearch( " << VectorToString( in1[i] ) << ", \"" << in2[i] << "\" ) = " << aco[i] << endl;
    }
    else
    {
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << std::endl;
      std::cout << "EXPECTED  LinearSearch(" << VectorToString( in1[i] ) << ", \"" << in2[i] << "\") = " << exo[i] << endl;
      std::cout << "ACTUAL    LinearSearch(" << VectorToString( in1[i] ) << ", \"" << in2[i] << "\") = " << aco[i] << endl;
    }
  }
  
  std::cout << CLR;
}
