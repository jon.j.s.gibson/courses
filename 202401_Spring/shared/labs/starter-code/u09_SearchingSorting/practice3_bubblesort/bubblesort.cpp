#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

void BubbleSort( vector<string>& arr )
{
  // TODO: Implement me
}

//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
string VectorToString( const vector<string>& arr )
{
  string built_string = "{ ";
  for ( size_t i = 0; i < arr.size(); i++ )
  {
    if ( i != 0 ) { built_string += ", "; }
    built_string += "" + to_string( i ) + "=\"" + arr[i] + "\"";
  }
  built_string += " }";
  return built_string;
}

void Tester();

int main()
{
  cout << "MANUAL TESTS:" << endl;
  vector<string> my_vector = { "ice cream", "cake", "cookies", "pie" };
  
  cout << " Original:\t" << VectorToString( my_vector ) << endl;
  
  BubbleSort( my_vector );
  
  cout << endl << " After sort:\t" << VectorToString( my_vector ) << endl;



  cout << endl << endl << "AUTOMATED TESTS:" << endl;
  Tester();


  return 0;
}

void Tester()
{
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  
  const int TOTAL_TESTS = 2;
  vector<string>  in1[TOTAL_TESTS] = { { "c", "a", "t" }, { "h", "e", "l", "l", "o" } };
  vector<string>  exo[TOTAL_TESTS] = { { "a", "c", "t" }, { "e", "h", "l", "l", "o" } };
  vector<string>  aco[TOTAL_TESTS];
  
  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    aco[i] = in1[i];
    BubbleSort( aco[i] );
    
    if ( aco[i] == exo[i] )
    {
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 
        << ", BubbleSort( " << VectorToString( in1[i] ) << " ) = \n\t"
        << VectorToString( aco[i] ) << "" << endl;
    }
    else
    {
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << std::endl;
      std::cout << "EXPECTED  BubbleSort(" << VectorToString( in1[i] ) << ") \n\t = " << VectorToString( exo[i] ) << endl;
      std::cout << "ACTUAL    BubbleSort(" << VectorToString( in1[i] ) << ") \n\t = " << VectorToString( aco[i] ) << endl;
    }
  }
  
  std::cout << CLR;
}
