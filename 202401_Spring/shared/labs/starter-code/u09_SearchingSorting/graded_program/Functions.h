#ifndef _FUNCTIONS_H
#define _FUNCTIONS_H

#include <chrono>
#include <vector>
#include <string>
using namespace std;

void SelectionSort( vector<int>& arr );

void MergeSort( vector<int>& arr );
void MergeSort_Recursive( vector<int>& arr, size_t left, size_t right );
void MergeEm( vector<int>& arr, size_t left, size_t mid, size_t right );

void QuickSort( vector<int>& arr );
void QuickSort_Recursive( vector<int>& arr, int low, int high );
size_t PartitionEm( vector<int>& arr, int low, int high );

void RadixSort( vector<int>& arr );
void CountSort( vector<int>& arr, int n, int exp );
int GetMax( vector<int>& arr );

void HeapSort( vector<int>& arr );
void Heapify( vector<int>& arr, size_t size );
void SiftDown( vector<int>& arr, int root, int end );
int LeftChildIndex( int index );
int RightChildIndex( int index );
int ParentIndex( int index );

int  BinarySearch( const vector<int>& arr, int find_me );
int  LinearSearch( const vector<int>& arr, int find_me );

string VectorToString( const vector<int>& arr );
void GenerateData( vector<int>& data );
void TimerStart( chrono::system_clock::time_point& timer_start );
size_t TimerElapsedMS( const chrono::system_clock::time_point& start_time );
void Program();
void Tester();
void SortTests();
void SearchTests();

#endif
