#include <iostream>
using namespace std;

#include "Functions.h"

int main()
{
  std::cout << "1. Run AUTOMATED TESTS" << std::endl;
  std::cout << "2. Run PROGRAMS" << std::endl;
  int choice;
  std::cout << ">> ";
  std::cin >> choice;

  switch( choice )
  {
    case 1:
    Tester();
    break;

    case 2:
    Program();
    break;
  }

  return 0;
}

