# -*- mode: org -*-


#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML

[[file:../../images/comics/pixel-goals.png]] *Goals:*
  - Practice with STL array
  - Practice with STL vector
  - Practice with STL list
  - Practice with STL map
  - Practice with STL queue
  - Practice with STL stack


[[file:../../images/comics/pixel-turnin.png]] *Turn-in:*
- You'll commit your code to your repository, create a *merge request*, and submit the merge request URL in Canvas. (Instructions in document.)

[[file:../../images/comics/pixel-practice.png]] *Practice programs and graded program:*
- This assignment contains several "practice" program folders,
  and a "graded" program folder. Only the "graded" program is required.
- The practice programs are there to help you practice with the topics before
  tackling the larger graded program. If you feel confident in the topic already,
  you may go ahead and work on the graded program. If you feel like you need
  additional practice first, that's what the practice assignments are for.
- The *graded program* assignment contains unit tests to help verify your code.
- You can score up to 100% turning in just the *graded program* assignment.
- You can turn in the *practice* assignments for *extra credit points*.



[[file:../../images/comics/pixel-fight.png]] *Stuck on the assignment? Not sure what to do next?*
- Continue scrolling down in the documentation and see if you're missing anything.
- Try skimming through the entire assignment before getting started,
  to get a high-level overview of what we're going to be doing.
- If you're stuck on a practice program, try working on a different one and come back to it later.
- If you're stuck on the graded program, try the practice programs first.


[[file:../../images/comics/pixel-dual.png]] *Dual enrollment:*
If you are in both my CS 235 and CS 250 this semester, these instructions are the same.
You only need to implement it once and upload it to _one_ repository, then turn in the link for the one merge request.
Please turn in on both Canvas assignments so they're marked done.

#+END_HTML

------------------------------------------------
* *Setup: Starter code and new branch*

For this assignment, I've already added the code to your repository.

1. Pull the starter code:
   1. Check out the =main= branch: =git checkout main=
   2. Pull latest: =git pull=
2. Create a new branch to start working from: =git checkout -b BRANCHNAME=

The =u07_StandardTemplateLibrary= folder contains the following items:

#+begin_src artist
.
├── examples
│   ├── array_example.cpp
│   ├── list_example.cpp
│   ├── map_example.cpp
│   ├── queue_example.cpp
│   ├── stack_example.cpp
│   └── vector_example.cpp
├── graded_program
│   ├── Customer.h
│   ├── events.txt
│   ├── GroceryStoreProgram.cpp
│   ├── GroceryStoreProgram.h
│   ├── INFO.h
│   ├── main.cpp
│   ├── Product.h
│   ├── Project_CodeBlocks
│   │   ├── STL.cbp
│   │   ├── STL.depend
│   │   └── STL.layout
│   ├── Project_Makefile
│   │   └── Makefile
│   ├── Project_VisualStudio2022
│   │   ├── StandardTemplateLibraryProject.sln
│   │   ├── StandardTemplateLibraryProject.vcxproj
│   │   └── StandardTemplateLibraryProject.vcxproj.filters
│   └── Tester.cpp
├── practice1_array
│   └── u07_p1_array.cpp
├── practice2_vector
│   └── u07_p2_vector.cpp
├── practice3_list
│   └── u07_p3_list.cpp
├── practice4_map
│   └── u07_p4_map.cpp
├── practice5_stack
│   └── u07_p5_stack.cpp
└── practice6_queue
    └── u07_p6_queue.cpp
#+end_src

The *examples* folder contains examples of each of the Standard Template Library
items we're using in this assignment.

------------------------------------------------
* *=practice1_array=*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
ARRAY PROGRAM

COURSES:
0: CS 134
1: CS 200
2: CS 235
3: CS 250

THE END
#+END_SRC

Look out for the comment lines in the code that give instructions on what to do.

Utilize the STL =array= class (https://cplusplus.com/reference/array/array/) for this program.
Set the array's size and fill it up with items, then iterate over the array,
displaying each index and element value.

#+ATTR_HTML: :class hidden-hint
#+NAME: content
#+BEGIN_HTML
QUESTION: How do I iterate over an array?

#+ATTR_HTML: :class hidden-hint
=for ( size_t i = 0; i < ARRAYNAME.size(); i++ )= for the loop.

QUESTION: How do I get the index?

#+ATTR_HTML: :class hidden-hint
Within the loop example above, =i= is the index.

QUESTION: How do I get the element?

#+ATTR_HTML: :class hidden-hint
Within the loop example above, =ARRAYNAME[i]= is the element.
#+END_HTML

------------------------------------------------
* *=practice2_vector=*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
VECTOR PROGRAM
Enter a new course, or STOP to finish: CS134
Enter a new course, or STOP to finish: CS200
Enter a new course, or STOP to finish: CS235
Enter a new course, or STOP to finish: STOP

COURSES:
0: CS134
1: CS200
2: CS235
THE END
#+END_SRC

Look out for the comment lines in the code that give instructions on what to do.

Utilize the STL =vector= class (https://cplusplus.com/reference/vector/vector/) for this program.
Use the =push_back= function to add new items to the vector, and use a normal
for loop to display each index and element value.

#+ATTR_HTML: :class hidden-hint
#+NAME: content
#+BEGIN_HTML
QUESTION: How do I iterate over a vector?

#+ATTR_HTML: :class hidden-hint
=for ( size_t i = 0; i < VECTORNAME.size(); i++ )= for the loop.

QUESTION: How do I get the index?

#+ATTR_HTML: :class hidden-hint
Within the loop example above, =i= is the index.

QUESTION: How do I get the element?

#+ATTR_HTML: :class hidden-hint
Within the loop example above, =VECTORNAME[i]= is the element.
#+END_HTML

------------------------------------------------
* *=practice3_list=*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
LIST PROGRAM

Enter a new course, or STOP to finish: ASL120
Insert at (F) FRONT or (B) BACK? F
Enter a new course, or STOP to finish: ASL121
Insert at (F) FRONT or (B) BACK? B
Enter a new course, or STOP to finish: CS134
Insert at (F) FRONT or (B) BACK? F
Enter a new course, or STOP to finish: CS250
Insert at (F) FRONT or (B) BACK? B
Enter a new course, or STOP to finish: STOP

COURSES:
F
F
B
B

THE END
#+END_SRC

Look out for the comment lines in the code that give instructions on what to do.

Utilize the STL =list= class (https://cplusplus.com/reference/list/list/) for this program.
As you add items to the list, the user can choose to put the new item at the FRONT or the BACK
of the list. Afterwards, use a range-based for loop to iterate over all the items in the list
to display each one.

#+ATTR_HTML: :class hidden-hint
#+NAME: content
#+BEGIN_HTML
QUESTION: How do I iterate over a list to display each value?

#+ATTR_HTML: :class hidden-hint
=for ( auto& item : LISTNAME )= for the loop.
#+END_HTML

------------------------------------------------
* *=practice4_map=*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
MAP PROGRAM

Item: burrito, Price: $1.29
Item: quesadilla, Price: $2.36
Item: taco, Price: $1.29

Enter a food: burrito
The price is: $1.29

THE END
#+END_SRC

Look out for the comment lines in the code that give instructions on what to do.

Utilize the STL =map= class (https://cplusplus.com/reference/map/map/) for this program.
You'll add items to the map, access items via their keys, and utilize a range-based for loop to
iterate over all the items in the map.

#+ATTR_HTML: :class hidden-hint
#+NAME: content
#+BEGIN_HTML
QUESTION: How do I iterate over a map and display all keys/values?

#+ATTR_HTML: :class hidden-hint
=for ( auto& item : product_prices )= for the loop,
then you can use =item.first= to get the key, and =item.second= to get the value.
#+END_HTML

---------------------------------
* *=practice5_stack=*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
STACK PROGRAM
--------------------------------------------------
STACK IS EMPTY
--------------------------------------------------
0. Quit
1. PUSH item
2. POP item
>> 1
Enter new text to push on stack: A
--------------------------------------------------
TOP ITEM IN STACK: A
--------------------------------------------------
0. Quit
1. PUSH item
2. POP item
>> 1
Enter new text to push on stack: B
--------------------------------------------------
TOP ITEM IN STACK: B
--------------------------------------------------
0. Quit
1. PUSH item
2. POP item
>> 1
Enter new text to push on stack: C
--------------------------------------------------
TOP ITEM IN STACK: C
--------------------------------------------------
0. Quit
1. PUSH item
2. POP item
>> 2
Popped top item off stack
--------------------------------------------------
TOP ITEM IN STACK: B
--------------------------------------------------
0. Quit
1. PUSH item
2. POP item
>> 0

--------------------------------------------------
TOP OF STACK: B
TOP OF STACK: A

THE END
#+END_SRC

Look out for the comment lines in the code that give instructions on what to do.

Utilize the STL =stack= class (https://cplusplus.com/reference/stack/stack/) for this program.
You'll create a stack, use =push=, =pop=, and =top= functions to interface with it.


------------------------------------------------
* *=practice6_queue=*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
QUEUE PROGRAM
--------------------------------------------------
QUEUE IS EMPTY
--------------------------------------------------
0. Quit
1. PUSH item
2. POP item
>> 1
Enter new text to push on queue: A
--------------------------------------------------
FRONT ITEM IN QUEUE: A
--------------------------------------------------
0. Quit
1. PUSH item
2. POP item
>> 1
Enter new text to push on queue: B
--------------------------------------------------
FRONT ITEM IN QUEUE: A
--------------------------------------------------
0. Quit
1. PUSH item
2. POP item
>> 1
Enter new text to push on queue: C
--------------------------------------------------
FRONT ITEM IN QUEUE: A
--------------------------------------------------
0. Quit
1. PUSH item
2. POP item
>> 2
Popped top item off queue
--------------------------------------------------
FRONT ITEM IN QUEUE: B
--------------------------------------------------
0. Quit
1. PUSH item
2. POP item
>> 0

--------------------------------------------------
FRONT OF QUEUE: B
FRONT OF QUEUE: C

THE END
#+END_SRC

Look out for the comment lines in the code that give instructions on what to do.

Utilize the STL =queue= class (https://cplusplus.com/reference/queue/queue/) for this program.
You'll create a queue, use =push=, =pop=, and =front= functions to interface with it.


------------------------------------------------
* *=graded_program=*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
(8:00) Added apple @ $0.76 to shelves.
(8:05) Added avocado @ $2.14 to shelves.
(8:10) Added banana @ $0.24 to shelves.
(8:15) Added watermelon @ $12.59 to shelves.
(8:20) Customer Maribel entered store.
(8:25) Customer Maribel adds apple to their cart.
(8:30) Customer Maribel adds apple to their cart.
(8:35) Customer Maribel decides to put apple back.
(8:40) Customer Maribel adds watermelon to their cart.
(8:45) Customer Sara entered store.
(8:50) Customer Sara adds apple to their cart.
(8:55) Customer Sara queues up in the checkout line.
(9:00) Customer Cody entered store.
(9:05) Customer Cody adds watermelon to their cart.
(9:10) Customer Cody adds watermelon to their cart.
(9:15) Customer Cody adds watermelon to their cart.
(9:20) Customer Cody decides to put watermelon back.
(9:25) Customer Cody queues up in the checkout line.
(9:30) Customer Rai entered store.
(9:35) Customer Rai adds avocado to their cart.
(9:40) Customer Rai adds apple to their cart.
(9:45) Customer Rai adds watermelon to their cart.
(9:50) Customer Maribel queues up in the checkout line.
(9:55) Customer Rai queues up in the checkout line.
(10:00) Customer Sara is at the register.
(10:05)   >> apple ($0.76) is their next item.
(10:10)   >> Checkout done. Total: $0.76... Customer Sara leaves.
(10:15) Customer Cody is at the register.
(10:20)   >> watermelon ($12.59) is their next item.
(10:25)   >> watermelon ($12.59) is their next item.
(10:30)   >> Checkout done. Total: $25.18... Customer Cody leaves.
(10:35) Customer Maribel is at the register.
(10:40)   >> watermelon ($12.59) is their next item.
(10:45)   >> apple ($0.76) is their next item.
(10:50)   >> Checkout done. Total: $13.35... Customer Maribel leaves.
(10:55) Customer Rai is at the register.
(11:00)   >> watermelon ($12.59) is their next item.
(11:05)   >> apple ($0.76) is their next item.
(11:10)   >> avocado ($2.14) is their next item.
(11:15)   >> Checkout done. Total: $15.49... Customer Rai leaves.
(11:20) Done processing line!
#+END_SRC

This program simulates events in a grocery store. The =events.txt= contains a series of
commands, though you don't have to worry about parsing the file. You only need to implement
functionality in the *GroceryStoreProgram.cpp* file.

You will need to keep in mind these structures as you work on your class:

*Product.h:*

#+BEGIN_SRC cpp :class cpp
struct Product
{
    string name;
    float price;
};
#+END_SRC

*Customer.h:* Each Customer has a =name= and a stack of =products_in_cart=.
Sometimes, customers will put something in their cart and then change their mind.
When this happens, the item at the top of the stack will be popped to be removed.

#+BEGIN_SRC cpp :class cpp
struct Customer
{
    string name;
    stack<string> products_in_cart;
};
#+END_SRC

*GroceryStoreProgram.h:*

#+BEGIN_SRC cpp :class cpp
class GroceryStoreProgram
{
public:
    GroceryStoreProgram();

    void Stock( string product, float price );
    void CustomerEnterStore( string customer );
    void CustomerCartAdd( string customer, string product );
    void CustomerOops( string customer );
    void CustomerLineup( string customer );
    void Process();
    void PrintTimestamp();

private:
    queue<Customer> checkout_queue;
    map<string, float> product_prices;
    map<string, Customer> customers_in_store;
    int minutes, hours;

    friend void Tester_GroceryStoreProgram();
};
#+END_SRC

The grocery store program has several important member variables:

- =queue<Customer> checkout_queue= is the queue of customers in line waiting to check out.
  The customer at the front of the line gets helped first.
- =map<string, float> product_prices= is a map of product names -> product prices.
  This can be used to look up the price of a product.
- =map<string, Customer> customers_in_store= is a map of all customers in the store.
  The customer's name is the key, and then a Customer object is the value.


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *void GroceryStoreProgram::Stock( string product, float price )*

Use the =product= as the key and the =price= as the value, adding the data to the =product_prices= map.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *void GroceryStoreProgram::CustomerEnterStore( string customer )*

Use the =customer= as the key for a new item in the =customers_in_store= map.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *void GroceryStoreProgram::CustomerCartAdd( string customer, string product )*

Access the customer at the =customer= key. The Customer object contains
a =products_in_cart= stack, push the new =product= onto that stack.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *void GroceryStoreProgram::CustomerOops( string customer )*

Access the customer at the =customer= key. The Customer object contains
a =products_in_cart= stack, use the pop function to remove the last-added item from their cart.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *void GroceryStoreProgram::CustomerLineup( string customer )*

Access the customer at the =customer= key in the =customers_in_store= map.
Push this customer into the =checkout_queue=.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *void GroceryStoreProgram::Process()*
This function will process everybody who is currently in the =checkout_queue=.

1. While the checkout queue is not empty:
   1. Display the name of the customer at the front of the checkout queue.
   2. Create a float variable to store the total cost of the transaction, initialize to 0.
   3. For the front customer, while their =products_in_cart= stack is not empty:
      1. Get the price of the next item - use the =product_prices= and the key of the product name.
      2. Add the product price to your total variable.
      3. Display the front item in the customer's cart and the price.
      4. Pop the top item from the =products_in_cart=.
   4. After the while loop, display that checkout is done and the total amount of money.
   5. Pop the customer from the checkout queue.

Hints:
- You can get the next customer in line with: =checkout_queue.front()=.
- You can access Customer member variables further: =checkout_queue.front().name=
- You can access the next customer's next item in cart with: =checkout_queue.front().products_in_cart.top()=
- You can get the price of an item with =product_prices[ KEY ]=.
    The key will be the product name, which corresponds to =checkout_queue.front().products_in_cart.top()=.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

Once everything is implemented, all the tests should pass.
You can also run the program itself to see the "simulation" run.


------------------------------------------------
* *Turning in the assignment*


*Screenshot:* Before finishing up, run the automated tests and take a screenshot
of all of your tests passing. Save the file somewhere as a .png.

*Back up your code:* Open Git Bash in the base directory of your repository folder.

1. Use =git status= to view all the changed items (in red).
2. Use =git add .= to add all current changed items.
3. Use =git commit -m "INFO"= to commit your changes, change "INFO" to a more descriptive message.
4. Use =git push -u origin BRANCHNAME= to push your changes to the server.
5. On GitLab.com, go to your repository webpage. There should be a button to
   *Create a new Merge Request*. You can leave the default settings and create the merge request.
6. Copy the URL of your merge request.

*Turn in on Canvas:*
1. Find the assignment on Canvas and click *"Start assignment"*.
2. Select *Upload file* and upload your screenshot.
3. *Paste your GitLab URL* into the Comments box.
4. Click *"Submit assignment"* to finish.
