# -*- mode: org -*-

For each of the problems below, answer the following questions:

- How does the *expected result* differ from the *actual result*?
- Which line(s) of code are causing the issue?
- How can the issue be fixed?
- Write out the fixed code.

* Problem 1

*Original code:*

#+BEGIN_SRC cpp :class cpp
#+END_SRC

*Expected result:*


*Actual result:*


* Problem 2

*Original code:*

#+BEGIN_SRC cpp :class cpp
#+END_SRC

*Expected result:*


*Actual result:*

