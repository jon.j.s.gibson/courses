# -*- mode: org -*-

*=graded_program=:*

*Starter code:*

#+BEGIN_SRC cpp :class cpp
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - STUDENT CODE -----------------------------------------------------------//
const string YOURNAME = "Your Name, Spring 2024"; // TODO: Update this to your name!

int StudentCode( int guest_count, int pizza_slices )
{
  // The variable `guest_count` has an UNKNOWN NUMBER in it.
  // The variable `pizza_slices` has an UNKNOWN NUMBER in it.

  // TODO: Create an integer variable `slices_per_person`.


  // TODO: Calculate how many slices of pizza each guest gets (slices divided by guests).
  // Assign the result to the `slices_per_person` variable.


  // TODO: Use `cout` to display "Total guests:", and then the value from the `guest_count` variable.


  // TODO: Use `cout` to display "Total slices of pizza:", and then the value from the `pizza_slices` variable.


  // TODO: Use `cout` to display a message of how many slices of pizza each guest gets


  // TODO: Use the `return` keyword to return the amount of slices (e.g., `return slices_per_person;`).
  return -1; // erase this line of code!!
}
#+END_SRC

There's a lot of extra code in this program, but you don't have to worry about anything below the

#+BEGIN_SRC cpp :class cpp
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#+END_SRC

Basically, I've added some automated testing to test out the program, but you can also test it out manually.

Within the program code, labeled =StudentCode=, we have two variables already made:
=int guest_count= and =int pizza_slices=. (These are within the parentheses above.
We'll learn more about functions later.)

Beneath each =TODO= item, you'll add additional code. You'll need to do the following:

1. *Declare* an integer varaible named =slices_per_person=.
2. *Calculate* how many slices of pizza each guest gets, assign the result to the =slices_per_person= variable.
   (the math here is =pizza_slices / guest_count=.)
3. *Display* "Total guests:" and then the =guest_count= variable.
4. *Display* "Total slices of pizza:" and then the =pizza_slices= variable.
5. *Display* "Each guest gets", then the =slices_per_person=, then "slices of pizza".
6. *Return* the =slices_per_person= variable.

In our previous programs, main() had =return 0;= at the end. For this special function, instead we're
going to return the result of our math operation. It will look like this: =return slices_per_person;=.
This should go before the closing curly brace =}=.

Now you can build the program. I've added =-o PizzaParty.exe= to the end here so that the generated program
is named PizzaParty.exe.

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
$ g++ program.cpp -o PizzaParty.exe
#+END_SRC

You can run the program, but it will complain about "NOT ENOUGH ARGUMENTS!"

#+BEGIN_SRC terminal :class terminal
$ ./PizzaParty.exe
argCount is 1; NOT ENOUGH ARGUMENTS!
Run tests:      ./PizzaParty.exe test
Run as program: ./PizzaParty.exe guest_count pizza_slices
#+END_SRC

With some of the special code I added, we can add additional information when running this program.
Basically, we either have to run it and provide a =guest_count= and =pizza_slices=:

#+BEGIN_SRC terminal :class terminal
$ ./PizzaParty.exe  20 100
Total guests: 20
Total slices of pizza: 100
Each guest gets 5 slices of pizza
RESULT: 5
#+END_SRC

Or we run it with the =test= option and it will run automated tests to check your work:

#+BEGIN_SRC terminal :class terminal
$ ./PizzaParty.exe test
2024-01-U03-P1-TEST; STUDENT: Your Name, Spring 2024

Total guests: 50
Total slices of pizza: 100
Each guest gets 2 slices of pizza
[PASS]  TEST 1, StudentCode(50, 100) = 2

Total guests: 10
Total slices of pizza: 75
Each guest gets 7 slices of pizza
[PASS]  TEST 2, StudentCode(10, 75) = 7
#+END_SRC

The automated tests run the PizzaParty program a couple of times, first with 50 guests and 100 slices pizza,
then with 10 guests and 75 slices of pizza. It checks the results that get =return=-ed from the StudentCode function,
and if the math is right, it will show those tests as =[PASS]=. If the math isn't correct, it will show =[FAIL]= instead.
