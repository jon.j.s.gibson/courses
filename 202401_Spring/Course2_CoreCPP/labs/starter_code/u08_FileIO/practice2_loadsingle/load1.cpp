// PROGRAM: Practice loading text files, one item at a time

// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <fstream>    // Library that contains `ifstream` and `ofstream`
using namespace std;  // Using the C++ STanDard libraries

/*
 *Example program output:*
 ------------------------------------------
 LINE #0, READ IN: We're
 LINE #1, READ IN: no
 LINE #2, READ IN: strangers
 ------------------------------------------


 *Example input file:*
 ------------------------------------------
 We're no strangers to love
 You know the rules and so do I (do I)
 A full commitment's what I'm thinking of
 You wouldn't get this from any other guy

 I just wanna tell you how I'm feeling
 Gotta make you understand
 ------------------------------------------

 *Program requirements:*
 This program will use an input file stream (`ifstream`) and the input stream operator (`>>`)
 to read in the first 3 words from the input file "lyricsA.txt".

 1. Create an input file stream (=ifstream=) variable named =input=.
 2. Use the ifstream's `open` function (https://cplusplus.com/reference/fstream/ifstream/open/) to open the file "lyricsA.txt".
 3. Use the `>>` operator to read 1 item from the =input= file to the =read= string variable.
 4. Use `cout` to display the contents of the `counter` and `word` variables.
 5. Do steps 3 and 4 two more times (same exact code).

 Once you run the program it will show the first 3 words of the text file read in
 and displayed to the screen.
*/
int main()
{
  string word;
  int counter = 0;


  // TODO: Add your code here.


  return 0;
}
