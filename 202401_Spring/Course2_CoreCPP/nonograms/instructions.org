
#+ATTR_HTML: :class puzzle-instructions
#+NAME: content
#+BEGIN_HTML
*How to play:* Each cell in the grid of squares can either have a *X* (or blank) or be *filled in*. The filled in boxes create the pixels of an image.
Each row and each column have one or more numbers. This specifies how many pixels are in that row/column.
The number given is how many unbroken pixels will show up together.
Two numbers separated by a space, e.g., "1 1", will have /at least one empty spot/ between them.
#+END_HTML
