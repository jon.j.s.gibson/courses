# -*- mode: org -*-

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML

[[file:../../images/comics/pixel-goals.png]] *Goals:*
  - See how to link a third party library.



[[file:../../images/comics/pixel-turnin.png]] *Turn-in:*
  - 1. You'll commit your code to your repository, create a *merge request*,
    and submit the merge request URL in Canvas. (Instructions in document.)


[[file:../../images/comics/pixel-fight.png]] *This assignment is optional!*
  - I can't always help out with every person's different computer configurations,
    so while I can often help with setup in Code::Blocks and Visual Studio,
    sometimes I can't get it working on different machines.
    Because of this, this assignment is optional. Try what you can and turn in
    what you get, you'll get credit either way.

#+END_HTML

------------------------------------------------
* *Setup: Starter code and new branch*

Start this assignment by checking out =main=, pulling latest to get the files, and creating a new branch...

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
git checkout main
git pull
git checkout -b u19
#+END_SRC

*You'll be creating a new project in your IDE of choice and following the setup guide to create a project.*

-----

* *Setting up the library*

*Windows users:*

First, go to the SFML downloads page here: https://www.sfml-dev.org/download.php.
Then click on the "SFML", whatever the latest version is.
You will need to download a version based on whether you're using Visual Studio or Code::Blocks.

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Visual Studio:

Download the latest version even if the year doesn't match. At time of writing,
"Visual C++ 15 (2017) - xx-bit" is the version you should download.
You can download the 32-bit or 64-bit version.

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

Code::Blocks:

1. Navigate to where you have Codeblocks installed on your computer, such as "C:\Program Files\CodeBlocks\".
2. Then, go to the "MinGW" folder, then the "bin" folder.
3. Look for a "libgcc" dll file. We will know which SFML version to download based on this file.

[[file:images/lab_u19_ThirdPartyLibraries_libgcc.png]]

| libgcc file           | Which SFML version to download               |
|-----------------------+----------------------------------------------|
| =libgcc_s_seh-1.dll=  | GCC 7.3.0 MinGW (SEH) - 64-bit               |
| =libgcc_s_sjlj-1.dll= | GCC 5.1.0 TDM (SJLJ) - Code::Blocks - 32-bit |
| =libgcc_s_dw2-1.dll=  | GCC 7.3.0 MinGW (DW2) - 32-bit               |

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML

Once the library is downloaded, extract to an easy directory to find on your computer.
For example, I usually have a "C:\Libraries\" folder on my computer, and so
I'd extract SFML here - "C:\Libraries\SFML-2.5.1\".



*Linux users:*

Go to your package manager and search for "libsfml". Install the *libsfml-dev* package.

The files will be installed to "=/usr/include/SFML/=" and "=/usr/lib/x86_64-linux-gnu/=" directories
(at least that's where they're at on my computer.)



*Mac users:*

You will have to try to follow the Xcode guide on the SFML page (https://www.sfml-dev.org/tutorials/2.6/);
I do not have a Mac to try to be able to setup SFML and write documentation with.


*All users:*

Within the SFML folder there are two important directories we care about:
The *lib* folder and the *include* folder. You might keep these folders open
or keep a notepad document open and paste your paths into that file for reference
while we're setting up our project.

-----

* *Part 1: Getting a test program running*

Create a new project in Visual Studio or Code::Blocks. Right now we just need one file - *main.cpp*.
The following code is the test program from the SFML setup documentation, you can copy-paste it into the main.cpp file:

#+BEGIN_SRC cpp :class cpp
  #include <SFML/Graphics.hpp>

  int main()
  {
    sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!");
    sf::CircleShape shape(100.f);
    shape.setFillColor(sf::Color::Green);

    while (window.isOpen())
      {
        sf::Event event;
        while (window.pollEvent(evet))
          {
            if (event.type == sf::Event::Closed)
              window.close();
          }

        window.clear();
        window.draw(shape);
        window.display();
      }

    return 0;
  }
#+END_SRC

It won't build at the moment, we still need to configure the project to use SFML.

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
*For instructions on how to configure SFML with your IDE, look at the reference page:*
"Setting up and using third party libraries"

*You can also follow along with archived videos:*
https://www.youtube.com/playlist?list=PLCHjtgI2IPe5j5rFDe4liWJiab63r44qk
#+END_HTML




-----

* *Part 2: Implementing a basic game*

Make sure to download the *GameAssets* folder:
https://gitlab.com/moosadee/courses/-/tree/main/202401_Spring/Course3_ObjectOrientedProgramming/labs/starter_code/u19_ThirdPartyLibraries/GameAssets?ref_type=heads


Download the assets in that directory, bunny.png, diamond.png, grass.png, and PressStart2P.ttf. Copy
these files into your SFML project's directory (where your .vcxproj is for Visual Studio, or your .cbp is for Code::Blocks).

We're going to keep everything in main.cpp for now so we can just focus on some basic SFML.




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *Additional #includes*

At the top of the program, make sure you're including the following:

- cmath
- map
- string





#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *Handy functions*

Add the following function to your program. You can copy/paste this above the =int main()= part in
main.cpp. As long as they show up above main() you'll be able to use these functions /within/ main().

#+BEGIN_SRC cpp :class cpp
  sf::Vector2f GetRandomPosition( int screenWidth, int screenHeight )
  {
    sf::Vector2f pos;
    pos.x = rand() % screenWidth - 64;
    pos.y = rand() % screenHeight - 64;
    return pos;
  }
  float GetDistance( sf::Vector2f obj1, sf::Vector2f obj2 )
  {
    return sqrt( pow( obj1.x - obj2.x, 2 ) + pow( obj1.y - obj2.y, 2 ) );
  }
#+END_SRC




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *Setting up the game*

*1. Create named constants for the window resolution:*
First, we’re going to define the screen width and height as named constants so we
can refer to them in the program without having the numbers hard-coded all over the
place (this is handy if you want to change the resolution later.)

#+BEGIN_SRC cpp :class cpp
  const int SCREEN_WIDTH = 800;
  const int SCREEN_HEIGHT = 600;
#+END_SRC

*2. Set up the game window:* Then we need to create a RenderWindow and set it up. We will set the resolution via
the VideoMode class and give the title bar the text “Example game”, as well as set our framerate.

#+BEGIN_SRC cpp :class cpp
  sf::RenderWindow window( sf::VideoMode( SCREEN_WIDTH, SCREEN_HEIGHT ), "Example game" );
  window.setFramerateLimit( 60 );
#+END_SRC

*3. Load in images:* We will create a map of textures so we can grab them by a string key, instead of having to deal with
index numbers. We’re going to load each of the images.

#+BEGIN_SRC cpp :class cpp
  map<string,sf::Texture> textures;
  textures["bunny"].loadFromFile("bunny.png");
  textures["diamond"].loadFromFile("diamond.png");
  textures["grass"].loadFromFile("grass.png");
#+END_SRC

*4. Load in a font:* We’ll also create a Font variable and load in the font.

#+BEGIN_SRC cpp :class cpp
  sf::Font font;
  font.loadFromFile("PressStart2P.ttf");
#+END_SRC

*5. Create a player sprite:* Let’s create one Sprite for the player, set its texture and position,
and create a couple other helper variables. (In a larger program, these should be in a class.)

#+BEGIN_SRC cpp :class cpp
  sf::Sprite player;
  player.setTexture( textures["bunny"] );
  player.setPosition( SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 );
  float playerSpeed = 5;
  int playerScore = 0;
#+END_SRC

*6. Create a diamond sprite:* Let’s also create a Sprite for the diamond, the collectable item.

#+BEGIN_SRC cpp :class cpp
  sf::Sprite item;
  item.setTexture( textures["diamond"] );
  item.setPosition( GetRandomPosition( SCREEN_WIDTH, SCREEN_HEIGHT ) );
#+END_SRC

*7. Add score text:* And a Text object to be able to render the player’s score to the screen.

#+BEGIN_SRC cpp :class cpp
  sf::Text scoreText;
  scoreText.setFont( font );
  scoreText.setCharacterSize( 30 );
  scoreText.setFillColor( sf::Color::White );
  scoreText.setString( "Score: " + to_string( playerScore ) );
#+END_SRC

*8. Add background tiles:* Add a vector of sf::Sprites named groundTiles to make the background:

#+BEGIN_SRC cpp :class cpp
  vector<sf::Sprite> groundTiles;
  sf::Sprite ground;
  ground.setTexture( textures["grass"] );
  for ( int y = 0; y < SCREEN_HEIGHT; y += 32 )
    {
      for ( int x = 0; x < SCREEN_WIDTH; x += 32 )
        {
          ground.setPosition( x, y );
          groundTiles.push_back( ground );
        }
    }
#+END_SRC


These are all the setup parts, and we will add in the grass background later. Next, we need to create the
game loop – the game will continue running until it gets a “close window” event.





#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *Creating the game loop*

Create the game loop. This will go after all the setup code.

#+BEGIN_SRC cpp :class cpp
    while ( window.isOpen() )
      {
      }
#+END_SRC

And we'll add the following items within...

*9. Listen for window close event:* This code checks for any events happening, and if the event code is a
“Closed” event, then close the window. This will cause the game to exit.

#+BEGIN_SRC cpp :class cpp
  sf::Event event;
  while (window.pollEvent(event))
    {
      if (event.type == sf::Event::Closed)
        window.close();
    }
#+END_SRC

*10. Listen for keyboard input:* Next we’re going to listen for any keyboard presses. If an
arrow key is pressed we are going to move the bunny by changing its x, y coordinates.

#+BEGIN_SRC cpp :class cpp
  sf::Vector2f playerPos = player.getPosition();
  if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Left ) )
    {
      playerPos.x -= playerSpeed;
    }
   else if
     ( sf::Keyboard::isKeyPressed( sf::Keyboard::Right ) )
     {
       playerPos.x += playerSpeed;
     }
  if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Up ) )
    {
      playerPos.y -= playerSpeed;
    }
   else if
     ( sf::Keyboard::isKeyPressed( sf::Keyboard::Down ) )
     {
       playerPos.y += playerSpeed;
     }
  player.setPosition( playerPos );
#+END_SRC

*11. Check for collision between player and diamond:* Now that we’ve handled events and inputs, the only game logic we need at the moment is to check
if the Diamond and the Player are close to each other. We can use the GetDistance function here. You
can adjust the value of 32, it is the amount of pixels of “distance”, and if the two objects are within the
threshold, we will count it as a collision

#+BEGIN_SRC cpp :class cpp
  if ( GetDistance( player.getPosition(), item.getPosition() ) < 32 )
    {
      // Count this as a collect
      item.setPosition( GetRandomPosition( SCREEN_WIDTH, SCREEN_HEIGHT ) );
      playerScore++;
      scoreText.setString( "Score: " + to_string( playerScore ) );
    }
#+END_SRC

[[file:images/lab_u19_ThirdPartyLibraries_collisionregion.png]]

*12. Draw items to the screen:* Finally within the game loop we are going to clear the screen and draw all our items.

#+BEGIN_SRC cpp :class cpp
  window.clear();
  // Draw all the backgrounds
  for ( auto& tile : groundTiles )
    {
      window.draw( tile );
    }
  // Draw item
  window.draw( item );
  // Draw player
  window.draw( player );
  // Draw text
  window.draw( scoreText );
  window.display();
#+END_SRC

*13. Testing the program:*

[[file:images/lab_u19_ThirdPartyLibraries_example1.png]]

When we run the program, the core bunny game should work.
You can move the bunny around with the arrow keys and each time you collect a diamond
the score should go up.

There are lots of ways you could customize or add onto this game, and feel free to experiment,
but this is just an example of what you could do with additional libraries linked up in your IDE.



-----




