# -*- mode: org -*-

If you're using a school lab computer they will already have the software we need set up. Use these instructions for setting up the programs on your own computer.

You can use any *code editor* that you'd like to use. In my example screenshots, I will be using *Geany* (https://www.geany.org/)
but you may have other preferences. VSCode (https://code.visualstudio.com/) is another popular option though it is a Microsoft product.

#+BEGIN_LATEX
\vspace{0.5cm}
#+END_LATEX

Once you've installed a text editor, open the =test-program.cpp= file in the text editor. You will see some basic C++ code.
Change the =world= text to say something else, like your name.

After any changes are made to *source code* you must *recompile* the program for the executable file to show the updates.
So, if you run =g++ test-program.cpp= again, next time you run the program with =./a.exe= you'll get updated text:

#+ATTR_LATEX: :width \textwidth
[[file:reference/images/terminal_build_cpp2.png]]



