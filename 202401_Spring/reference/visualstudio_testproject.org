# -*- mode: org -*-

1. From the starting page, select "*Create a new project*".

   [[file:reference/images/vs_newproject_01_splash.png]]

2. Select "*Empty project*" and click "Next".

   [[file:reference/images/vs_newproject_02_emptyproject.png]]

3. Set your *Project name*. Make sure to set the project *Location* to somewhere within your *repository directory*.

   [[file:reference/images/vs_newproject_03_namelocation.png]]

4. Right-click on your empty project and select *Add* then *New item...*

   [[file:reference/images/vs_newproject_04_addfile.png]]

5. Type in "*main.cpp*" then click "Add".

   [[file:reference/images/vs_newproject_04_addfile2.png]]

6. Add a simple starter C++ program into main.cpp.

   [[file:reference/images/vs_newproject_05_program.png]]

7. Go to the "*Build*" dropdown menu and select "*Build Solution*".

   [[file:reference/images/vs_newproject_06_build.png]]

8. It should show a success in the *Output* window.

   [[file:reference/images/vs_newproject_07_buildsuccess.png]]

9. Then, click the "*(play) Local Windows Debugger*" button to run the program.

   [[file:reference/images/vs_newproject_08_run.png]]

10. Your program text will show up in a console.

    [[file:reference/images/vs_newproject_08_run2.png]]

11. Your files will be in the location you set up. The .cpp file is your source code,
    and the .vcxproj is the project file.

    [[file:reference/images/vs_newproject_09_files.png]]
