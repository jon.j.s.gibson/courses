# -*- mode: org -*-

*Open Git Bash in Windows:*

[[file:images/git-harddrive-opengitbash.png]]

Use the *Windows Explorer* to navigate to a folder where you would like
to open Git Bash in. Right-click in the empty space, and select
"*Git Bash Here*":

In this screenshot, my "rsingh-cs235" repo has already been cloned.

-----------------

*Clone a repository to your hard drive:*

[[file:images/git_clone.png]]

Use the =clone= command:

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
git clone URL
#+END_SRC


-----------------

*Create a new branch:*

[[file:images/git_newbranch.png]]

Make sure to pull latest changes from =main= first before creating a new branch
so that you have the most up-to-date code to start from:

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
git checkout main
git pull
#+END_SRC

Then create a new branch with the =checkout= command:

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
git checkout -b BRANCHNAME
#+END_SRC


-----------------

*View what files have changed with the =status= command:*

[[file:images/git_status.png]]

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
git status
#+END_SRC

-----------------

*Add, commit, and push your changes:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
git add .
git commit -m "CHANGE DESCRIPTION"
git push -u origin BRANCHNAME
#+END_SRC

Add:

[[file:images/git_add.png]]

Commit:

[[file:images/git_commit.png]]

Push:

[[file:images/git_push.png]]

Afterwards, your changes will show up on the server.
Make sure to use the dropdown to select the branch you're on:

[[file:images/git_push2.png]]


-----------------

*Pull latest changes from server:*
You'll probably want to pull latest from the =main= branch.
Note that if you're on a different branch, itw ill pull any changes
to your branch and attempt a merge.

=git pull origin main=


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

*Common errors/issues:*

[[file:images/git-specifybranch.png]]

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
There is no tracking information for the current branch. Please specify which branch you want to merge with.
#+END_SRC

Try to run: =git pull origin main= to point to the proper remote and branch.


--------

[[file:images/git-pushfail.png]]


#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
! [rejected]
error: failed to push some refs
hint: Updates were rejected because the tip fo your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
#+END_SRC

The hint gives you the solution: Run =git pull= and then repeat your =git push=.


--------

[[file:images/git-notarepo.png]]

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
fatal: not a git repository (or any of the parent directories): .git 
#+END_SRC

You are not running your git commands from within a repository folder. Make sure you open Git Bash within a folder, or use cd commands to navigate to your repository path. 

--------

[[file:images/git-defaultbrancherror.png]]

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
A default branch (e.g. main) does not yet exist for REPOSITORY
Ask a project Owner or Maintainer to create a default branch:
URL
![ remote rejected] BRANCH -> BRANCH (pre-receive hook declined)
#+END_SRC

Make sure you're on your own *branch* and not the =main= branch
when trying to do your =push= command.

Alternatively, the instructor may need to make a commit to the =main= branch
to get things up and ready to go.

--------

[[file:images/git_crlf.png]]

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
warning: in the working copy of 'FILE', LF will be replaced by CRLF the next time Git touches it.
#+END_SRC

This isn't a problem, this is just because the "enter" key is stored as "LF" or "CRLF" on Windows/Linux depending on the systems,
Git automatically converts it for us.
