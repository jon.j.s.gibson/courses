# -*- mode: org -*-

1. From the starting page, select "*Create a new project*".

   [[file:reference/images/cb_newproject_01_splash.png]]

2. Select "*Empty project*" and click "Go".

   [[file:reference/images/cb_newproject_02_emptyproject.png]]

3. Set your *Project name*. Make sure to set the project *Location* to somewhere within your *repository directory*.

   [[file:reference/images/cb_newproject_03_namelocation.png]]

4. Go to the "*File*" dropdown menu and select "*New*", then "*Empty file*".

   [[file:reference/images/cb_newproject_04_addfile.png]]

5. It will ask you to save the file first, name it *main.cpp*. Default options are fine.

   [[file:reference/images/cb_newproject_04_addfile2.png]]

6. Add a simple starter C++ program into main.cpp.

   [[file:reference/images/cb_newproject_05_program.png]]

7. Go to the "*Build*" dropdown menu and select "*Build*".

   [[file:reference/images/cb_newproject_06_build.png]]

8. The program should build without any errors.

   [[file:reference/images/cb_newproject_07_buildsuccess.png]]

9. Click on the "play" button in the toolbar to run the program.

   [[file:reference/images/cb_newproject_08_run.png]]

10. Your program text should show up in the console.

    [[file:reference/images/cb_newproject_08_run2.png]]

11. Your files will be in the location you set up. The .cpp file is your source code
    and the .cbp file is the project file.

    [[file:reference/images/cb_newproject_09_files.png]]
